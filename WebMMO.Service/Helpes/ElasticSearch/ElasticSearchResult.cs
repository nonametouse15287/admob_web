﻿using System;
using System.Collections.Generic;

namespace WebMMO.Service.Helpers.ElasticSearch
{
    public class ElasticSearchResult
    {
        public int took { get; set; }
        public bool timed_out { get; set; }
        public _Shards _shards { get; set; }
        public Hits hits { get; set; }
    }

    public class ElasticSearchUpdateResult
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public int _version { get; set; }
        public string result { get; set; }
        public _Shards _shards { get; set; }
        public int _seq_no { get; set; }
        public int _primary_term { get; set; }
    }

    public class ElasticSearchBulkInsertReturn
    {
        public int took { get; set; }
        public bool errors { get; set; }
        public Item[] items { get; set; }
    }

    public class ElasticSearchBulkUpdateResult
    {
        public int took { get; set; }
        public bool errors { get; set; }
        public ItemBulkUpdate[] items { get; set; }
    }

    public class ItemBulkUpdate
    {
        public Update update { get; set; }
    }

    public class Update
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public int _version { get; set; }
        public string result { get; set; }
        public _Shards _shards { get; set; }
        public int _seq_no { get; set; }
        public int _primary_term { get; set; }
        public int status { get; set; }
    }

    public class IndexBulkInsert
    {
        public IndexInsert index { get; set; }
    }

    public class IndexBulkUpdate
    {
        public IndexUpdate update { get; set; }
    }
    public class IndexUpdate
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
    }

    public class IndexInsert
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
    }

    public class Item
    {
        public Index index { get; set; }
    }

    public class Index
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public int _version { get; set; }
        public string result { get; set; }
        public _Shards _shards { get; set; }
        public int _seq_no { get; set; }
        public int _primary_term { get; set; }
        public int status { get; set; }
    }

    public class ElasticSearchUpdate
    {
        public dynamic doc;
        public bool doc_as_upsert;
    }

    public class _Shards
    {
        public int total { get; set; }
        public int successful { get; set; }
        public int skipped { get; set; }
        public int failed { get; set; }
    }

    public class Hits
    {
        public int total { get; set; }
        public float? max_score { get; set; }
        public Hit[] hits { get; set; }
    }
    public class Hit
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public float? _score { get; set; }
        public _Source _source { get; set; }
        public List<double> sort { get; set; }
    }

    public class _Source
    {
        public int ID { get; set; }
        public Guid Guid { get; set; }
        public Guid GuidSearch { get; set; }
        public int? CategoriesId { get; set; }
        public int? SubCategoriesId { get; set; }
        public string KeyElasticSearch { get; set; }
        public string ImageThumb { get; set; }
        public string ImageLink { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public int? Status { get; set; }
        public int? SyncElasticStatus { get; set; }
    }

}
