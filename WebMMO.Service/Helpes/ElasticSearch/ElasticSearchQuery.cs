﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace WebMMO.Service.Helpers.ElasticSearch
{
    public class SeacrhByQueryPaging
    {
        public int from { get; set; }
        public int size { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public QueryPaging query { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public FilterByQueryPaging filter { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<object> sort { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TermByQueryPaging term { get; set; }
    }

    public class QueryPaging
    {
        [JsonProperty("bool", NullValueHandling = NullValueHandling.Ignore)]
        public BoolByQueryPaging _bool { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<FilterByQueryPaging> filter { get; set; }

    }

    public class BoolByQueryPaging
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<object> must { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<FilterByQueryPaging> filter { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<FilterByQueryPaging> should { get; set; }
    }

    public class MustByQueryPaging
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public MatchByQueryPaging match { get; set; }

        [JsonProperty("bool", NullValueHandling = NullValueHandling.Ignore)]
        public object _bool { get; set; }
    }

    public class MatchByQueryPaging
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public NameByQueryPaging KeyElasticSearch { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public NameByQueryPaging CategoriesId { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public NameByQueryPaging SubCategoriesId { get; set; }
    }

    public class NameByQueryPaging
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string query { get; set; }
        [JsonProperty("operator", NullValueHandling = NullValueHandling.Ignore)]
        public string _operator { get; set; }
    }

    public class QueryByNameDetail
    {
        public string query { get; set; }
        [JsonProperty("operator")]
        public string _operator { get; set; }
    }

    public class Match
    {
        public string _id { get; set; }
    }
    public class QueryByName
    {
        public MatchQueryByName query { get; set; }
    }

    public class MatchQueryByName
    {
        public MatchName match { get; set; }
    }

    public class MatchName
    {
        public QueryByNameDetail name { get; set; }
    }

    public class FilterByQueryPaging
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TermByQueryPaging term { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<object> or { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public RangeByQueryPaging range { get; set; }
        [JsonProperty("bool", NullValueHandling = NullValueHandling.Ignore)]
        public BoolByQueryPaging _bool { get; set; }

    }

    public class FilterTerm
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TermByQueryPaging term { get; set; }
    }

    public class TermByQueryPaging
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string CategoriesId { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string SubCategoriesId { get; set; }
    }

    public class RangeByQueryPaging
    {


    }

}
