﻿using WebMMO.Common.Helpes;
using WebMMO.Service.Helpers.ElasticSearch;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebMMO.Service.Helpes
{
    public class ElasticSearchHelper
    {
        public static T ExecuteRest<T>(RestRequest request) where T : new()
        {
            var client = new RestClient();
            client.BaseUrl = new System.Uri(ConfigHelpers._ElasticSearchURL);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            //request.AddHeader("Authorization", "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(string.Format("{0}:{1}", ConfigHelpers._ElasticSearchUserName, ConfigHelpers._ElasticSearchPassword))));
            var response = client.Execute<T>(request);
            response.Data = JsonConvert.DeserializeObject<T>(((RestSharp.RestResponseBase)(response)).Content);
            return response.Data;
        }

        #region CRUD this one
        public static ElasticSearchResult DeleteByAll(string Id, string categories, string subcategories)
        {
            try
            {
                var request = new RestRequest(string.Format("{1}", Id, categories.ToLower(), subcategories.ToLower()), Method.DELETE);
                var result = ExecuteRest<ElasticSearchResult>(request);
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static ElasticSearchResult DeleteById(string Id, string categories, string subcategories)
        {
            try
            {
                var request = new RestRequest(string.Format("{1}/{2}/{0}", Id, categories.ToLower(), subcategories.ToLower()), Method.DELETE);
                var result = ExecuteRest<ElasticSearchResult>(request);
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static ElasticSearchResult InsertOne(dynamic ObjectData, string Id, string categories, string subcategories)
        {
            try
            {
                var request = new RestRequest(string.Format("{1}/{2}/{0}/_create", Id, categories.ToLower().ToLower(), subcategories.ToLower().ToLower()), Method.POST);
                request.AddParameter("application/json", JsonConvert.SerializeObject(ObjectData), ParameterType.RequestBody);
                var result = ExecuteRest<ElasticSearchResult>(request);
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static ElasticSearchUpdateResult UpdateOne(dynamic ObjectData, string Id, string categories, string subcategories)
        {
            try
            {
                var request = new RestRequest(string.Format("{1}/{2}/{0}/_update", Id, categories.ToLower(), subcategories.ToLower()), Method.POST);
                ElasticSearchUpdate objUpdate = new ElasticSearchUpdate
                {
                    doc = ObjectData,
                    doc_as_upsert = true
                };
                request.AddParameter("application/json", JsonConvert.SerializeObject(objUpdate), ParameterType.RequestBody);
                var result = ExecuteRest<ElasticSearchUpdateResult>(request);
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region CRUD ALL
        public static ElasticSearchBulkInsertReturn InsertBulk(dynamic ObjectDataList, string categories, string subcategories)
        {
            try
            {
                var jsonContent = new StringBuilder();
                var request = new RestRequest($@"{categories}/_bulk", Method.POST);
                foreach (var oneObject in ObjectDataList)
                {
                    IndexBulkInsert oneIndex = new IndexBulkInsert
                    {
                        index = new IndexInsert
                        {
                            _id = oneObject.ID.ToString(),
                            _index = categories,
                            _type = subcategories
                        }
                    };
                    jsonContent.AppendLine(JsonConvert.SerializeObject(oneIndex));
                    jsonContent.AppendLine(JsonConvert.SerializeObject(oneObject));
                }
                request.AddParameter("application/json", jsonContent.ToString(), ParameterType.RequestBody);
                var result = ExecuteRest<ElasticSearchBulkInsertReturn>(request);
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static ElasticSearchBulkUpdateResult UpdateBulk(dynamic ObjectDataList, string categories, string subcategories)
        {
            try
            {
                var jsonContent = new StringBuilder();
                var request = new RestRequest($@"{categories}/_bulk", Method.POST);
                foreach (var oneObject in ObjectDataList)
                {
                    IndexBulkUpdate oneIndex = new IndexBulkUpdate
                    {
                        update = new IndexUpdate
                        {
                            _id = oneObject.ID.ToString(),
                            _index = categories,
                            _type = subcategories
                        }
                    };
                    ElasticSearchUpdate objUpdate = new ElasticSearchUpdate
                    {
                        doc = oneObject,
                        doc_as_upsert = true
                    };
                    jsonContent.AppendLine(JsonConvert.SerializeObject(oneIndex));
                    jsonContent.AppendLine(JsonConvert.SerializeObject(objUpdate));
                }
                request.AddParameter("application/json", jsonContent.ToString(), ParameterType.RequestBody);
                var result = ExecuteRest<ElasticSearchBulkUpdateResult>(request);
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region Search

        public static ElasticSearchResult SearhByName(string name, string categories, string subcategories)
        {
            try
            {
                var request = new RestRequest($@"{categories.ToLower()}/{subcategories.ToLower()}/_search", Method.POST);

                QueryByName queryMain = new QueryByName
                {
                    query = new MatchQueryByName
                    {
                        match = new MatchName
                        {
                            name = new QueryByNameDetail
                            {
                                query = name,
                                _operator = "and"
                            }
                        }
                    }
                };
                request.AddParameter("application/json", JsonConvert.SerializeObject(queryMain), ParameterType.RequestBody);

                var result = ExecuteRest<ElasticSearchResult>(request);
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static ElasticSearchResult SearhByQuery(string queryJson, string categories, string subcategories)
        {
            try
            {
                var request = new RestRequest($@"{categories.ToLower()}/{subcategories.ToLower()}/_search", Method.POST);
                request.AddParameter("application/json", queryJson, ParameterType.RequestBody);

                var result = ExecuteRest<ElasticSearchResult>(request);
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string BuildQuerySearch(int PageNumber, int PageSize, string SearchKey, string CategoriesId, string SubCategoriesId)
        {
            SeacrhByQueryPaging queryMain = new SeacrhByQueryPaging();
            int pageNum = (PageNumber - 1) < 0 ? 0 : (PageNumber - 1);
            queryMain.from = pageNum * PageSize;
            queryMain.size = PageSize;
            queryMain.query = new QueryPaging();
            queryMain.query._bool = new BoolByQueryPaging();

            if (!string.IsNullOrEmpty(SearchKey))
            {
                queryMain.query._bool.must = new List<object>();

                MustByQueryPaging queryMatch = new MustByQueryPaging();
                queryMatch.match = new MatchByQueryPaging();
                queryMatch.match.KeyElasticSearch = new NameByQueryPaging();
                queryMatch.match.KeyElasticSearch.query = SearchKey;
                queryMatch.match.KeyElasticSearch._operator = "and";
                queryMain.query._bool.must.Add(queryMatch);
            }

            if (!string.IsNullOrEmpty(CategoriesId) && CategoriesId != "0")
            {
                if (queryMain.query._bool.must == null)
                {
                    queryMain.query._bool.must = new List<object>();
                }

                MustByQueryPaging queryMatch = new MustByQueryPaging();
                queryMatch.match = new MatchByQueryPaging();
                queryMatch.match.CategoriesId = new NameByQueryPaging();
                queryMatch.match.CategoriesId.query = CategoriesId.ToString();
                queryMatch.match.CategoriesId._operator = "and";
                queryMain.query._bool.must.Add(queryMatch);

                //if (queryMain.filter == null)
                //{
                //    queryMain.filter = new FilterByQueryPaging();
                //    queryMain.filter.or = new List<object>();

                //}

                //string[] arraStrings = CategoriesId.Split(',');
                //List<int> lstCategory = new List<int>();
                //foreach (var item in arraStrings)
                //{
                //    int xxx = 0;
                //    if (int.TryParse(item, out xxx))
                //    {
                //        lstCategory.Add(xxx);
                //    }
                //}

                //if (lstCategory.Count > 0)
                //{
                //    foreach (var item in lstCategory)
                //    {
                //        FilterTerm objFil = new FilterTerm();
                //        objFil.term = new TermByQueryPaging { CategoriesId = item.ToString() };
                //        queryMain.filter.or.Add(objFil);
                //    }
                //}
            }

            if (!string.IsNullOrEmpty(SubCategoriesId) && SubCategoriesId != "0")
            {
                if (queryMain.query._bool.must == null)
                {
                    queryMain.query._bool.must = new List<object>();
                }

                MustByQueryPaging queryMatch = new MustByQueryPaging();
                queryMatch.match = new MatchByQueryPaging();
                queryMatch.match.SubCategoriesId = new NameByQueryPaging();
                queryMatch.match.SubCategoriesId.query = SubCategoriesId.ToString();
                queryMatch.match.SubCategoriesId._operator = "and";
                queryMain.query._bool.must.Add(queryMatch);

                //if (queryMain.filter == null)
                //{
                //    queryMain.filter = new FilterByQueryPaging();
                //    queryMain.filter.or = new List<object>();

                //}

                //string[] arraStrings = SubCategoriesId.Split(',');
                //List<int> lstCategory = new List<int>();
                //foreach (var item in arraStrings)
                //{
                //    int xxx = 0;
                //    if (int.TryParse(item, out xxx))
                //    {
                //        lstCategory.Add(xxx);
                //    }
                //}

                //if (lstCategory.Count > 0)
                //{
                //    foreach (var item in lstCategory)
                //    {
                //        FilterTerm objFil = new FilterTerm();
                //        objFil.term = new TermByQueryPaging { SubCategoriesId = item.ToString() };
                //        queryMain.filter.or.Add(objFil);
                //    }
                //}
            }

            string strJsonQuery = JsonConvert.SerializeObject(queryMain);
            return strJsonQuery;
        }
        #endregion
    }
}
