﻿using WebMMO.Core.DomainModels.Appsetting;
using WebMMO.Core.IRepositories;
using WebMMO.Core.MessageResponse;
using WebMMO.Infrastructures.Dapper.Repositories;
using WebMMO.Service.Services;

namespace WebMMO.Service.ServicesImpl
{
    public class AppSettingService : IAppSettingService
    {
        private readonly IAppSettingRepository _appSettingRepository;
        public AppSettingService()
        {
            _appSettingRepository = new AppSettingRepository();
        }

        public ServiceResponse<AppSettingDto> GetAppSettingByName(string isUnderMaintenanceProperty)
        {
            var result = _appSettingRepository.GetAppSettingByName(isUnderMaintenanceProperty);

            return new ServiceResponse<AppSettingDto>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> UpdateAppSetting(AppSettingDto appSetting)
        {
            int result = _appSettingRepository.UpdateAppSetting(appSetting);

            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }
    }
}
