﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebMMO.Core.DomainModels.Role;
using WebMMO.Core.DomainModels.User;
using WebMMO.Core.IRepositories;
using WebMMO.Core.MessageResponse;
using WebMMO.Infrastructures.Dapper.Repositories;
using WebMMO.Service.Services;

namespace WebMMO.Service.ServicesImpl
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService()
        {
            _userRepository = new UserRepository();
        }

        public ServiceResponse<UserDto> GetUserById(int id)
        {
            var result = _userRepository.GetUserById(id);

            return new ServiceResponse<UserDto>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> CreateNewUser(object o)
        {
            int result = 0;
            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<UserDto> GetUserByUserName(string userName)
        {
            UserDto result = _userRepository.GetUserByUsername(userName);

            return new ServiceResponse<UserDto>(result, string.Empty, "success", false);
        }

        public ServiceResponse<UserDto> GetUserByEmail(string email)
        {
            UserDto result = _userRepository.GetUserByEmail(email);

            return new ServiceResponse<UserDto>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> UpdatePassword(UserDto user)
        {
            int result = _userRepository.UpdatePassword(user);

            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> UpdateLastLoginDate(string email)
        {
            var user = _userRepository.GetUserByEmail(email);
            user.LastLoginDate = DateTime.UtcNow;

            int result = _userRepository.UpdateLastLoginDate(user);

            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<ActivateUserRequestDto> GetActivateUserByKey(string id)
        {
            ActivateUserRequestDto result = _userRepository.GetActivateUserByKey(id);

            return new ServiceResponse<ActivateUserRequestDto>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> UpdateUser(UserDto user)
        {

            int result = _userRepository.UpdateUser(user);

            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> RemoveActivateUserRequest(ActivateUserRequestDto activateUserRequest)
        {
            int result = _userRepository.RemoveActivateUserRequest(activateUserRequest);

            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<ActivateUserRequestDto> GetActivateUserByUserId(int id)
        {
            ActivateUserRequestDto result = _userRepository.GetActivateUserByUserId(id);

            return new ServiceResponse<ActivateUserRequestDto>(result, string.Empty, "success", false);
        }

        public ServiceResponse<IEnumerable<ActivateUserRequestDto>> GetActivateUserAll()
        {
            List<ActivateUserRequestDto> result = _userRepository.GetActivateUserAll();

            return new ServiceResponse<IEnumerable<ActivateUserRequestDto>>(result, string.Empty, "success", false);
        }

        public ServiceResponse<ResetPasswordRequestDto> GetResetPasswordByKey(string id)
        {
            ResetPasswordRequestDto result = _userRepository.GetResetPasswordByKey(id);

            return new ServiceResponse<ResetPasswordRequestDto>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> RemoveResetPassword(ResetPasswordRequestDto resetPasswordRequest)
        {
            int result = _userRepository.RemoveResetPassword(resetPasswordRequest);

            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> InsertResetPasswordRequest(ResetPasswordRequestDto resetPasswordRequest)
        {
            int result = _userRepository.InsertResetPasswordRequest(resetPasswordRequest);

            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<bool> CheckEmailUser(string newEmailAddress)
        {
            bool result = false;
            var user = _userRepository.GetUserByEmail(newEmailAddress);
            if (user != null) result = true;
            return new ServiceResponse<bool>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> AddUser(UserDto user)
        {
            int result = _userRepository.AddUser(user);

            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> AddUserRole(UserRoleDto userrole)
        {
            int result = _userRepository.AddUserRole(userrole);

            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<IEnumerable<UserRoleDto>> GetUserRolesByUserId(int userId)
        {
            List<UserRoleDto> result = _userRepository.GetUserRolesByUserId(userId).ToList();

            return new ServiceResponse<IEnumerable<UserRoleDto>>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> DeleteUserRole(UserRoleDto currentRole)
        {
            int result = _userRepository.DeleteUserRole(currentRole);

            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> DeleteUserRoleByUserId(int userId)
        {
            int result = _userRepository.DeleteUserRoleByUserId(userId);

            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<IEnumerable<UserRoleDto>> GetUserRolesById(int userId, int roleId)
        {
            IEnumerable<UserRoleDto> result = _userRepository.GetUserRolesById(userId, roleId);

            return new ServiceResponse<IEnumerable<UserRoleDto>>(result, string.Empty, "success", false);
        }

        public ServiceResponse<IEnumerable<UserResult>> GetQueryCbUser(int? organizationId, string status, int? roleId)
        {
            IEnumerable<UserResult> result = _userRepository.GetQueryCbUser(organizationId, status, roleId);

            return new ServiceResponse<IEnumerable<UserResult>>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> UpdateActiveUserRequest(ActivateUserRequestDto activateUserRequest)
        {
            int result = _userRepository.UpdateActiveUserRequest(activateUserRequest);

            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<IEnumerable<StateDto>> GetStateByCountryId(int countryIdValue)
        {
            IEnumerable<StateDto> result = _userRepository.GetStateByCountryId(countryIdValue);

            return new ServiceResponse<IEnumerable<StateDto>>(result, string.Empty, "success", false);
        }
    }
}
