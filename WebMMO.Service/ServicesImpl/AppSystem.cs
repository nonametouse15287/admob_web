﻿using System;
using WebMMO.Core.DomainModels.AppManage;
using WebMMO.Core.IRepositories;
using WebMMO.Core.MessageResponse;
using WebMMO.Infrastructures.Dapper.Repositories;
using WebMMO.Service.Services;
using System.Collections.Generic;
using WebMMO.Core.DomainModels.Appsetting;

namespace WebMMO.Service.ServicesImpl
{
    public class AppSystemService : IAppSystemService
    {
        private readonly IAppSystemRepository _appSystemRepository;
        public AppSystemService()
        {
            _appSystemRepository = new AppSystemRepository();
        }

        public int AddAndEdit(AppPermissionDto objNew)
        {
            int dataResult = _appSystemRepository.AddAndEdit(objNew);
            return dataResult;
        }

        public int AddAndEditArticlesAdmod(ArticlesAdmodDto objNew)
        {
            int dataResult = _appSystemRepository.AddAndEditArticlesAdmod(objNew);
            return dataResult;
        }

        public AppPermissionDto AppPermissionByGuid(Guid value)
        {
            AppPermissionDto dataResult = _appSystemRepository.AppPermissionByGuid(value);
            return dataResult;
        }

        public ArticlesAdmodDto ArticlesAdmodById(int value)
        {
            ArticlesAdmodDto dataResult = _appSystemRepository.ArticlesAdmodById(value);
            return dataResult;
        }

        public DataTableViewModel<AppPermissionResult> SearchAppSystems(FilterTableParams tableParams, string name)
        {
            Tuple<int, List<AppPermissionResult>> dataResult = _appSystemRepository.SearchAppSystems(tableParams, name);
            return new DataTableViewModel<AppPermissionResult>(tableParams.draw, dataResult.Item1, dataResult.Item1, dataResult.Item2);
        }

        public DataTableViewModel<ArticlesAdmodDto> SearchArticlesAdmod(FilterTableParams tableParams, string name)
        {
            Tuple<int, List<ArticlesAdmodDto>> dataResult = _appSystemRepository.SearchArticlesAdmod(tableParams, name);
            return new DataTableViewModel<ArticlesAdmodDto>(tableParams.draw, dataResult.Item1, dataResult.Item1, dataResult.Item2);
        }
    }
}
