﻿using System;
using WebMMO.Core.DomainModels.AppManage;
using WebMMO.Core.IRepositories;
using WebMMO.Core.MessageResponse;
using WebMMO.Infrastructures.Dapper.Repositories;
using WebMMO.Service.Services;
using System.Collections.Generic;
using WebMMO.Core.DomainModels.SSH;
using WebMMO.Core.DomainModels.Email;
using WebMMO.Core.DomainModels.Youtube;
using WebMMO.Core.DomainModels.Appsetting;

namespace WebMMO.Service.ServicesImpl
{
    public class SSHSystemService : ISSHSystemService
    {
        private readonly ISSHSystemRepository _sSHSystemRepository;
        public SSHSystemService()
        {
            _sSHSystemRepository = new SSHSystemRepository();
        }

        public List<ArticlesAdmodDto> GetArticlesAdmod(string countryCode)
        {
            List<ArticlesAdmodDto> dataResult = _sSHSystemRepository.GetArticlesAdmod(countryCode);
            return dataResult;
        }

        public GmailEmailResult GetGmailNoneProfiles(string accessToken, string countryCode)
        {
            GmailEmailResult dataResult = _sSHSystemRepository.GetGmailNoneProfiles(accessToken, countryCode);
            return dataResult;
        }

        public SSHWareHouseDto GetSSHSystem(string accessToken, string countryCode)
        {
            SSHWareHouseDto dataResult = _sSHSystemRepository.GetSSHSystem(accessToken, countryCode);
            return dataResult;
        }

        public List<SSHWareHouseDto> GetTopSSH()
        {
            List<SSHWareHouseDto> dataResult = _sSHSystemRepository.GetSSHSystem();
            return dataResult;
        }

        public QuanLyKenhYoutubeResult GetYoutubeInfo(string accessToken, string countryCode)
        {
            QuanLyKenhYoutubeResult dataResult = _sSHSystemRepository.GetYoutubeInfo();
            return dataResult;
        }

        public int InsertSShByList(List<SSHWareHouseDto> lstInsert)
        {
            int dataResult = _sSHSystemRepository.InsertSShByList(lstInsert);
            return dataResult;
        }

        public void ResetSSH()
        {
            _sSHSystemRepository.ResetSSH();
        }

        public DataTableViewModel<SSHWareHouseDto> SearchSSHSystem(FilterTableParams tableParams)
        {
            Tuple<int, List<SSHWareHouseDto>> dataResult = _sSHSystemRepository.SearchSSHSystem(tableParams);
            return new DataTableViewModel<SSHWareHouseDto>(tableParams.draw, dataResult.Item1, dataResult.Item1, dataResult.Item2);
        }

        public int UpdateGmailStatus(int id, int? statusprofile, string description)
        {
            int result = _sSHSystemRepository.UpdateGmailStatus(id, statusprofile, description);
            return result;
        }

        public void UpdateStatusSSH(SSHWareHouseDto item)
        {
            _sSHSystemRepository.UpdateStatusSSH(item);
        }
    }
}
