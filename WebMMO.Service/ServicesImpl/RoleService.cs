﻿using System.Collections.Generic;
using WebMMO.Core.DomainModels.Permission;
using WebMMO.Core.DomainModels.Role;
using WebMMO.Core.IRepositories;
using WebMMO.Core.MessageResponse;
using WebMMO.Infrastructures.Dapper.Repositories;
using WebMMO.Service.Services;

namespace WebMMO.Service.ServicesImpl
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;
        public RoleService()
        {
            _roleRepository = new RoleRepository();
        }

        public ServiceResponse<string> GetRoleByUserId(int id)
        {
            var result = _roleRepository.GetRoleByUserId(id);
            string strRole = string.Empty;
            if (result != null)
            {
                foreach (var items in result)
                {
                    if (strRole == "")
                    {
                        strRole = items.Name;
                    }
                    else
                    {
                        strRole += ", " + items.Name;
                    }
                }
            }

            return new ServiceResponse<string>(strRole, string.Empty, "success", false);
        }

        public ServiceResponse<int> GetHighesRoleRankByUserId(int id)
        {
            var result = _roleRepository.GetRoleByUserId(id);

            int rankLever = 0;
            if (result != null)
            {
                foreach (var items in result)
                {
                    if (items.RankLevel != null)
                    {
                        if (rankLever == 0)
                        {
                            rankLever = (int)items.RankLevel;
                        }
                        else
                        {
                            if (rankLever > items.RankLevel)
                            {
                                rankLever = (int)items.RankLevel;
                            }
                        }
                    }
                }
            }

            return new ServiceResponse<int>(rankLever, string.Empty, "success", false);
        }

        public ServiceResponse<RoleDto> GetRoleById(int roleRoleId)
        {
            RoleDto result = _roleRepository.GetRoleById(roleRoleId);
            return new ServiceResponse<RoleDto>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> InsertPermission(PermissionDto permissionDto)
        {
            int result = _roleRepository.InsertPermission(permissionDto);
            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<IEnumerable<PermissionDto>> GetPermission(string requiredPermission)
        {
            IEnumerable<PermissionDto> result = _roleRepository.GetPermission(null, null, requiredPermission, null);
            return new ServiceResponse<IEnumerable<PermissionDto>>(result, string.Empty, "success", false);
        }

        public ServiceResponse<IEnumerable<RoleDto>> GetRoleAll()
        {
            IEnumerable<RoleDto> result = _roleRepository.GetRoleAll();
            return new ServiceResponse<IEnumerable<RoleDto>>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> InsertRoles(RoleDto objNew)
        {
            int result = _roleRepository.InsertRoles(objNew);
            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> UpdateRole(RoleDto currentRole)
        {
            int result = _roleRepository.UpdateRole(currentRole);
            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<IEnumerable<RoleDto>> GetRoleQuery(bool isSysAdmin, bool isExternal, int rolerank)
        {
            IEnumerable<RoleDto> result = _roleRepository.GetRoleQuery(isSysAdmin, isExternal, rolerank);
            return new ServiceResponse<IEnumerable<RoleDto>>(result, string.Empty, "success", false);
        }



        public ServiceResponse<int> DeleteRolePermissionById(int id)
        {
            int result = _roleRepository.DeleteRolePermissionById(id);
            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> DeletePermissionById(int permissionId)
        {
            int result = _roleRepository.DeletePermissionById(permissionId);
            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<IEnumerable<RolePermissionDto>> GetRolePermissionById(int? id, int? roleid, int? permissionId)
        {
            IEnumerable<RolePermissionDto> result = _roleRepository.GetRolePermissionById(id, roleid, permissionId);
            return new ServiceResponse<IEnumerable<RolePermissionDto>>(result, string.Empty, "success", false);
        }

        public ServiceResponse<IEnumerable<PermissionDto>> GetPermission(int? id, int? menuId, string permissionDescription, string name)
        {
            IEnumerable<PermissionDto> result = _roleRepository.GetPermission(id, menuId, permissionDescription, name);
            return new ServiceResponse<IEnumerable<PermissionDto>>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> InsertRolePermission(RolePermissionDto rolePermission)
        {
            int result = _roleRepository.InsertRolePermission(rolePermission);
            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> UpdatePermission(PermissionDto permission)
        {
            int result = _roleRepository.UpdatePermission(permission);
            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<bool> CheckPermission(int userID, string strPermission)
        {
            bool result = _roleRepository.CheckPermission(userID, strPermission);
            return new ServiceResponse<bool>(result, string.Empty, "success", false);
        }
    }
}
