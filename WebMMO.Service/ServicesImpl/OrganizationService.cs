﻿using System.Collections.Generic;
using WebMMO.Core.DomainModels.Organization;
using WebMMO.Core.IRepositories;
using WebMMO.Core.MessageResponse;
using WebMMO.Infrastructures.Dapper.Repositories;
using WebMMO.Service.Services;

namespace WebMMO.Service.ServicesImpl
{
    public class OrganizationService : IOrganizationService
    {
        private readonly IOrganizationRepository _organizationRepository;
        public OrganizationService()
        {
            _organizationRepository = new OrganizationRepository();
        }

        public  ServiceResponse<OrganizationDto>GetOrganizationById(int id)
        {
            var result =  _organizationRepository.GetOrganizationById(id);

            return new ServiceResponse<OrganizationDto>(result, string.Empty, "success", false);
        }

        public ServiceResponse<IEnumerable<OrganizationDto>> GetOrganizationByCompanyCode(string firstLetter)
        {
            List<OrganizationDto> result = _organizationRepository.GetOrganizationByCompanyCode(firstLetter);

            return new ServiceResponse<IEnumerable<OrganizationDto>>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> UpdateOrganization(OrganizationDto organization)
        {
            int result = _organizationRepository.UpdateOrganization(organization);

            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }
    }
}
