﻿using System.Collections.Generic;
using WebMMO.Core.DomainModels.Menu;
using WebMMO.Core.IRepositories;
using WebMMO.Core.MessageResponse;
using WebMMO.Infrastructures.Dapper.Repositories;
using WebMMO.Service.Services;

namespace WebMMO.Service.ServicesImpl
{
    public class MenuService : IMenuService
    {
        private readonly IMenuRepository _menuRepository;
        public MenuService()
        {
            _menuRepository = new MenuRepository();
        }

        public ServiceResponse<IEnumerable<MenuDto>> GetMenuList(int? menuParentId, int? Seq)
        {
            IEnumerable<MenuDto> result = _menuRepository.GetMenuList(menuParentId, Seq);

            return new ServiceResponse<IEnumerable<MenuDto>>(result, string.Empty, "success", false);
        }

        public ServiceResponse<MenuDto> GetMenuById(int Id)
        {
            MenuDto result = _menuRepository.GetMenuById(Id);

            return new ServiceResponse<MenuDto>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> UpdateMenu(MenuDto menu)
        {
            int result = _menuRepository.UpdateMenu(menu);

            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<IEnumerable<RoleMenuDto>> GetRoleMenu(int? menuId, int? roleId)
        {
            IEnumerable<RoleMenuDto> result = _menuRepository.GetRoleMenu(menuId, roleId);

            return new ServiceResponse<IEnumerable<RoleMenuDto>>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> DeleteRoleMenu(int id)
        {
            int result = _menuRepository.DeleteRoleMenu(id);

            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> InsertRoleMenu(RoleMenuDto newaccess)
        {
            int result = _menuRepository.InsertRoleMenu(newaccess);

            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<IEnumerable<MenuDto>> GetMenuByParentId(int parentId)
        {
            IEnumerable<MenuDto> result = _menuRepository.GetMenuByParentId(parentId);

            return new ServiceResponse<IEnumerable<MenuDto>>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> InsertMenu(MenuDto menu)
        {
            int result = _menuRepository.InsertMenu(menu);

            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<int> DeleteMenu(int menuId)
        {
            int result = _menuRepository.DeleteMenu(menuId);

            return new ServiceResponse<int>(result, string.Empty, "success", false);
        }

        public ServiceResponse<IEnumerable<MenuDto>> GetMenu_HIAS_GET_MENUACCESS(int id, int? intMenuParentID)
        {
            IEnumerable<MenuDto> result = _menuRepository.GetMenu_HIAS_GET_MENUACCESS(id, intMenuParentID);

            return new ServiceResponse<IEnumerable<MenuDto>>(result, string.Empty, "success", false);
        }
    }
}
