﻿using System;
using WebMMO.Core.DomainModels.WebMMO;
using WebMMO.Core.MessageResponse;
using WebMMO.Service.Services;
using WebMMO.Core.IRepositories;
using WebMMO.Infrastructures.Dapper.Repositories;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Extensions.MonoHttp;
using WebMMO.Core.DomainModels.ImageSystem;
using WebMMO.Core.DomainModels.Sms;
using WebMMO.Service.Helpers.ElasticSearch;
using WebMMO.Core.DomainModels.Gmail;
using WebMMO.Core.DomainModels.devices;

namespace WebMMO.Service.ServicesImpl
{
    public class WebMmoService : IWebMmoService
    {
        private readonly IWebMMORepository _webMmoRepository;
        public WebMmoService()
        {
            _webMmoRepository = new WebMMORepository();
        }

        public int AddAndEdit(ImageCategoriesDto model)
        {
            int dataResult = _webMmoRepository.AddAndEdit(model);
            return dataResult;
        }

        public int AddAndEditImageSearch(ImageListDto objInsert)
        {
            int dataResult = _webMmoRepository.AddAndEditImageSearch(objInsert);
            return dataResult;
        }

        public int AddAndEditSearchLog(SearchLogDto objSearch)
        {
            int dataResult = _webMmoRepository.AddAndEditSearchLog(objSearch);
            return dataResult;
        }

        public int AddAndEditSubCategories(ImageSubCategoriesDto objNew)
        {
            int dataResult = _webMmoRepository.AddAndEditSubCategories(objNew);
            return dataResult;
        }

        public List<ImageCategoriesDto> GetAllCategories()
        {
            List<ImageCategoriesDto> dataResult = _webMmoRepository.GetAllCategories();
            return dataResult;
        }

        public List<ImageListDto> GetAllImagesSync(int status)
        {
            List<ImageListDto> dataResult = _webMmoRepository.GetAllImagesSync(status);
            return dataResult;
        }

        public List<ImageSubCategoriesDto> GetAllSubCategories()
        {
            List<ImageSubCategoriesDto> dataResult = _webMmoRepository.GetAllSubCategories();
            return dataResult;
        }

        public List<ImageCategoriesDto> GetAppCategories(string appPackage)
        {
            List<ImageCategoriesDto> dataResult = _webMmoRepository.GetAppCategories(appPackage);
            return dataResult;
        }

        public List<ImageSubCategoriesDto> GetAppSubCategories(string appPackage, int categoriesId)
        {
            List<ImageSubCategoriesDto> dataResult = _webMmoRepository.GetSubCategoriesByGroup(categoriesId);
            return dataResult;
        }

        public ImageCategoriesDto GetCategoriesByGuid(Guid value)
        {
            ImageCategoriesDto dataResult = _webMmoRepository.GetCategoriesByGuid(value);
            return dataResult;
        }

        public ImageListDto GetWebMmoByGuid(Guid valueGuid)
        {
            ImageListDto dataResult = _webMmoRepository.GetWebMMOByGuid(valueGuid);
            return dataResult;
        }

        public List<ImageSubCategoriesDto> GetSubCategoriesByGroup(int categoriesId)
        {
            List<ImageSubCategoriesDto> dataResult = _webMmoRepository.GetSubCategoriesByGroup(categoriesId);
            return dataResult;
        }

        public ImageSubCategoriesDto GetSubCategoriesByGuid(Guid value)
        {
            ImageSubCategoriesDto dataResult = _webMmoRepository.GetSubCategoriesByGuid(value);
            return dataResult;
        }

        public ApiKeyDto GetTop1Key()
        {
            ApiKeyDto dataResult = _webMmoRepository.GetTop1Key();
            return dataResult;
        }

        public int RefreshApiKey()
        {
            int dataResult = _webMmoRepository.RefreshApiKey();
            return dataResult;
        }

        public DataTableViewModel<ImageCategoriesResult> SearchCategories(FilterTableParams tableParams, string name)
        {
            Tuple<int, List<ImageCategoriesResult>> dataResult = _webMmoRepository.SearchCategories(tableParams, name);
            return new DataTableViewModel<ImageCategoriesResult>(tableParams.draw, dataResult.Item1, dataResult.Item1, dataResult.Item2);
        }

        public DataTableViewModel<ImageListResult> SearchImages(FilterTableParams tableParams, string name, string Status, int Categories, int SubCategories)
        {
            Tuple<int, List<ImageListResult>> dataResult = _webMmoRepository.SearchImages(tableParams, name, Status, Categories, SubCategories);
            return new DataTableViewModel<ImageListResult>(tableParams.draw, dataResult.Item1, dataResult.Item1, dataResult.Item2);
        }

        public DataTableViewModel<ImageSubCategoriesResult> SearchSubCategories(FilterTableParams tableParams, string name, string Status, int Categories)
        {
            Tuple<int, List<ImageSubCategoriesResult>> dataResult = _webMmoRepository.SearchSubCategories(tableParams, name, Status, Categories);
            return new DataTableViewModel<ImageSubCategoriesResult>(tableParams.draw, dataResult.Item1, dataResult.Item1, dataResult.Item2);
        }

        public int UpdateCountApiKey(int iD)
        {
            int dataResult = _webMmoRepository.UpdateCountApiKey(iD);
            return dataResult;
        }

        public int UpdateBalanceApiKey(AccessTokenUserDto chkCheckApi)
        {
            int dataResult = _webMmoRepository.UpdateBalanceApiKey(chkCheckApi);
            return dataResult;
        }



        #region onlinesim.ru

        /// <summary>
        /// 
        /// </summary>
        /// <param name="country">7</param>
        /// <param name="services">gmail</param>
        /// <returns></returns>
        public SmsOnlineSimResult GetNumber(string country, string services)
        {
            NameValueCollection collection = new NameValueCollection();

            collection.Add("country", country);
            collection.Add("service", services);

            SmsOnlineSimResult lstResult = new SmsOnlineSimResult();
            var request = new RestRequest("api/getNum.php" + ToQueryString(collection), Method.GET);
            request.AddParameter("application/x-www-form-urlencoded", ToQueryString(collection), ParameterType.RequestBody);

            var result = ExecuteRest<SmsOnlineSimResult>(request);

            return result;
        }

        public List<SmsOnlineSimResult> GetOperations()
        {
            List<SmsOnlineSimResult> lstResult = new List<SmsOnlineSimResult>();
            NameValueCollection collection = new NameValueCollection();

            var request = new RestRequest("api/getOperations.php" + ToQueryString(collection), Method.GET);

            request.AddParameter("application/x-www-form-urlencoded", ToQueryString(collection), ParameterType.RequestBody);

            var result = ExecuteRest<List<SmsOnlineSimResult>>(request);

            return result;
        }

        public AccessTokenUserDto CheckToken(string token)
        {
            AccessTokenUserDto dataResult = _webMmoRepository.CheckToken(token);
            return dataResult;
        }

        private static T ExecuteRest<T>(RestRequest request) where T : new()
        {
            var client = new RestClient();
            client.BaseUrl = new System.Uri("https://onlinesim.ru");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            //request.AddHeader("Authorization", "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(string.Format("{0}:{1}", ConfigHelpers._ElasticSearchUserName, ConfigHelpers._ElasticSearchPassword))));
            var response = client.Execute<T>(request);
            response.Data = JsonConvert.DeserializeObject<T>(((RestSharp.RestResponseBase)(response)).Content);
            return response.Data;
        }

        private string ToQueryString(NameValueCollection nvc)
        {
            var array = (from key in nvc.AllKeys
                         from value in nvc.GetValues(key)
                         select string.Format("{0}={1}", HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(value)))
                .ToArray();
            return "?" + string.Join("&", array);
        }

        #endregion

        #region MyRegion

        public SmsOnlineSimResult GetNumberActivateRu(string country, string services)
        {
            SmsOnlineSimResult result = new SmsOnlineSimResult();

            NameValueCollection collection = new NameValueCollection();

            collection.Add("country", country);
            collection.Add("service", services);
            collection.Add("action", "getNumber");

            SmsOnlineSimResult lstResult = new SmsOnlineSimResult();
            var request = new RestRequest("stubs/handler_api.php" + ToQueryString(collection), Method.GET);
            request.AddParameter("application/x-www-form-urlencoded", ToQueryString(collection), ParameterType.RequestBody);

            var client = new RestClient();
            client.BaseUrl = new System.Uri("http://sms-activate.ru");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            var response = client.Execute(request);
            return result;
        }

        public AccessTokenUserDto GetAccessTokenUserGuid(string key)
        {
            AccessTokenUserDto dataResult = _webMmoRepository.GetAccessTokenUserGuid(key);
            return dataResult;
        }

        public int AddAndEditAccessTokenUser(AccessTokenUserDto objNew)
        {
            int dataResult = _webMmoRepository.AddAndEditAccessTokenUser(objNew);
            return dataResult;
        }

        public DataTableViewModel<AccessTokenUserDto> SearchManageKey(FilterTableParams tableParams, string name)
        {
            Tuple<int, List<AccessTokenUserDto>> dataResult = _webMmoRepository.SearchManageKey(tableParams, name);
            return new DataTableViewModel<AccessTokenUserDto>(tableParams.draw, dataResult.Item1, dataResult.Item1, dataResult.Item2);

        }

        public int AddAndEditSmsPrice(SmsPriceDto objNew)
        {
            int dataResult = _webMmoRepository.AddAndEditSmsPrice(objNew);
            return dataResult;
        }

        public SmsPriceDto GetSmsPriceById(int id)
        {
            SmsPriceDto dataResult = _webMmoRepository.GetSmsPriceById(id);
            return dataResult;
        }

        public DataTableViewModel<SmsPriceDto> SearchManagePriceSms(FilterTableParams tableParams, string name)
        {
            Tuple<int, List<SmsPriceDto>> dataResult = _webMmoRepository.SearchManagePriceSms(tableParams, name);
            return new DataTableViewModel<SmsPriceDto>(tableParams.draw, dataResult.Item1, dataResult.Item1, dataResult.Item2);

        }



        #endregion

        #region Gmail
        public int GmailPush(GmailInfoDto objGmailPush)
        {
            int dataResult = _webMmoRepository.GmailPush(objGmailPush);
            return dataResult;
        }

        public List<GmailInfoDto> GmailGet(string token, string emailtype, int amount)
        {
            List<GmailInfoDto> dataResult = _webMmoRepository.GmailGet(token, emailtype, amount);
            return dataResult;
        }

        public GmailPriceDto GmailPriceGetByType(string type)
        {
            GmailPriceDto dataResult = _webMmoRepository.GmailPriceGetByType(type);
            return dataResult;
        }

        public List<GmailInfoDto> GmailGetByKey(string token, string type, int amount)
        {
            List<GmailInfoDto> dataResult = _webMmoRepository.GmailGetByKey(token, type, amount);
            return dataResult;
        }

        public GmailStatictisResult GmailStatistics(string token)
        {
            GmailStatictisResult dataResult = _webMmoRepository.GmailStatistics(token);
            return dataResult;
        }

        public int DevicesPush(string deviceinfo)
        {
            int dataResult = _webMmoRepository.DevicesPush(deviceinfo);
            return dataResult;
        }

        public DevicesInfoDto DevicesGet()
        {
            DevicesInfoDto dataResult = _webMmoRepository.DevicesGet();
            return dataResult;
        }

        public void DevicesFakeInsert(DevicesFakeInfoDto objFakeInfo)
        {
            _webMmoRepository.DevicesFakeInsert(objFakeInfo);

        }

        public List<CitiesCodeDto> GetCitiesCode(string countryCode)
        {
            List<CitiesCodeDto> dataResult = _webMmoRepository.GetCitiesCode(countryCode);
            return dataResult;
        }

        public List<string> GetKeyNews()
        {
            List<string> dataResult = _webMmoRepository.GetKeyNews();
            return dataResult;
        }
        #endregion

    }
}
