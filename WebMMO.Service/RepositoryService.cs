﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using WebMMO.Service.Services;
using WebMMO.Service.ServicesImpl;

namespace WebMMO.Service
{
    public sealed class ServicesManager
    {
        private readonly ConcurrentDictionary<string, object> _services = new ConcurrentDictionary<string, object>();
        private static readonly Lazy<ServicesManager> Manager = new Lazy<ServicesManager>(() => new ServicesManager());

        public static ServicesManager RepositoryService => Manager.Value;

        private ServicesManager()
        {

        }

        private T GetService<T>()
        {
            if (!typeof(IBaseService).IsAssignableFrom(typeof(T)))
            {
                throw new TypeAccessException("Only IBaseService accepted");
            }
            var type = typeof(T).FullName;
            if (_services.ContainsKey(type))
            {
                return (T)_services[type];
            }

            var service = new Lazy<T>(LazyThreadSafetyMode.PublicationOnly);
            _services.TryAdd(type, service.Value);
            return service.Value;
        }

        #region Service register

        private IUserService _userService;
        public IUserService UserService => _userService ?? (_userService = GetService<UserService>());

        private IRoleService _roleSeService;
        public IRoleService RoleService => _roleSeService ?? (_roleSeService = GetService<RoleService>());

        private IAppSettingService _appSettingService;
        public IAppSettingService AppSettingService => _appSettingService ?? (_appSettingService = GetService<AppSettingService>());

        private IMenuService _menuService;
        public IMenuService MenuService => _menuService ?? (_menuService = GetService<MenuService>());

        private IOrganizationService _organizationService;
        public IOrganizationService OrganizationService => _organizationService ?? (_organizationService = GetService<OrganizationService>());

        private IWebMmoService _WebMMOService;
        public IWebMmoService WebMMOService => _WebMMOService ?? (_WebMMOService = GetService<WebMmoService>());

        private IAppSystemService _appSystemService;
        public IAppSystemService AppSystemService => _appSystemService ?? (_appSystemService = GetService<AppSystemService>());

        private ISSHSystemService _sSHSystemService;
        public ISSHSystemService SSHSystemService => _sSHSystemService ?? (_sSHSystemService = GetService<SSHSystemService>());

        #endregion


    }

    public sealed class ApiServicesManager
    {
        private readonly ConcurrentDictionary<string, object> _services = new ConcurrentDictionary<string, object>();
        private static readonly Lazy<ApiServicesManager> Manager = new Lazy<ApiServicesManager>(() => new ApiServicesManager());

        public static ApiServicesManager RepositoryService => Manager.Value;

        private ApiServicesManager()
        {

        }

        private T GetService<T>()
        {
            if (!typeof(IBaseApiService).IsAssignableFrom(typeof(T)))
            {
                throw new TypeAccessException("Only IBaseApiService accepted");
            }
            var type = typeof(T).FullName;
            if (_services.ContainsKey(type))
            {
                return (T)_services[type];
            }

            var service = new Lazy<T>(LazyThreadSafetyMode.PublicationOnly);
            _services.TryAdd(type, service.Value);
            return service.Value;
        }

        #region Service register


        #endregion
    }
}
