﻿using System;
using System.Collections.Generic;
using WebMMO.Core.DomainModels.devices;
using WebMMO.Core.DomainModels.Gmail;
using WebMMO.Core.DomainModels.ImageSystem;
using WebMMO.Core.DomainModels.Sms;
using WebMMO.Core.DomainModels.WebMMO;
using WebMMO.Core.MessageResponse;

namespace WebMMO.Service.Services
{
    public interface IWebMmoService : IViewWebMmoService, IUpdateWebMMOService
    {
        List<ImageCategoriesDto> GetAllCategories();
        List<ImageSubCategoriesDto> GetAllSubCategories();
        List<ImageSubCategoriesDto> GetSubCategoriesByGroup(int categoriesId);
        GmailStatictisResult GmailStatistics(string token);
        int DevicesPush(string deviceinfo);
        List<ImageListDto> GetAllImagesSync(int status);
        DataTableViewModel<ImageListResult> SearchImages(FilterTableParams tableParams, string name, string Status, int Categories, int SubCategories);
        DevicesInfoDto DevicesGet();
        List<ImageCategoriesDto> GetAppCategories(string appPackage);
        ImageListDto GetWebMmoByGuid(Guid valueGuid);
        List<ImageSubCategoriesDto> GetAppSubCategories(string appPackage, int categoriesId);
        ApiKeyDto GetTop1Key();
        SmsOnlineSimResult GetNumber(string country, string services);
        List<SmsOnlineSimResult> GetOperations();
        AccessTokenUserDto CheckToken(string token);
        int UpdateBalanceApiKey(AccessTokenUserDto chkCheckApi);
        SmsOnlineSimResult GetNumberActivateRu(string country, string services);
      
        AccessTokenUserDto GetAccessTokenUserGuid(string key);
        void DevicesFakeInsert(DevicesFakeInfoDto objFakeInfo);
        int AddAndEditAccessTokenUser(AccessTokenUserDto objNew);
        DataTableViewModel<AccessTokenUserDto> SearchManageKey(FilterTableParams tableParams, string name);
        int GmailPush(GmailInfoDto objGmailPush);
        int AddAndEditSmsPrice(SmsPriceDto objNew);
        SmsPriceDto GetSmsPriceById(int id);
        DataTableViewModel<SmsPriceDto> SearchManagePriceSms(FilterTableParams tableParams, string name);
        List<GmailInfoDto> GmailGet(string token, string emailtype, int amount);
        GmailPriceDto GmailPriceGetByType(string type);
        List<GmailInfoDto> GmailGetByKey(string token, string type, int amount);
        List<CitiesCodeDto> GetCitiesCode(string countryCode);
        List<string> GetKeyNews();
    }

    public interface IViewWebMmoService : IBaseService
    {
        ImageCategoriesDto GetCategoriesByGuid(Guid value);
        ImageSubCategoriesDto GetSubCategoriesByGuid(Guid value);
        DataTableViewModel<ImageCategoriesResult> SearchCategories(FilterTableParams tableParams, string name);
        DataTableViewModel<ImageSubCategoriesResult> SearchSubCategories(FilterTableParams tableParams, string name, string Status, int Categories);
    }

    public interface IUpdateWebMMOService : IBaseService
    {
        int AddAndEdit(ImageCategoriesDto model);
        int AddAndEditSubCategories(ImageSubCategoriesDto objNew);
        int AddAndEditImageSearch(ImageListDto objInsert);
        int UpdateCountApiKey(int iD);
        int RefreshApiKey();
        int AddAndEditSearchLog(SearchLogDto objSearch);
    }
}
