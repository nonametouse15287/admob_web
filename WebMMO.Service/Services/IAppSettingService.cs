﻿using WebMMO.Core.DomainModels.Appsetting;
using WebMMO.Core.MessageResponse;

namespace WebMMO.Service.Services
{
    public interface IAppSettingService : IViewAppSettingService, IUpdateAppSettingService
    {

    }

    public interface IViewAppSettingService : IBaseService
    {
        ServiceResponse<AppSettingDto> GetAppSettingByName(string isUnderMaintenanceProperty);
    }

    public interface IUpdateAppSettingService : IBaseService
    {
        ServiceResponse<int> UpdateAppSetting(AppSettingDto appSetting);
    }
}
