﻿using WebMMO.Core.MessageResponse;
using WebMMO.Core.DomainModels.SSH;
using System.Collections.Generic;
using WebMMO.Core.DomainModels.Email;
using WebMMO.Core.DomainModels.Youtube;
using WebMMO.Core.DomainModels.Appsetting;

namespace WebMMO.Service.Services
{
    public interface ISSHSystemService : IViewSSHSystemService, IUpdateSSHSystemService
    {
        DataTableViewModel<SSHWareHouseDto> SearchSSHSystem(FilterTableParams tableParams);
        SSHWareHouseDto GetSSHSystem(string accessToken, string countryCode);
        List<SSHWareHouseDto> GetTopSSH();
        void UpdateStatusSSH(SSHWareHouseDto item);
        void ResetSSH();
        GmailEmailResult GetGmailNoneProfiles(string accessToken, string countryCode);
        QuanLyKenhYoutubeResult GetYoutubeInfo(string accessToken, string countryCode);
        int UpdateGmailStatus(int id, int? statusprofile, string description);
        List<ArticlesAdmodDto> GetArticlesAdmod(string countryCode);
    }

    public interface IViewSSHSystemService : IBaseService
    {

    }

    public interface IUpdateSSHSystemService : IBaseService
    {
        int InsertSShByList(List<SSHWareHouseDto> lstInsert);
    }
}
