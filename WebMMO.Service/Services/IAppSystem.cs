﻿
using System;
using WebMMO.Core.DomainModels.AppManage;
using WebMMO.Core.DomainModels.Appsetting;
using WebMMO.Core.MessageResponse;

namespace WebMMO.Service.Services
{
    public interface IAppSystemService : IViewAppSystemService, IUpdateAppSystemService
    {
        AppPermissionDto AppPermissionByGuid(Guid value);
        DataTableViewModel<ArticlesAdmodDto> SearchArticlesAdmod(FilterTableParams tableParams, string name);
        ArticlesAdmodDto ArticlesAdmodById(int value);
        int AddAndEditArticlesAdmod(ArticlesAdmodDto objNew);
    }

    public interface IViewAppSystemService : IBaseService
    {
        DataTableViewModel<AppPermissionResult> SearchAppSystems(FilterTableParams tableParams, string name);
    }

    public interface IUpdateAppSystemService : IBaseService
    {
        int AddAndEdit(AppPermissionDto objNew);
    }
}
