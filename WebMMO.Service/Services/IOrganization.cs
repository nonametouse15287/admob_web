﻿
using System.Collections.Generic;
using WebMMO.Core.DomainModels.Organization;
using WebMMO.Core.MessageResponse;

namespace WebMMO.Service.Services
{
    public interface IOrganizationService : IViewOrganizationService, IUpdateOrganizationService
    {

    }

    public interface IViewOrganizationService : IBaseService
    {
        ServiceResponse<OrganizationDto> GetOrganizationById(int id);
        ServiceResponse<IEnumerable<OrganizationDto>> GetOrganizationByCompanyCode(string firstLetter);
    }

    public interface IUpdateOrganizationService : IBaseService
    {
        ServiceResponse<int> UpdateOrganization(OrganizationDto organization);
    }
}
