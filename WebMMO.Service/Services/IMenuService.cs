﻿using System.Collections.Generic;
using WebMMO.Core.DomainModels.Menu;
using WebMMO.Core.MessageResponse;

namespace WebMMO.Service.Services
{
    public interface IMenuService : IViewMenuService, IUpdateMenuService
    {
       
    }

    public interface IViewMenuService : IBaseService
    {
        ServiceResponse<IEnumerable<MenuDto>> GetMenuList(int? menuParentId, int? Seq);
        ServiceResponse<MenuDto> GetMenuById(int value);
        ServiceResponse<IEnumerable<RoleMenuDto>> GetRoleMenu(int? menuId, int? roleId);
        ServiceResponse<IEnumerable<MenuDto>> GetMenuByParentId(int parentId);
        ServiceResponse<IEnumerable<MenuDto>> GetMenu_HIAS_GET_MENUACCESS(int id, int? intMenuParentID);
    }

    public interface IUpdateMenuService : IBaseService
    {
        ServiceResponse<int> UpdateMenu(MenuDto menu);
        ServiceResponse<int> DeleteRoleMenu(int id);
        ServiceResponse<int> InsertRoleMenu(RoleMenuDto newaccess);
        ServiceResponse<int> InsertMenu(MenuDto menu);
        ServiceResponse<int> DeleteMenu(int menuId);
    }
}
