﻿using System.Collections.Generic;
using WebMMO.Core.DomainModels.Role;
using WebMMO.Core.DomainModels.User;
using WebMMO.Core.MessageResponse;

namespace WebMMO.Service.Services
{
    public interface IUserService : IViewUserService, IUpdateUserService
    {
       
    }

    public interface IViewUserService : IBaseService
    {
        ServiceResponse<UserDto> GetUserById(int id);

        ServiceResponse<UserDto> GetUserByUserName(string userName);

        ServiceResponse<UserDto> GetUserByEmail(string email);

        ServiceResponse<ActivateUserRequestDto> GetActivateUserByKey(string id);

        ServiceResponse<ActivateUserRequestDto> GetActivateUserByUserId(int id);
        ServiceResponse<IEnumerable<ActivateUserRequestDto>> GetActivateUserAll();
        ServiceResponse<ResetPasswordRequestDto> GetResetPasswordByKey(string id);
        ServiceResponse<bool> CheckEmailUser(string newEmailAddress);

        ServiceResponse<IEnumerable<UserRoleDto>> GetUserRolesByUserId(int userId);
        ServiceResponse<IEnumerable<UserRoleDto>> GetUserRolesById(int userId, int availableRoleId);
        ServiceResponse<IEnumerable<UserResult>> GetQueryCbUser(int? organizationId, string status, int? roleId);
        ServiceResponse<IEnumerable<StateDto>> GetStateByCountryId(int countryIdValue);
    }

    public interface IUpdateUserService : IBaseService
    {
        ServiceResponse<int> CreateNewUser(object o);
        ServiceResponse<int> UpdatePassword(UserDto user);
        ServiceResponse<int> UpdateLastLoginDate(string email);
        ServiceResponse<int> UpdateUser(UserDto user);
        ServiceResponse<int> RemoveActivateUserRequest(ActivateUserRequestDto activateUserRequest);
        ServiceResponse<int> RemoveResetPassword(ResetPasswordRequestDto resetPasswordRequest);
        ServiceResponse<int> InsertResetPasswordRequest(ResetPasswordRequestDto resetPasswordRequest);
        ServiceResponse<int> AddUser(UserDto user);
        ServiceResponse<int> AddUserRole(UserRoleDto userrole);
        ServiceResponse<int> DeleteUserRole(UserRoleDto currentRole);
        ServiceResponse<int> DeleteUserRoleByUserId(int userId);
        ServiceResponse<int> UpdateActiveUserRequest(ActivateUserRequestDto activateUserRequest);
    }
}
