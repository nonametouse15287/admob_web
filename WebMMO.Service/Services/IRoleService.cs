﻿using System.Collections.Generic;
using WebMMO.Core.DomainModels.Permission;
using WebMMO.Core.DomainModels.Role;
using WebMMO.Core.MessageResponse;

namespace WebMMO.Service.Services
{
    public interface IRoleService : IViewRoleService, IUpdateRoleService
    {

    }

    public interface IViewRoleService : IBaseService
    {
        ServiceResponse<string> GetRoleByUserId(int id);

        ServiceResponse<int> GetHighesRoleRankByUserId(int id);

        ServiceResponse<RoleDto> GetRoleById(int roleRoleId);
        ServiceResponse<IEnumerable<RoleDto>> GetRoleAll();
        ServiceResponse<IEnumerable<RoleDto>> GetRoleQuery(bool isSysAdmin, bool isExternal, int rolerank);
        ServiceResponse<IEnumerable<RolePermissionDto>> GetRolePermissionById(int? id, int? roleid, int? permissionId);
        ServiceResponse<IEnumerable<PermissionDto>> GetPermission(int? id, int? menuId, string permissionDescription, string name);
    }

    public interface IUpdateRoleService : IBaseService
    {
        ServiceResponse<int> InsertPermission(PermissionDto permissionDto);
        ServiceResponse<int> InsertRoles(RoleDto objNew);
        ServiceResponse<int> UpdateRole(RoleDto currentRole);
        ServiceResponse<int> DeleteRolePermissionById(int id);
        ServiceResponse<int> DeletePermissionById(int permissionId);
        ServiceResponse<int> InsertRolePermission(RolePermissionDto rolePermission);
        ServiceResponse<int> UpdatePermission(PermissionDto permission);
        ServiceResponse<bool> CheckPermission(int userID, string strPermission);
    }
}
