﻿using System.Collections.Generic;
using System.Linq;
using WebMMO.Core.DomainModels.Permission;
using WebMMO.Core.DomainModels.Role;
using WebMMO.Core.IRepositories;
using Dapper;

namespace WebMMO.Infrastructures.Dapper.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        public IEnumerable<RoleDto> GetRoleByUserId(int id)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"SELECT * FROM [Role] where ID  in (SELECT RoleID FROM [UserRole] WHERE UserID = @UserID);";

                return dbContext.Query<RoleDto>(query, new { @UserID = id }).ToList();

            }
        }

        public RoleDto GetRoleById(int roleRoleId)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"SELECT * FROM [Role] where ID  = @ID;";

                return dbContext.Query<RoleDto>(query, new { @ID = roleRoleId }).FirstOrDefault();

            }
        }

        public int InsertPermission(PermissionDto permissionDto)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"INSERT INTO Permission(Name, PermissionDescription, MenuID, IsDefault) 
                                VALUES (@Name, @PermissionDescription, @MenuID, @IsDefault);
                                SELECT CAST(SCOPE_IDENTITY() as int);";

                return dbContext.Query<int>(query, permissionDto).FirstOrDefault();

            }
        }

        public IEnumerable<RolePermissionDto> GetRolePermissionById(int? id, int? roleid, int? permissionId)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string strWhere = "";

                if (id != null)
                {
                    if (strWhere.Length > 0)
                    {
                        strWhere = strWhere + " AND ";
                    }
                    strWhere = strWhere + " Id=@id ";
                }

                if (roleid != null)
                {
                    if (strWhere.Length > 0)
                    {
                        strWhere = strWhere + " AND ";
                    }
                    strWhere = strWhere + " roleid=@roleid ";
                }

                if (permissionId != null)
                {
                    if (strWhere.Length > 0)
                    {
                        strWhere = strWhere + " AND ";
                    }
                    strWhere = strWhere + " permissionId=@permissionId ";
                }

                if (strWhere.Length > 0)
                {
                    strWhere = " WHERE " + strWhere;
                }

                string query = $@"SELECT * FROM  RolePermission {strWhere}";

                return dbContext.Query<RolePermissionDto>(query, new
                {
                    @id = id,
                    @roleid = roleid,
                    @permissionId = permissionId
                });
            }
        }

        public IEnumerable<RoleDto> GetRoleAll()
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"SELECT * FROM  Role ";

                return dbContext.Query<RoleDto>(query);
            }
        }

        public int InsertRoles(RoleDto objNew)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"INSERT INTO Role(Name, Description, IsSysAdmin, IsExternal, RankLevel, IsAuditor, JpName, DashboardID) 
                                VALUES (@Name, @Description, @IsSysAdmin, @IsExternal, @RankLevel, @IsAuditor, @JpName, @DashboardID);
                                SELECT CAST(SCOPE_IDENTITY() as int);";
                return dbContext.Query<int>(query, objNew).FirstOrDefault();
            }
        }

        public int UpdateRole(RoleDto currentRole)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"UPDATE Role SET Name = @Name, Description = @Description, IsSysAdmin = @IsSysAdmin, IsExternal = @IsExternal, RankLevel = @RankLevel, IsAuditor = @IsAuditor, JpName = @JpName WHERE ID = @ID ";
                return dbContext.Execute(query, currentRole);
            }
        }

        public IEnumerable<RoleDto> GetRoleQuery(bool isSysAdmin, bool isExternal, int rolerank)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"SELECT * FROM  Role WHERE  isSysAdmin = @isSysAdmin AND isExternal = @isExternal AND RankLevel > @rolerank ORDER BY Ranklevel asc;";

                return dbContext.Query<RoleDto>(query, new { @isSysAdmin = isSysAdmin, @isExternal = isExternal, @rolerank = rolerank });
            }
        }

        public IEnumerable<RolePermissionDto> GetRolePermissionByPermissionId(int permissionId)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"SELECT * FROM  RolePermission WHERE  PermissionID = @permissionId";

                return dbContext.Query<RolePermissionDto>(query, new { @permissionId = permissionId });
            }
        }

        public int DeleteRolePermissionById(int id)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"DELETE FROM  RolePermission WHERE  ID = @ID";

                return dbContext.Execute(query, new { @ID = id });
            }
        }

        public int DeletePermissionById(int permissionId)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"DELETE FROM Permission WHERE  ID = @ID";

                return dbContext.Execute(query, new { @ID = permissionId });
            }
        }

        public IEnumerable<PermissionDto> GetPermission(int? id, int? menuId, string permissionDescription, string name)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string strWhere = "";

                if (id != null)
                {
                    if (strWhere.Length > 0)
                    {
                        strWhere = strWhere + " AND ";
                    }
                    strWhere = strWhere + " Id=@id ";
                }

                if (menuId != null)
                {
                    if (strWhere.Length > 0)
                    {
                        strWhere = strWhere + " AND ";
                    }
                    strWhere = strWhere + " menuId=@menuId ";
                }

                if (!string.IsNullOrEmpty(permissionDescription))
                {
                    if (strWhere.Length > 0)
                    {
                        strWhere = strWhere + " AND ";
                    }
                    strWhere = strWhere + " permissionDescription=@permissionDescription ";
                }

                if (!string.IsNullOrEmpty(name))
                {
                    if (strWhere.Length > 0)
                    {
                        strWhere = strWhere + " AND ";
                    }
                    strWhere = strWhere + " name=@name ";
                }

                if (strWhere.Length > 0)
                {
                    strWhere = " WHERE " + strWhere;
                }

                string query = $@"SELECT * FROM Permission {strWhere}";

                return dbContext.Query<PermissionDto>(query, new
                {
                    @id = id,
                    @menuId = menuId,
                    @permissionDescription = permissionDescription,
                    @name = name
                });
            }
        }

        public int InsertRolePermission(RolePermissionDto rolePermission)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"INSERT INTO RolePermission(RoleID, PermissionID) VALUES (@RoleID, @PermissionID);
                                    SELECT CAST(SCOPE_IDENTITY() as int);";
                return dbContext.Query<int>(query, rolePermission).FirstOrDefault();
            }
        }

        public int UpdatePermission(PermissionDto permission)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"UPDATE Permission SET Name = @Name, PermissionDescription = @PermissionDescription, MenuID = @MenuID, IsDefault = @IsDefault WHERE ID = @ID ;";
                return dbContext.Execute(query, permission);
            }
        }

        public bool CheckPermission(int userId, string strPermission)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"SELECT COUNT(1) FROM [User] t1, [UserRole] t2, [RolePermission] t3, [Permission] t4 
                                WHERE t1.ID = t2.UserID AND t2.RoleID =t3.RoleID AND t3.PermissionID = t4.ID
                                AND t1.ID= @UserID AND t4.PermissionDescription=@PermissionDesc;";
                int xxx = dbContext.Query<int>(query, new { @UserID = userId, @PermissionDesc = strPermission }).FirstOrDefault();
                return xxx > 0;
            }
        }
    }
}
