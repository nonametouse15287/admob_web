﻿using System;
using System.Collections.Generic;
using WebMMO.Core.IRepositories;
using WebMMO.Core.MessageResponse;
using WebMMO.Infrastructures.Dapper.SQLServer;
using Dapper;
using System.Linq;
using WebMMO.Core.DomainModels.SSH;
using WebMMO.Common.Helpes;
using WebMMO.Core.DomainModels.Email;
using WebMMO.Core.DomainModels.Youtube;
using WebMMO.Core.DomainModels.Appsetting;

namespace WebMMO.Infrastructures.Dapper.Repositories
{
    public class SSHSystemRepository : ISSHSystemRepository
    {
        public GmailEmailResult GetGmailNoneProfiles(string accessToken, string countryCode)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                GmailEmailResult objResult = new GmailEmailResult();
                string sql = $@"SELECT TOP 1 * FROM GmailEmail where statusprofile = {ConfigHelpers.StatusPending} order by createddate asc;";
                var result = dbContext.Query<GmailEmailDto>(sql).FirstOrDefault();
                if (result != null)
                {
                    UpdateGmailNoneProfiles(result);
                    objResult.Id = result.Id;
                    objResult.email = result.email;
                    objResult.password = result.password;
                    objResult.statusprofile = result.statusprofile;
                    objResult.statusdelete = result.statusdelete;
                    objResult.description = result.description;
                    objResult.fulltextinput = result.fulltextinput;
                }
                else
                {
                    objResult = null;
                }

                return objResult;
            }
        }

        public int UpdateGmailNoneProfiles(GmailEmailDto objUpdate)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                objUpdate.createddate = DateTime.UtcNow;
                string sql = $@"Update GmailEmail set statusprofile = @statusprofile, statusdelete = @statusdelete,
                        createddate =@createddate, createdby=@createdby, description=@description WHERE Id=@Id;";
                return dbContext.Execute(sql, objUpdate);
            }
        }

        public List<SSHWareHouseDto> GetSSHSystem()
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string sql = $@"SELECT TOP 30 * FROM SSHWareHouse where Status = {ConfigHelpers.StatusPending};";
                return dbContext.Query<SSHWareHouseDto>(sql).ToList();
            }
        }

        public SSHWareHouseDto GetSSHSystem(string accessToken, string countryCode)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string sql = $@"SELECT TOP 1 * FROM SSHWareHouse WHERE [CountryCode]  like N'%('+@CountryCode+')%'  and  status = 4 order by CreatedDate asc ";
                var result = dbContext.Query<SSHWareHouseDto>(sql,
                    new
                    {
                        @CountryCode = countryCode,
                        @AccessToken = accessToken
                    }).FirstOrDefault();

                if (result != null)
                {
                    result.Status = 0;
                    result.CreatedDate = DateTime.UtcNow;
                    UpdateStatusSSH(result);
                }

                return result;
            }
        }

        public int InsertSShByList(List<SSHWareHouseDto> lstInsert)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string sql = "";
                foreach (var item in lstInsert)
                {
                    sql = "SELECT * FROM [dbo].[SSHWareHouse] WHERE IpAddress = @IpAddress";
                    SSHWareHouseDto obj = dbContext.Query<SSHWareHouseDto>(sql, item).FirstOrDefault();
                    if (obj == null)
                    {
                        sql = $@"INSERT INTO [dbo].[SSHWareHouse]
                               (username,password,IpAddress,CountryCode,CreatedDate,Status,FullText)
                         VALUES
	                     (@username,@password,@IpAddress,@CountryCode,@CreatedDate,@Status,@FullText)";

                        var result = dbContext.Execute(sql, item);
                    }
                }

                return 0;
            }
        }

        public void ResetSSH()
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                DateTime dateCheck = DateTime.UtcNow.AddHours(-12);
                string sql = $@"UPDATE SSHWareHouse Set Status = 0 where Status = {ConfigHelpers.StatusApproved} and CreatedDate < @dateCheck;";
                dbContext.Execute(sql, new { @dateCheck = dateCheck });
            }
        }

        public Tuple<int, List<SSHWareHouseDto>> SearchSSHSystem(FilterTableParams tableParams)
        {
            /*WHERE*/
            /*paging*/
            var fetchStr = $" OFFSET {tableParams.Offset} ROWS FETCH NEXT {tableParams.PageSize} ROWS ONLY";
            /*Sort*/
            var sortStr = string.Empty;
            switch (tableParams.SortColum)
            {
                case 0:
                    sortStr = " ORDER BY CreatedDate " + tableParams.SortDirection;
                    break;
                default:
                    sortStr = " ORDER BY CreatedDate " + tableParams.SortDirection;
                    break;
            }

            var sql = $@"
                                WITH tableResult(Id,username,password,IpAddress,CountryCode,CreatedDate)
                                AS ( 
                                     SELECT Id,username,password,IpAddress,CountryCode,CreatedDate
                                     FROM [dbo].[SSHWareHouse]                                      
                                 )

                                 SELECT * FROM tableResult {sortStr} {fetchStr};

                                  SELECT COUNT(1)
                                     FROM [dbo].[SSHWareHouse]  ;";

            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                var result = dbContext.QueryMultiple(sql);

                var list = result.Read<SSHWareHouseDto>().ToList();
                var total = result.Read<int>().FirstOrDefault();
                var tuple = new Tuple<int, List<SSHWareHouseDto>>(total, list);
                return tuple;
            }



        }

        public void UpdateStatusSSH(SSHWareHouseDto item)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                item.CreatedDate = DateTime.UtcNow;
                string sql = $@"UPDATE SSHWareHouse Set CreatedDate=@CreatedDate,Status=@Status where id = @Id;";
                dbContext.Execute(sql, item);
            }
        }

        public QuanLyKenhYoutubeResult GetYoutubeInfo()
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string sql = $@"SELECT TOP 1 * FROM QuanLyKenhYoutube where Status = {ConfigHelpers.StatusApproved} order by createddate asc;";
                var result = dbContext.Query<QuanLyKenhYoutubeResult>(sql).FirstOrDefault();
                return result;
            }
        }

        public int UpdateGmailStatus(int id, int? statusprofile, string description)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                GmailEmailResult objResult = new GmailEmailResult();
                string sql = $@"SELECT * FROM GmailEmail where Id = @Id;";
                var result = dbContext.Query<GmailEmailDto>(sql, new { @Id = id }).FirstOrDefault();
                if (result != null)
                {
                    result.statusprofile = statusprofile;
                    if (result.statusprofile == ConfigHelpers.StatusDeleted)
                    {
                        result.statusprofile = ConfigHelpers.StatusDeleted;
                    }
                    result.description = description;
                    UpdateGmailNoneProfiles(result);
                }
                return 1;
            }
        }

        public List<ArticlesAdmodDto> GetArticlesAdmod(string countryCode)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string sql = $@"SELECT [Id],[Title],[BodyContent],[CountryCode],[Status],[CreatedDate],[CreatedBy]
                            FROM [dbo].[ArticlesAdmod] where CountryCode = @CountryCode Order by CreatedDate desc;";
                return dbContext.Query<ArticlesAdmodDto>(sql, new { @countryCode = countryCode }).ToList();
            }
        }
    }
}
