﻿using System.Linq;
using WebMMO.Core.DomainModels.Appsetting;
using WebMMO.Core.IRepositories;
using Dapper;

namespace WebMMO.Infrastructures.Dapper.Repositories
{
    public class AppSettingRepository : IAppSettingRepository
    {
        public AppSettingDto GetAppSettingByName(string isUnderMaintenanceProperty)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"SELECT * FROM [AppSetting] where PropertyName = @PropertyName";

                return (dbContext.Query<AppSettingDto>(query, new { @PropertyName = isUnderMaintenanceProperty })).FirstOrDefault();

            }
        }

        public int UpdateAppSetting(AppSettingDto appSetting)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"UPDATE AppSetting SET PropertyName = @PropertyName, PropertyDescription = @PropertyDescription, Value = @Value WHERE ID = @ID ";

                return dbContext.Execute(query, appSetting);

            }
        }
    }
}
