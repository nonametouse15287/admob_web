﻿using System.Collections.Generic;
using System.Linq;
using WebMMO.Core.DomainModels.Organization;
using WebMMO.Core.IRepositories;
using Dapper;

namespace WebMMO.Infrastructures.Dapper.Repositories
{
    public class OrganizationRepository : IOrganizationRepository
    {
        public OrganizationDto GetOrganizationById(int id)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"SELECT * FROM [Organization] where ID = @ID";

                return (dbContext.Query<OrganizationDto>(query, new { @ID = id })).FirstOrDefault();
            }
        }

        public List<OrganizationDto> GetOrganizationByCompanyCode(string firstLetter)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"SELECT * FROM [Organization] where CompanyCode like N'@CompanyCode%'";

                return (dbContext.Query<OrganizationDto>(query, new { @CompanyCode = firstLetter })).ToList();
            }
        }

        public int UpdateOrganization(OrganizationDto organization)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"UPDATE Organization SET Name = @Name, CbCode = @CbCode, BusinessRegNumber = @BusinessRegNumber, 
                        Street1 = @Street1, Street2 = @Street2, Postcode = @Postcode, City = @City, StateID = @StateID, CountryID = @CountryID, 
                        ContactNumber = @ContactNumber, FaxNumber = @FaxNumber, BusinessTypeID = @BusinessTypeID, ApplicationTypeID = @ApplicationTypeID,
                        CompanySizeID = @CompanySizeID, IndustryTypeID = @IndustryTypeID, Website = @Website, Logo = @Logo, HalalLogo = @HalalLogo, 
                        SampleCertificate = @SampleCertificate, Status = @Status, DateCreated = @DateCreated, DateModified = @DateModified, 
                        CreatedByUserID = @CreatedByUserID, ModifiedByUserID = @ModifiedByUserID, VatNo = @VatNo, TradingName = @TradingName, 
                        CompanyEmail = @CompanyEmail, OperatingHours = @OperatingHours, CompanyCode = @CompanyCode, ECNumber = @ECNumber, 
                        FactoryStreet1 = @FactoryStreet1, FactoryStreet2 = @FactoryStreet2, FactoryPostCode = @FactoryPostCode, FactoryCity = @FactoryCity, 
                        FactoryStateID = @FactoryStateID, FactoryCountryID = @FactoryCountryID, FactoryName = @FactoryName, 
                        AlwaysOpen = @AlwaysOpen, NoofEmployees = @NoofEmployees, HCMSID = @HCMSID, LogoPath = @LogoPath, HalalLogoPath = @HalalLogoPath, 
                        LastSyncDate = @LastSyncDate WHERE ID = @ID ";

                return dbContext.Execute(query, organization);
            }
        }
    }
}
