﻿using System;
using System.Collections.Generic;
using WebMMO.Core.DomainModels.AppManage;
using WebMMO.Core.IRepositories;
using WebMMO.Core.MessageResponse;
using WebMMO.Infrastructures.Dapper.SQLServer;
using WebMMO.Common.Helpes;
using Dapper;
using System.Linq;
using WebMMO.Core.DomainModels.Appsetting;

namespace WebMMO.Infrastructures.Dapper.Repositories
{
    public class AppSystemRepository : IAppSystemRepository
    {
        public int AddAndEdit(AppPermissionDto model)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@"INSERT INTO [dbo].[AppPermission]
                               (Guid,AppName,AppPackage,ListPermission,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,Status)
                                VALUES
                               (@Guid,@AppName,@AppPackage,@ListPermission,@CreatedDate,@CreatedBy,@ModifiedDate,@ModifiedBy,@Status)";

                if (model.ID > 0)
                {
                    query = $@"UPDATE [dbo].[AppPermission]
                                   SET [Guid] = @Guid
                                      ,[AppName] = @AppName
                                      ,[AppPackage] = @AppPackage
                                      ,[ListPermission] = @ListPermission
                                      ,[CreatedDate] = @CreatedDate
                                      ,[CreatedBy] = @CreatedBy
                                      ,[ModifiedDate] = @ModifiedDate
                                      ,[ModifiedBy] = @ModifiedBy
                                      ,[Status] = @Status
                                 WHERE ID = @ID";
                }

                return dbContext.Execute(query, model);
            }
        }

        public int AddAndEditArticlesAdmod(ArticlesAdmodDto model)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@"INSERT INTO [dbo].[ArticlesAdmod]
                               (Title,BodyContent,CountryCode,Status,CreatedDate,CreatedBy)
                                VALUES
                              (@Title,@BodyContent,@CountryCode,@Status,@CreatedDate,@CreatedBy)";

                if (model.Id > 0)
                {
                    query = $@"UPDATE [dbo].[ArticlesAdmod]
                               SET [Title] = @Title
                                  ,[BodyContent] = @BodyContent
                                  ,[CountryCode] = @CountryCode
                                  ,[Status] = @Status
                                  ,[CreatedDate] = @CreatedDate
                                  ,[CreatedBy] = @CreatedBy
                             WHERE Id = @Id";
                }

                return dbContext.Execute(query, model);
            }
        }

        public AppPermissionDto AppPermissionByGuid(Guid value)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string sql = $@"SELECT ID,Guid,AppName,AppPackage,ListPermission,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,Status
                                    FROM [dbo].[AppPermission] WHERE Guid = @Guid";

                var result = dbContext.Query<AppPermissionDto>(sql, new { @Guid = value }).FirstOrDefault();

                return result;
            }
        }

        public ArticlesAdmodDto ArticlesAdmodById(int value)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string sql = $@"SELECT [Id]
                                  ,[Title]
                                  ,[BodyContent]
                                  ,[CountryCode]
                                  ,[Status]
                                  ,[CreatedDate]
                                  ,[CreatedBy]
                              FROM [dbo].[ArticlesAdmod] WHERE Id = @Id";

                var result = dbContext.Query<ArticlesAdmodDto>(sql, new { @Id = value }).FirstOrDefault();

                return result;
            }
        }

        public Tuple<int, List<AppPermissionResult>> SearchAppSystems(FilterTableParams tableParams, string name)
        {
            var searchParam = !string.IsNullOrWhiteSpace(name)
                             ? "SET @SearchQuery = @Search;"
                             : string.Empty;

            var searchStr = !string.IsNullOrWhiteSpace(name)
                        ? @" ((AppName LIKE '%'+@SearchQuery+'%')  OR (AppPackage LIKE '%'+@SearchQuery+'%'))"
                        : string.Empty;


            /*WHERE*/
            if (searchStr.Length > 0)
            {
                if (searchStr.Length > 0)
                {
                    searchStr = " AND " + searchStr;
                }

                searchStr = $@" WHERE Status <> {ConfigHelpers.StatusDeleted}" + searchStr;
            }
            /*paging*/
            var fetchStr = $" OFFSET {tableParams.Offset} ROWS FETCH NEXT {tableParams.PageSize} ROWS ONLY";
            /*Sort*/
            var sortStr = string.Empty;
            switch (tableParams.SortColum)
            {
                case 0:
                    sortStr = " ORDER BY CreatedDate " + tableParams.SortDirection;
                    break;
                default:
                    sortStr = " ORDER BY CreatedDate " + tableParams.SortDirection;
                    break;
            }

            var sql = $@" DECLARE @SearchQuery NVARCHAR(20) = NULL; 
                            {searchParam}
                            WITH tableResult(ID,Guid,AppName,AppPackage,ListPermission,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,Status)
                            AS ( 
                                 SELECT ID,Guid,AppName,AppPackage,ListPermission,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,Status
                                        FROM [dbo].[AppPermission]
                                   {searchStr}
                             )

                             SELECT * FROM tableResult {sortStr} {fetchStr};

                             SELECT COUNT(1)
                                  FROM dbo.AppPermission 
                                    {searchStr};";

            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                var result = !string.IsNullOrWhiteSpace(name)
                    ? dbContext.QueryMultiple(sql, new { @Search = name })
                    : dbContext.QueryMultiple(sql);

                var list = result.Read<AppPermissionResult>().ToList();
                var total = result.Read<int>().FirstOrDefault();
                var tuple = new Tuple<int, List<AppPermissionResult>>(total, list);
                return tuple;
            }
        }

        public Tuple<int, List<ArticlesAdmodDto>> SearchArticlesAdmod(FilterTableParams tableParams, string name)
        {
            var searchParam = !string.IsNullOrWhiteSpace(name)
                              ? "SET @SearchQuery = @Search;"
                              : string.Empty;

            var searchStr = !string.IsNullOrWhiteSpace(name)
                        ? @" Title LIKE '%'+@SearchQuery+'%' "
                        : string.Empty;


            /*WHERE*/
            if (searchStr.Length > 0)
            {
                if (searchStr.Length > 0)
                {
                    searchStr = " AND " + searchStr;
                }

                searchStr = $@" WHERE Status <> {ConfigHelpers.StatusDeleted}" + searchStr;
            }
            /*paging*/
            var fetchStr = $" OFFSET {tableParams.Offset} ROWS FETCH NEXT {tableParams.PageSize} ROWS ONLY";
            /*Sort*/
            var sortStr = string.Empty;
            switch (tableParams.SortColum)
            {
                case 0:
                    sortStr = " ORDER BY CreatedDate " + tableParams.SortDirection;
                    break;
                default:
                    sortStr = " ORDER BY CreatedDate " + tableParams.SortDirection;
                    break;
            }

            var sql = $@" DECLARE @SearchQuery NVARCHAR(20) = NULL; 
                            {searchParam}
                            WITH tableResult(Id,Title,BodyContent,CountryCode,Status,CreatedDate,CreatedBy)
                            AS ( 
                                 SELECT Id,Title,BodyContent,CountryCode,Status,CreatedDate,CreatedBy
                              FROM [dbo].[ArticlesAdmod]
                                   {searchStr}
                             )

                             SELECT * FROM tableResult {sortStr} {fetchStr};

                             SELECT COUNT(1)
                                  FROM dbo.AppPermission 
                                    {searchStr};";

            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                var result = !string.IsNullOrWhiteSpace(name)
                    ? dbContext.QueryMultiple(sql, new { @Search = name })
                    : dbContext.QueryMultiple(sql);

                var list = result.Read<ArticlesAdmodDto>().ToList();
                var total = result.Read<int>().FirstOrDefault();
                var tuple = new Tuple<int, List<ArticlesAdmodDto>>(total, list);
                return tuple;
            }
        }
    }
}
