﻿using System.Collections.Generic;
using System.Linq;
using WebMMO.Core.IRepositories;
using Dapper;
using WebMMO.Core.DomainModels.WebMMO;
using WebMMO.Core.MessageResponse;
using System;
using WebMMO.Infrastructures.Dapper.SQLServer;
using WebMMO.Common.Helpes;
using WebMMO.Core.DomainModels.AppManage;
using WebMMO.Core.DomainModels.ImageSystem;
using WebMMO.Core.DomainModels.Sms;
using WebMMO.Core.DomainModels.Gmail;
using WebMMO.Core.DomainModels.devices;

namespace WebMMO.Infrastructures.Dapper.Repositories
{
    public class WebMMORepository : IWebMMORepository
    {
        public int AddAndEdit(ImageCategoriesDto model)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@"INSERT INTO dbo.ImageCategories
                                   (Guid,CategoriesName,CategoriesSearch,CategoriesElastic,CreatedDate
                                   ,CreatedBy,ModifiedDate,ModifedBy,Status,SyncElasticStatus)
                             VALUES
                                  (@Guid,@CategoriesName,@CategoriesSearch,@CategoriesElastic,@CreatedDate
                                   ,@CreatedBy,@ModifiedDate,@ModifedBy,@Status,@SyncElasticStatus)";

                if (model.ID > 0)
                {
                    query = $@"UPDATE dbo.ImageCategories
                               SET Guid = @Guid
                                  ,CategoriesName = @CategoriesName
                                  ,CategoriesSearch = @CategoriesElastic
                                  ,CategoriesElastic = @CategoriesElastic  
                                  ,ModifiedDate = @ModifiedDate
                                  ,ModifedBy = @ModifedBy
                                  ,Status = @Status
                                  ,SyncElasticStatus = @SyncElasticStatus
                             WHERE ID = @ID";
                }
                return dbContext.Execute(query, model);
            }
        }

        public int AddAndEditSubCategories(ImageSubCategoriesDto objNew)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@"INSERT INTO [dbo].[ImageSubCategories]
                               ([Guid],[SubCategoriesName],[SubCategoriesSearch],[SubCategoriesElastic],[CategoriesId]
                               ,[CreatedDate],[CreatedBy],[ModifiedDate],[ModifedBy]
                               ,[Status],[SyncElasticStatus])
                         VALUES
                               (@Guid,@SubCategoriesName,@SubCategoriesSearch
                               ,@SubCategoriesElastic,@CategoriesId,@CreatedDate
                               ,@CreatedBy,@ModifiedDate,@ModifedBy
                               ,@Status,@SyncElasticStatus)";

                if (objNew.ID > 0)
                {
                    query = $@"UPDATE [dbo].[ImageSubCategories]
                           SET [SubCategoriesName] = @SubCategoriesName
                              ,[SubCategoriesSearch] = @SubCategoriesSearch
                              ,[SubCategoriesElastic] = @SubCategoriesElastic
                              ,[CategoriesId] = @CategoriesId                              
                              ,[ModifiedDate] = @ModifiedDate
                              ,[ModifedBy] = @ModifedBy
                              ,[Status] = @Status
                              ,[SyncElasticStatus] = @SyncElasticStatus
                         WHERE ID = @ID";
                }
                return dbContext.Execute(query, objNew);
            }
        }

        public ImageCategoriesDto GetCategoriesByGuid(Guid value)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@"SELECT * FROM ImageCategories WHERE Guid = @Guid";
                return dbContext.Query<ImageCategoriesDto>(query, new { @Guid = value }).FirstOrDefault();
            }
        }

        public Tuple<int, List<ImageCategoriesResult>> SearchCategories(FilterTableParams tableParams, string name)
        {
            var searchParam = !string.IsNullOrWhiteSpace(name)
                              ? "SET @SearchQuery = @Search;"
                              : string.Empty;

            var searchStr = !string.IsNullOrWhiteSpace(name)
                        ? @" (CategoriesName LIKE '%'+@SearchQuery+'%')  "
                        : string.Empty;


            /*WHERE*/
            if (searchStr.Length > 0)
            {
                if (searchStr.Length > 0)
                {
                    searchStr = " AND " + searchStr;
                }

                searchStr = $@" WHERE Status <> {ConfigHelpers.StatusDeleted}" + searchStr;
            }
            /*paging*/
            var fetchStr = $" OFFSET {tableParams.Offset} ROWS FETCH NEXT {tableParams.PageSize} ROWS ONLY";
            /*Sort*/
            var sortStr = string.Empty;
            switch (tableParams.SortColum)
            {
                case 0:
                    sortStr = " ORDER BY CreatedDate " + tableParams.SortDirection;
                    break;
                default:
                    sortStr = " ORDER BY CreatedDate " + tableParams.SortDirection;
                    break;
            }

            var sql = $@" DECLARE @SearchQuery NVARCHAR(20) = NULL; 
                            {searchParam}
                            WITH tableResult(ID,Guid,CategoriesName
                                      ,CategoriesSearch,CategoriesElastic,CreatedDate,CreatedBy,ModifiedDate
                                      ,ModifedBy,Status,SyncElasticStatus
                                            )
                            AS ( 
                                  SELECT ID,Guid,CategoriesName
                                      ,CategoriesSearch,CategoriesElastic,CreatedDate,CreatedBy,ModifiedDate
                                      ,ModifedBy,Status,SyncElasticStatus
                                  FROM dbo.ImageCategories 
                                   {searchStr}
                             )

                             SELECT * FROM tableResult {sortStr} {fetchStr};

                             SELECT COUNT(1)
                                  FROM dbo.ImageCategories 
                                    {searchStr};";

            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                var result = !string.IsNullOrWhiteSpace(name)
                    ? dbContext.QueryMultiple(sql, new { @Search = name })
                    : dbContext.QueryMultiple(sql);

                var list = result.Read<ImageCategoriesResult>().ToList();
                var total = result.Read<int>().FirstOrDefault();
                var tuple = new Tuple<int, List<ImageCategoriesResult>>(total, list);
                return tuple;
            }
        }

        public Tuple<int, List<ImageSubCategoriesResult>> SearchSubCategories(FilterTableParams tableParams, string name, string status, int categories)
        {
            var searchParam = !string.IsNullOrWhiteSpace(name)
                               ? "SET @SearchQuery = @Search;"
                               : string.Empty;

            var searchStr = !string.IsNullOrWhiteSpace(name)
                        ? @" (t1.SubCategoriesName LIKE '%'+@SearchQuery+'%')  "
                        : string.Empty;

            if (categories > 0)
            {
                if (searchStr.Length > 0)
                {
                    searchStr = searchStr + $@" AND  ";
                }
                searchStr = searchStr + $@" t1.CategoriesId = {categories} ";
            }

            if (!string.IsNullOrEmpty(status))
            {
                string[] strArray = status.Split(',');
                List<int> lstIDStatus = new List<int>();
                foreach (var item in strArray)
                {
                    int xxx = 0;
                    if (int.TryParse(item, out xxx))
                    {
                        lstIDStatus.Add(xxx);
                    }
                }

                if (lstIDStatus.Count > 0)
                {
                    if (searchStr.Length > 0)
                    {
                        searchStr = searchStr + " AND ";
                    }
                    string strId = string.Join(",", lstIDStatus.ToArray());
                    searchStr = searchStr + $@"  t1.Status in ({strId})";
                }
            }

            /*WHERE*/
            if (searchStr.Length > 0)
            {
                if (searchStr.Length > 0)
                {
                    searchStr = " AND " + searchStr;
                }

                searchStr = $@" WHERE t1.Status <> {ConfigHelpers.StatusDeleted} " + searchStr;
            }
            /*paging*/
            var fetchStr = $" OFFSET {tableParams.Offset} ROWS FETCH NEXT {tableParams.PageSize} ROWS ONLY";
            /*Sort*/
            var sortStr = string.Empty;
            switch (tableParams.SortColum)
            {
                case 0:
                    sortStr = " ORDER BY CreatedDate " + tableParams.SortDirection;
                    break;
                default:
                    sortStr = " ORDER BY CreatedDate " + tableParams.SortDirection;
                    break;
            }

            var sql = $@" DECLARE @SearchQuery NVARCHAR(20) = NULL; 
                            {searchParam}
                            WITH tableResult(ID,Guid,SubCategoriesName,SubCategoriesSearch
                                      ,SubCategoriesElastic,CategoriesId,CreatedDate
                                      ,CreatedBy,ModifiedDate,ModifedBy,Status,SyncElasticStatus,CategoriesName)
                            AS ( 
                                  SELECT t1.ID,t1.Guid,t1.SubCategoriesName,t1.SubCategoriesSearch
                                      ,t1.SubCategoriesElastic,t1.CategoriesId,t1.CreatedDate
                                      ,t1.CreatedBy,t1.ModifiedDate,t1.ModifedBy,t1.Status,t1.SyncElasticStatus, t2.CategoriesName
                                  FROM dbo.ImageSubCategories t1
                                  Inner Join [dbo].[ImageCategories] t2 on t1.CategoriesId = t2.ID
                                   {searchStr}
                             )

                             SELECT * FROM tableResult {sortStr} {fetchStr};

                             SELECT COUNT(1)
                                  FROM dbo.ImageSubCategories t1
                                    {searchStr};";

            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                var result = !string.IsNullOrWhiteSpace(name)
                    ? dbContext.QueryMultiple(sql, new { @Search = name })
                    : dbContext.QueryMultiple(sql);

                var list = result.Read<ImageSubCategoriesResult>().ToList();
                var total = result.Read<int>().FirstOrDefault();
                var tuple = new Tuple<int, List<ImageSubCategoriesResult>>(total, list);
                return tuple;
            }
        }

        public ImageSubCategoriesDto GetSubCategoriesByGuid(Guid value)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@"SELECT * FROM ImageSubCategories WHERE Guid = @Guid";
                return dbContext.Query<ImageSubCategoriesDto>(query, new { @Guid = value }).FirstOrDefault();
            }
        }

        public List<ImageCategoriesDto> GetAllCategories()
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@"SELECT * FROM ImageCategories WHERE Status <> {ConfigHelpers.StatusDeleted}";
                return dbContext.Query<ImageCategoriesDto>(query).ToList();
            }
        }

        public List<ImageSubCategoriesDto> GetAllSubCategories()
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@"SELECT * FROM ImageSubCategories WHERE Status <> {ConfigHelpers.StatusDeleted}";
                return dbContext.Query<ImageSubCategoriesDto>(query).ToList();
            }
        }

        public List<ImageSubCategoriesDto> GetSubCategoriesByGroup(int categoriesId)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@"SELECT * FROM ImageSubCategories WHERE categoriesid = @categoriesid and Status <> {ConfigHelpers.StatusDeleted}";
                return dbContext.Query<ImageSubCategoriesDto>(query, new { @categoriesid = categoriesId }).ToList();
            }
        }

        public int AddAndEditImageSearch(ImageListDto model)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string querySelect = @"SELECT * FROM [dbo].[ImageList] WHERE ImageLink = @ImageLink 
                        AND CategoriesId = @CategoriesId AND SubCategoriesId = @SubCategoriesId";

                ImageListDto objUpdate = dbContext.Query<ImageListDto>(querySelect, model).FirstOrDefault();
                if (objUpdate != null)
                {
                    model.ID = objUpdate.ID;
                }

                string query = $@"INSERT INTO [dbo].[ImageList]
                                   ([Guid],[GuidSearch],[CategoriesId]
                                   ,[SubCategoriesId],[KeyElasticSearch]
                                   ,[ImageThumb],[ImageLink],[CreatedDate]
                                   ,[CreatedBy],[ModifiedDate],[ModifiedBy]
                                   ,[Status],[SyncElasticStatus])
                             VALUES
                                   (@Guid,@GuidSearch,@CategoriesId,@SubCategoriesId,@KeyElasticSearch
                                   ,@ImageThumb,@ImageLink,@CreatedDate,@CreatedBy
                                   ,@ModifiedDate,@ModifiedBy,@Status,@SyncElasticStatus)";

                if (model.ID > 0)
                {
                    query = $@"UPDATE [dbo].[ImageList]
                               SET [Guid] = @Guid
                                  ,[GuidSearch] = @GuidSearch
                                  ,[CategoriesId] = @CategoriesId
                                  ,[SubCategoriesId] = @SubCategoriesId
                                  ,[KeyElasticSearch] = @KeyElasticSearch
                                  ,[ImageThumb] = @ImageThumb
                                  ,[ImageLink] = @ImageLink
                                  ,[CreatedDate] = @CreatedDate
                                  ,[CreatedBy] = @CreatedBy
                                  ,[ModifiedDate] = @ModifiedDate
                                  ,[ModifiedBy] = @ModifiedBy
                                  ,[Status] = @Status
                                  ,[SyncElasticStatus] = @SyncElasticStatus
                             WHERE ID = @ID";
                }

                return dbContext.Execute(query, model);
            }
        }

        public List<ImageListDto> GetAllImagesSync(int status)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@"SELECT TOP 20 * FROM ImageList WHERE Status = {status} AND SyncElasticStatus = {ConfigHelpers.StatusPending}";
                return dbContext.Query<ImageListDto>(query).ToList();
            }
        }

        public Tuple<int, List<ImageListResult>> SearchImages(FilterTableParams tableParams, string name, string status, int categories, int subCategories)
        {
            var searchParam = !string.IsNullOrWhiteSpace(name)
                                ? "SET @SearchQuery = @Search;"
                                : string.Empty;

            var searchStr = !string.IsNullOrWhiteSpace(name)
                        ? @" (t1.KeyElasticSearch LIKE '%'+@SearchQuery+'%')  "
                        : string.Empty;

            if (categories > 0)
            {
                if (searchStr.Length > 0)
                {
                    searchStr = searchStr + $@" AND  ";
                }
                searchStr = searchStr + $@" t1.CategoriesId = {categories} ";
            }

            if (subCategories > 0)
            {
                if (searchStr.Length > 0)
                {
                    searchStr = searchStr + $@" AND  ";
                }
                searchStr = searchStr + $@" t1.SubCategoriesId = {subCategories} ";
            }

            if (!string.IsNullOrEmpty(status))
            {
                string[] strArray = status.Split(',');
                List<int> lstIDStatus = new List<int>();
                foreach (var item in strArray)
                {
                    int xxx = 0;
                    if (int.TryParse(item, out xxx))
                    {
                        lstIDStatus.Add(xxx);
                    }
                }

                if (lstIDStatus.Count > 0)
                {
                    if (searchStr.Length > 0)
                    {
                        searchStr = searchStr + " AND ";
                    }
                    string strId = string.Join(",", lstIDStatus.ToArray());
                    searchStr = searchStr + $@"  t1.Status in ({strId})";
                }
            }

            /*WHERE*/
            if (searchStr.Length > 0)
            {
                searchStr = $@" WHERE  " + searchStr;
            }
            /*paging*/
            var fetchStr = $" OFFSET {tableParams.Offset} ROWS FETCH NEXT {tableParams.PageSize} ROWS ONLY";
            /*Sort*/
            var sortStr = string.Empty;
            switch (tableParams.SortColum)
            {
                case 0:
                    sortStr = " ORDER BY CreatedDate " + tableParams.SortDirection;
                    break;
                default:
                    sortStr = " ORDER BY CreatedDate " + tableParams.SortDirection;
                    break;
            }

            var sql = $@" DECLARE @SearchQuery NVARCHAR(20) = NULL; 
                            {searchParam}
                            WITH tableResult(ID,Guid,GuidSearch,CategoriesId
	                            ,SubCategoriesId,KeyElasticSearch
	                            ,ImageThumb,ImageLink,CreatedDate
	                            ,CreatedBy,ModifiedDate,ModifiedBy
	                            ,Status,SyncElasticStatus,CategoriesName,SubCategoriesName)
                            AS ( 
                                  SELECT t1.ID,t1.Guid,t1.GuidSearch,t1.CategoriesId
	                            ,t1.SubCategoriesId,t1.KeyElasticSearch
	                            ,t1.ImageThumb,t1.ImageLink,t1.CreatedDate
	                            ,t1.CreatedBy,t1.ModifiedDate,t1.ModifiedBy
	                            ,t1.Status,t1.SyncElasticStatus, t2.CategoriesName, t3.SubCategoriesName FROM [dbo].[ImageList] t1
                            Inner Join [dbo].[ImageCategories] t2 on t1.CategoriesId = t2.ID
                            Inner join [dbo].[ImageSubCategories] t3 on t1.SubCategoriesId = t3.ID
                                   {searchStr}
                             )

                             SELECT * FROM tableResult {sortStr} {fetchStr};

                            SELECT COUNT(1) FROM [dbo].[ImageList] t1
                            Inner Join [dbo].[ImageCategories] t2 on t1.CategoriesId = t2.ID
                            Inner join [dbo].[ImageSubCategories] t3 on t1.SubCategoriesId = t3.ID
                                    {searchStr};";

            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                var result = !string.IsNullOrWhiteSpace(name)
                    ? dbContext.QueryMultiple(sql, new { @Search = name })
                    : dbContext.QueryMultiple(sql);

                var list = result.Read<ImageListResult>().ToList();
                var total = result.Read<int>().FirstOrDefault();
                var tuple = new Tuple<int, List<ImageListResult>>(total, list);
                return tuple;
            }
        }

        public ImageListDto GetWebMMOByGuid(Guid valueGuid)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@"SELECT * FROM ImageList WHERE Guid = @Guid";
                return dbContext.Query<ImageListDto>(query, new { @Guid = valueGuid }).FirstOrDefault();
            }
        }

        public List<ImageCategoriesDto> GetAppCategories(string appPackage)
        {

            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                try
                {
                    string query = $@"SELECT * FROM [dbo].[AppPermission] 
                                    WHERE AppPackage = @AppPackage ";

                    var objApp = dbContext.Query<AppPermissionDto>(query, new { @AppPackage = appPackage.Trim() }).FirstOrDefault();
                    if (objApp != null && !string.IsNullOrEmpty(objApp.ListPermission))
                    {
                        query = $@"SELECT * FROM [dbo].[ImageCategories] WHERE ID in ({objApp.ListPermission})";
                        return dbContext.Query<ImageCategoriesDto>(query).ToList();
                    }
                }
                catch (Exception)
                {

                }
                return null;
            }

        }

        public ApiKeyDto GetTop1Key()
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@"SELECT TOP 1 * FROM ApiKey WHERE CountNumber < 100 ORDER BY ID";
                return dbContext.Query<ApiKeyDto>(query).FirstOrDefault();
            }
        }

        public int UpdateCountApiKey(int iD)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@" UPDATE ApiKey SET CountNumber = ISNULL(CountNumber, 0) + 1 WHERE ID = @ID";
                return dbContext.Execute(query, new { @ID = iD });
            }
        }

        public int RefreshApiKey()
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@" UPDATE ApiKey SET CountNumber = 0, NextDate = @NextDate";
                return dbContext.Execute(query, new { @NextDate = DateTime.UtcNow.AddDays(1) });
            }
        }

        public AccessTokenUserDto CheckToken(string token)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@" SELECT * FROM AccessTokenUser WHERE AccessToken = @token";
                var objToken = dbContext.Query<AccessTokenUserDto>(query, new { @token = token }).FirstOrDefault();
                return objToken;
            }
        }

        public int UpdateBalanceApiKey(AccessTokenUserDto chkCheckApi)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@" UPDATE [dbo].[AccessTokenUser]
                       SET [AccessToken] = @AccessToken
                          ,[UserId] = @UserId
                          ,[BalanceMoney] = @BalanceMoney
                          ,[BalanceUsed] = @BalanceUsed
                          ,[BalanceErorr] = @BalanceErorr
                     WHERE ID = @ID";
                return dbContext.Execute(query, chkCheckApi);
            }
        }

        public AccessTokenUserDto GetAccessTokenUserGuid(string key)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@" SELECT * FROM [dbo].[AccessTokenUser] WHERE [AccessToken] = @AccessToken";
                return dbContext.Query<AccessTokenUserDto>(query, new { @AccessToken = key }).FirstOrDefault();
            }
        }

        public int AddAndEditAccessTokenUser(AccessTokenUserDto objNew)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@" UPDATE [dbo].[AccessTokenUser]
                       SET [AccessToken] = @AccessToken
                          ,[UserId] = @UserId
                          ,[BalanceMoney] = @BalanceMoney
                          ,[BalanceUsed] = @BalanceUsed
                          ,[BalanceErorr] = @BalanceErorr
                            ,[CreatedDate] = @CreatedDate
                            ,[CreatedBy] = @CreatedBy
                            ,[ModifiedDate] = @ModifiedDate
                            ,[ModifiedBy] = @ModifiedBy
                            ,[Status] = @Status
                     WHERE ID = @ID";

                if (objNew.Id == 0)
                {
                    query = $@" INSERT INTO [dbo].[AccessTokenUser]
                               ([AccessToken],[UserId],[BalanceMoney],[BalanceUsed],[BalanceErorr],CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,Status)
                         VALUES
                               (@AccessToken,@UserId,@BalanceMoney,@BalanceUsed,@BalanceErorr,@CreatedDate,@CreatedBy,@ModifiedDate,@ModifiedBy,@Status)";
                }
                return dbContext.Execute(query, objNew);
            }
        }

        public Tuple<int, List<AccessTokenUserDto>> SearchManageKey(FilterTableParams tableParams, string name)
        {
            var searchParam = !string.IsNullOrWhiteSpace(name)
                               ? "SET @SearchQuery = @Search;"
                               : string.Empty;

            var searchStr = !string.IsNullOrWhiteSpace(name)
                        ? @" (AccessToken LIKE '%'+@SearchQuery+'%')  "
                        : string.Empty;

            /*WHERE*/
            if (searchStr.Length > 0)
            {
                searchStr = $@" WHERE  " + searchStr;
            }
            /*paging*/
            var fetchStr = $" OFFSET {tableParams.Offset} ROWS FETCH NEXT {tableParams.PageSize} ROWS ONLY";
            /*Sort*/
            var sortStr = string.Empty;
            switch (tableParams.SortColum)
            {
                case 0:
                    sortStr = " ORDER BY CreatedDate " + tableParams.SortDirection;
                    break;
                default:
                    sortStr = " ORDER BY CreatedDate " + tableParams.SortDirection;
                    break;
            }

            var sql = $@" DECLARE @SearchQuery NVARCHAR(20) = NULL; 
                            {searchParam}
                            WITH tableResult(Id,AccessToken,UserId,BalanceMoney,BalanceUsed,BalanceErorr,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,Status)
                            AS ( 
                                 SELECT Id,AccessToken,UserId,BalanceMoney,BalanceUsed,BalanceErorr,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,Status FROM AccessTokenUser
                                   {searchStr}
                             )

                             SELECT * FROM tableResult {sortStr} {fetchStr};

                            SELECT COUNT(1) FROM AccessTokenUser
                                    {searchStr};";

            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                var result = dbContext.QueryMultiple(sql, new { @Search = name });

                var list = result.Read<AccessTokenUserDto>().ToList();
                var total = result.Read<int>().FirstOrDefault();
                var tuple = new Tuple<int, List<AccessTokenUserDto>>(total, list);
                return tuple;
            }
        }

        public int AddAndEditSmsPrice(SmsPriceDto objNew)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@" UPDATE [dbo].[SMSPrice]
                   SET [ServicesName] = @ServicesName
                      ,[ServicesKey] = @ServicesKey
                      ,[Price] = @Price
                      ,[CreatedBy] = @CreatedBy
                      ,[CreatedDate] = @CreatedDate
                      ,[ModifiedBy] = @ModifiedBy
                      ,[ModifiedDate] = @ModifiedDate
                 WHERE Id = @Id";

                if (objNew.Id == 0)
                {
                    query = $@" INSERT INTO [dbo].[SMSPrice]
                               ([ServicesName],[ServicesKey],[Price]
                               ,[CreatedBy],[CreatedDate],[ModifiedBy],[ModifiedDate])
                                VALUES
                               (@ServicesName,@ServicesKey,@Price,@CreatedBy
                               ,@CreatedDate,@ModifiedBy,@ModifiedDate)";
                }
                return dbContext.Execute(query, objNew);
            }
        }

        public SmsPriceDto GetSmsPriceById(int id)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@" SELECT * FROM [dbo].[SMSPrice] WHERE Id = @Id";
                return dbContext.Query<SmsPriceDto>(query, new { @Id = id }).FirstOrDefault();
            }
        }

        public Tuple<int, List<SmsPriceDto>> SearchManagePriceSms(FilterTableParams tableParams, string name)
        {
            var searchParam = !string.IsNullOrWhiteSpace(name)
                              ? "SET @SearchQuery = @Search;"
                              : string.Empty;

            var searchStr = !string.IsNullOrWhiteSpace(name)
                        ? @" (ServicesName LIKE '%'+@SearchQuery+'%')  "
                        : string.Empty;

            /*WHERE*/
            if (searchStr.Length > 0)
            {
                searchStr = $@" WHERE  " + searchStr;
            }
            /*paging*/
            var fetchStr = $" OFFSET {tableParams.Offset} ROWS FETCH NEXT {tableParams.PageSize} ROWS ONLY";
            /*Sort*/
            var sortStr = string.Empty;
            switch (tableParams.SortColum)
            {
                case 0:
                    sortStr = " ORDER BY CreatedDate " + tableParams.SortDirection;
                    break;
                default:
                    sortStr = " ORDER BY CreatedDate " + tableParams.SortDirection;
                    break;
            }

            var sql = $@" DECLARE @SearchQuery NVARCHAR(20) = NULL; 
                            {searchParam}
                            WITH tableResult(Id,ServicesName,ServicesKey,Price,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
                            AS ( 
                                 SELECT Id,ServicesName,ServicesKey
                                      ,Price,CreatedBy,CreatedDate
                                      ,ModifiedBy,ModifiedDate
                                    FROM SMSPrice
                                   {searchStr}
                             )

                             SELECT * FROM tableResult {sortStr} {fetchStr};

                            SELECT COUNT(1) FROM SMSPrice
                                    {searchStr};";

            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                var result = dbContext.QueryMultiple(sql, new { @Search = name });

                var list = result.Read<SmsPriceDto>().ToList();
                var total = result.Read<int>().FirstOrDefault();
                var tuple = new Tuple<int, List<SmsPriceDto>>(total, list);
                return tuple;
            }
        }

        public int AddAndEditSearchLog(SearchLogDto objSearch)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@" INSERT INTO [dbo].[SearchLog] ([KeySearch],[CategoriesId],[SubCategoriesId],[UserId],[CreatedDate])
                                    VALUES (@KeySearch,@CategoriesId,@SubCategoriesId,@UserId,@CreatedDate)";
                return dbContext.Execute(query, objSearch);
            }
        }

        public int GmailPush(GmailInfoDto objGmailPush)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@" INSERT INTO [dbo].[GmailInfo] ([username],[password],[emailrecover],[cookies],[emailType],[createddate],[token],status)
                                VALUES (@username,@password,@emailrecover,@cookies,@emailType,@createddate,@token,@status)";
                return dbContext.Execute(query, objGmailPush);
            }
        }

        public List<GmailInfoDto> GmailGet(string token, string emailtype, int amount)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                DateTime dt = DateTime.Now.AddMinutes(-40);
                string query = $@"SELECT TOP({amount}) * FROM GmailInfo where createddate > @createddate and emailType = @emailType and status = 0 order by createddate asc";
                var lstEmail = dbContext.Query<GmailInfoDto>(query, new { @createddate = dt, @emailType = emailtype }).ToList();

                if (lstEmail.Count > 0)
                {
                    query = "UPDATE GmailInfo SET status = 1 WHERE id = @id";
                    dbContext.Execute(query, lstEmail);
                    GmailPriceDto objGmailPrice = new GmailPriceDto();
                    query = $@"SELECT * FROM GmailPrice where gmailtype = @gmailtype";
                    objGmailPrice = dbContext.Query<GmailPriceDto>(query, new { @gmailtype = emailtype }).FirstOrDefault();
                    /*Update balance*/
                    query = "UPDATE [dbo].[AccessTokenUser] SET BalanceMoney = BalanceMoney - @money,BalanceUsed = BalanceUsed + @money WHERE AccessToken = @AccessToken";
                    dbContext.Execute(query, new { @money = lstEmail.Count * objGmailPrice.gmailprice, @AccessToken = token });
                }

                return lstEmail;
            }
        }

        public GmailPriceDto GmailPriceGetByType(string type)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                DateTime dt = DateTime.Now.AddMinutes(-40);
                string query = $@"SELECT * FROM GmailPrice where gmailtype = @gmailtype";
                return dbContext.Query<GmailPriceDto>(query, new { @gmailtype = type }).FirstOrDefault();
            }
        }

        public List<GmailInfoDto> GmailGetByKey(string token, string type, int amount)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@"SELECT TOP({amount}) * FROM GmailInfo where emailType = @emailType and status = 0 and token = @token order by createddate asc";
                var lstEmail = dbContext.Query<GmailInfoDto>(query, new { @token = token, @emailType = type }).ToList();
                if (lstEmail.Count > 0)
                {
                    query = "UPDATE GmailInfo SET status = 1 WHERE id = @id";
                    dbContext.Execute(query, lstEmail);
                }
                return lstEmail;
            }
        }

        public GmailStatictisResult GmailStatistics(string token)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                DateTime dt = DateTime.Now.AddMinutes(-40);
                string query = $@"SELECT @token as token,
                    (SELECT COUNT(1) FROM GmailInfo where token = @token and createddate > DATEADD(MINUTE, -10, getdate()) ) as amount10phut,
                    (SELECT COUNT(1) FROM GmailInfo where token = @token and createddate > DATEADD(hour, -2, getdate()) ) as amount2h,
                    (SELECT COUNT(1) FROM GmailInfo where token = @token and createddate > DATEADD(day, -1, getdate()) ) as amount24h,
                    (SELECT COUNT(1) FROM GmailInfo where token = @token) as amounttotal,
                    (SELECT COUNT(1) FROM GmailInfo where token = @token and status = 1) as gettotal,
                    (SELECT COUNT(1) FROM GmailInfo where token = @token and status = 0) as realtotal";
                return dbContext.Query<GmailStatictisResult>(query, new { @token = token }).FirstOrDefault();
            }
        }

        public int DevicesPush(string deviceinfo)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                DevicesInfoDto objInsert = new DevicesInfoDto();
                objInsert.CreatedDate = DateTime.Now;
                objInsert.DevicesType = 1;
                objInsert.CreatedDate = DateTime.Now;
                objInsert.Status = 0;
                objInsert.DevicesInfo = deviceinfo;

                string query = "SELECT * FROM [dbo].[DevicesInfo] WHERE [DevicesInfo] = @DevicesInfo";

                var objCheck = dbContext.Query<DevicesInfoDto>(query, objInsert).FirstOrDefault();
                if (objCheck == null)
                {
                    query = $@"INSERT INTO [dbo].[DevicesInfo]([DevicesInfo],[DevicesType],[CreatedDate],[Status])
                        VALUES
                     (@DevicesInfo,@DevicesType,@CreatedDate,@Status)";
                    dbContext.Execute(query, objInsert);
                }
                return 0;
            }
        }

        public DevicesInfoDto DevicesGet()
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                DevicesInfoDto objInsert = new DevicesInfoDto();
                objInsert.CreatedDate = DateTime.Now;
                objInsert.DevicesType = 1;
                objInsert.CreatedDate = DateTime.Now;
                string query = $@"SELECT TOP 1 * FROM [dbo].[DevicesInfo] WHERE Status = 0 Order by CreatedDate asc";
                var objResult = dbContext.Query<DevicesInfoDto>(query).FirstOrDefault();
                if (objResult == null)
                {
                    query = $@"Update [dbo].[DevicesInfo] SET Status = 0 ";
                    dbContext.Execute(query);
                }
                else
                {
                    query = $@"Update [dbo].[DevicesInfo] SET Status = 1 where ID= @id ";
                    dbContext.Execute(query, objResult);
                }
                return objResult;
            }
        }

        public void DevicesFakeInsert(DevicesFakeInfoDto objFakeInfo)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@"INSERT INTO [dbo].[DevicesFakeInfo]([androidID],[androidSerial],[base_OS],[board],[bootloader],[brand],[buildID],[buildTime],[codeName],[countryISO],[display],[fingerPrint],[gsFID],[hardWare],[host],[incremental],[kernelVersion],[languageCode],[manufacture],[model],[other1],[other2],[other3],[previewSDK],[previewSDKInt],[product],[radio],[release],[resolution],[sdkInt],[securityPatch],[serial],[tags],[time],[type],[user],[userAgent],[CreatedDate],[Status],[imei])
                         VALUES (@androidID,@androidSerial,@base_OS,@board,@bootloader,@brand,@buildID,@buildTime,@codeName,@countryISO,@display,@fingerPrint,@gsFID,@hardWare,@host,@incremental,@kernelVersion,@languageCode,@manufacture,@model,@other1,@other2,@other3,@previewSDK,@previewSDKInt,@product,@radio,@release,@resolution,@sdkInt,@securityPatch,@serial,@tags,@time,@type,@user,@userAgent,@CreatedDate,@Status,@imei)";
                dbContext.Execute(query, objFakeInfo);
            }
        }

        public List<CitiesCodeDto> GetCitiesCode(string countryCode)
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@"SELECT * FROM CitiesCode WHERE country=@countrycode";
                return dbContext.Query<CitiesCodeDto>(query, new { @countrycode = countryCode }).ToList();
            }
        }

        public List<string> GetKeyNews()
        {
            using (var dbContext = SqlServerWebRepository.OpenConnection())
            {
                string query = $@"SELECT keynews FROM tbKeyNews";
                return dbContext.Query<string>(query).ToList();

            }
        }
    }
}
