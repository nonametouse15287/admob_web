﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using WebMMO.Core.DomainModels.Menu;
using WebMMO.Core.IRepositories;
using Dapper;

namespace WebMMO.Infrastructures.Dapper.Repositories
{
    public class MenuRepository : IMenuRepository
    {
        public IEnumerable<MenuDto> GetMenuList(int? menuParentId, int? Seq)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string strWhere = "";

                if (menuParentId == null)
                {
                    if (strWhere.Length > 0)
                    {
                        strWhere = strWhere + " AND ";
                    }

                    strWhere = strWhere + " MenuParentID is null ";
                }
                else
                {
                    if (strWhere.Length > 0)
                    {
                        strWhere = strWhere + " AND ";
                    }
                    strWhere = strWhere + " MenuParentID = @MenuParentID ";
                }

                if (Seq != null)
                {
                    if (strWhere.Length > 0)
                    {
                        strWhere = strWhere + " AND ";
                    }

                    strWhere = strWhere + " Seq >= @Seq ";
                }

                if (strWhere.Length > 0)
                {
                    strWhere = " WHERE " + strWhere;
                }

                string query = $@"SELECT * FROM [Menu] {strWhere} order by Seq asc";

                return dbContext.Query<MenuDto>(query, new { @menuParentId = menuParentId, @Seq = Seq }).ToList();

            }
        }

        public MenuDto GetMenuById(int id)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = $@"SELECT * FROM [Menu] WHERE ID = @id";

                return dbContext.Query<MenuDto>(query, new { @id = id }).FirstOrDefault();

            }
        }

        public int UpdateMenu(MenuDto menu)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = $@"UPDATE Menu SET Name = @Name, Link = @Link, MenuParentID = @MenuParentID, Seq = @Seq, IconClass = @IconClass, JpName = @JpName WHERE ID = @ID ";

                return dbContext.Execute(query, menu);

            }
        }

        public IEnumerable<RoleMenuDto> GetRoleMenu(int? menuId, int? roleId)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string strWhere = "";

                if (menuId != null)
                {
                    if (strWhere.Length > 0)
                    {
                        strWhere = strWhere + " AND";
                    }

                    strWhere = strWhere + "  MenuID = @menuId ";
                }

                if (roleId != null)
                {
                    if (strWhere.Length > 0)
                    {
                        strWhere = strWhere + " AND";
                    }

                    strWhere = strWhere + "  roleId =@roleId ";
                }


                if (strWhere.Length > 0)
                {
                    strWhere = " WHERE " + strWhere;
                }

                string query = $@"SELECT * FROM [RoleMenu] {strWhere} ";

                return dbContext.Query<RoleMenuDto>(query, new { @menuId = menuId, @roleId = roleId });

            }
        }

        public int DeleteRoleMenu(int id)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = $@"DELETE FROM [RoleMenu] WHERE ID = @Id ";
                return dbContext.Execute(query, new { @Id = id });
            }
        }

        public int InsertRoleMenu(RoleMenuDto newaccess)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = $@"INSERT INTO RoleMenu(RoleID, MenuID) VALUES (@RoleID, @MenuID); 
                                    SELECT CAST(SCOPE_IDENTITY() as int);";
                return dbContext.Query<int>(query, newaccess).FirstOrDefault();
            }
        }

        public IEnumerable<MenuDto> GetMenuByParentId(int parentId)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = $@"SELECT * FROM [Menu] WHERE MenuParentID = @parentId";

                return dbContext.Query<MenuDto>(query, new { @parentId = parentId });

            }
        }

        public int InsertMenu(MenuDto menu)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = $@"INSERT INTO [dbo].[Menu]
                                       ([Name],[Link],[MenuParentID],[Seq],[IconClass],[JpName])
                                 VALUES
                                       (@Name,@Link,@MenuParentID,@Seq,@IconClass,@JpName); 
                                SELECT CAST(SCOPE_IDENTITY() as int);";

                return dbContext.Query<int>(query, menu).FirstOrDefault();
            }
        }

        public int DeleteMenu(int menuId)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = $@"DELETE FROM [Menu] WHERE ID = @ID";

                return dbContext.Execute(query, new { @ID = menuId });

            }
        }

        public IEnumerable<MenuDto> GetMenu_HIAS_GET_MENUACCESS(int id, int? intMenuParentId)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = $@"dbo.HIAS_GET_MENUACCESS";

                return dbContext.Query<MenuDto>(query, new { @intUserID = id, @intMenuParentID = intMenuParentId }, commandType: CommandType.StoredProcedure);

            }
        }
    }
}
