﻿using System.Collections.Generic;
using System.Linq;
using WebMMO.Core.DomainModels.Role;
using WebMMO.Core.DomainModels.User;
using WebMMO.Core.IRepositories;
using Dapper;

namespace WebMMO.Infrastructures.Dapper.Repositories
{
    public class UserRepository : IUserRepository
    {

        public UserDto GetUserByUsername(string userName)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"SELECT * FROM [User] where username = @userName";

                return (dbContext.Query<UserDto>(query, new { @userName = userName })).FirstOrDefault();

            }
        }

        public UserDto GetUserById(int id)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"SELECT * FROM [User] where ID = @id";

                return (dbContext.Query<UserDto>(query, new { @id = id })).FirstOrDefault();

            }
        }

        public UserDto GetUserByEmail(string email)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"SELECT * FROM [User] where EmailAddress = @email";

                return (dbContext.Query<UserDto>(query, new { @email = email })).FirstOrDefault();

            }
        }

        public int UpdatePassword(UserDto user)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"UPDATE  [User] SET Password = @Password where ID = @ID";

                return (dbContext.Execute(query, new { @Password = user.Password, @ID = user.ID }));

            }
        }

        public int UpdateLastLoginDate(UserDto user)
        {

            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"UPDATE  [User] SET LastLoginDate = @LastLoginDate where ID = @ID";

                return (dbContext.Execute(query, new { @LastLoginDate = user.LastLoginDate, @ID = user.ID }));

            }
        }

        public ActivateUserRequestDto GetActivateUserByKey(string id)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"SELECT * FROM [ActivateUserRequest] where key = @key";

                return (dbContext.Query<ActivateUserRequestDto>(query, new { @key = id })).FirstOrDefault();

            }
        }

        public int UpdateUser(UserDto user)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"UPDATE [dbo].[User]
                               SET [OrganizationID] =@OrganizationID
                                  ,[FirstName] =@FirstName
                                  ,[LastName] =@LastName
                                  ,[EmailAddress] =@EmailAddress
                                  ,[ContactNumber] =@ContactNumber
                                  ,[IsActive] =@IsActive
                                  ,[Password] =@Password
                                  ,[Photo] =@Photo
                                  ,[DateCreated] =@DateCreated
                                  ,[DateModified] =@DateModified
                                  ,[CreatedByUserID] =@CreatedByUserID
                                  ,[ModifiedByUserID] =@ModifiedByUserID
                                  ,[LastLoginDate] =@LastLoginDate
                                  ,[GenderID] =@GenderID
                                  ,[AgeGroupID] =@AgeGroupID
                                  ,[NationalityID] =@NationalityID
                                  ,[CountryID] =@CountryID
                                  ,[JobTitle] =@JobTitle
                                  ,[DisableGetStarted] =@DisableGetStarted
                                  WHERE ID=@ID";

                return (dbContext.Execute(query, user));

            }
        }

        public int RemoveActivateUserRequest(ActivateUserRequestDto activateUserRequest)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"DELETE FROM [ActivateUserRequest] where ID = @ID";

                return dbContext.Execute(query, new { @ID = activateUserRequest.ID });

            }
        }

        public ActivateUserRequestDto GetActivateUserByUserId(int id)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"SELECT * FROM [ActivateUserRequest] where UserId = @UserId";

                return (dbContext.Query<ActivateUserRequestDto>(query, new { @UserId = id })).FirstOrDefault();

            }
        }

        public List<ActivateUserRequestDto> GetActivateUserAll()
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"SELECT * FROM [ActivateUserRequest] ;";

                return (dbContext.Query<ActivateUserRequestDto>(query)).ToList();

            }
        }

        public ResetPasswordRequestDto GetResetPasswordByKey(string key)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"SELECT * FROM [ResetPasswordRequest] where key = @key;";

                return dbContext.Query<ResetPasswordRequestDto>(query).FirstOrDefault();

            }
        }

        public int RemoveResetPassword(ResetPasswordRequestDto resetPasswordRequest)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"DELETE FROM [ResetPasswordRequest] where ID = @ID;";

                return dbContext.Execute(query, new { @ID = resetPasswordRequest.ID });

            }
        }

        public int InsertResetPasswordRequest(ResetPasswordRequestDto resetPasswordRequest)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"INSERT INTO ResetPasswordRequest(UserID, Key, DateCreated) 
                            VALUES (@UserID, @Key, @DateCreated);
                            SELECT CAST(SCOPE_IDENTITY() as int);";

                return dbContext.Query<int>(query, resetPasswordRequest).FirstOrDefault();

            }
        }

        public int AddUser(UserDto user)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"INSERT INTO [dbo].[User]
                                           ([OrganizationID],[FirstName],[LastName],[EmailAddress],[ContactNumber]
                                           ,[IsActive],[Password],[Photo],[DateCreated],[DateModified],[CreatedByUserID]
                                           ,[ModifiedByUserID],[LastLoginDate],[GenderID],[AgeGroupID],[NationalityID]
                                           ,[CountryID],[JobTitle],[DisableGetStarted],[LastLoginIP],[DashboardID])
                                     VALUES
                                           (@OrganizationID,@FirstName,@LastName,@EmailAddress,@ContactNumber,@IsActive,@Password
                                           ,@Photo,@DateCreated,@DateModified,@CreatedByUserID,@ModifiedByUserID,@LastLoginDate,@GenderID
                                           ,@AgeGroupID,@NationalityID,@CountryID,@JobTitle,@DisableGetStarted,@LastLoginIP,@DashboardID);
                                    SELECT CAST(SCOPE_IDENTITY() as int);";

                return dbContext.Query<int>(query, user).FirstOrDefault();

            }
        }

        public int AddUserRole(UserRoleDto userrole)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"INSERT INTO UserRole(UserID, RoleID) VALUES (@UserID, @RoleID);
                                SELECT CAST(SCOPE_IDENTITY() as int);";

                return dbContext.Query<int>(query, userrole).FirstOrDefault();

            }
        }

        public IEnumerable<UserRoleDto> GetUserRolesByUserId(int userId)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"SELECT * FROM  UserRole WHERE  UserID = @UserID";

                return dbContext.Query<UserRoleDto>(query, new { @UserID = userId }).ToList();

            }
        }

        public int DeleteUserRole(UserRoleDto currentRole)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"DELETE FROM UserRole WHERE UserID = @UserID AND ID = @ID";

                return (dbContext.Execute(query, currentRole));

            }
        }

        public int DeleteUserRoleByUserId(int userId)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"DELETE FROM UserRole WHERE UserID = @UserID;";

                return (dbContext.Execute(query, new { @UserID = userId }));

            }
        }

        public IEnumerable<UserRoleDto> GetUserRolesById(int userId, int roleId)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"SELECT * FROM  UserRole  WHERE UserID = @UserID AND RoleID = @roleid;";

                return dbContext.Query<UserRoleDto>(query, new { @UserID = userId, @roleid = roleId });

            }
        }

        public IEnumerable<UserResult> GetQueryCbUser(int? organizationId, string status, int? roleId)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string strWhere = "";
                if (organizationId != null)
                {
                    strWhere = strWhere + " AND t1.OrganizationID = @OrganizationID";
                }
                else
                {
                    strWhere = strWhere + "  AND t1.OrganizationID = 1";
                }

                if (!string.IsNullOrEmpty(status))
                {
                    if (status == "Enabled")
                    {
                        strWhere = strWhere + " AND  t1.IsActive = 1";
                    }

                    if (status == "Disabled")
                    {
                        strWhere = strWhere + " AND  t1.IsActive = 0";
                    }
                }

                if (roleId != null)
                {

                    strWhere = strWhere + "  AND  t2.RoleId = @roleId";
                }


                string query = $@"SELECT distinct t1.* FROM [User] t1, UserRole t2
                                    WHERE t1.id = t2.userid  {strWhere} ";

                var result = dbContext.Query<UserDto>(query, new
                {
                    @OrganizationID = organizationId,
                    @Status = status,
                    @roleId = roleId
                }).ToList();

                List<UserResult> lstResults = new List<UserResult>();

                foreach (var item in result)
                {
                    UserResult objResult = new UserResult();
                    objResult.FromUserDto(item);
                    /*Get Role Name*/

                    query = $@"SELECT t3.* FROM [User] t1, UserRole t2, [Role] t3
                                    WHERE t1.id = t2.userid AND  t2.RoleID = t3.ID AND t1.id = {item.ID}  ";

                    var resultRole = dbContext.Query<RoleDto>(query).ToList();
                    foreach (var itemRole in resultRole)
                    {
                        if (string.IsNullOrEmpty(objResult.RoleName))
                        {
                            objResult.RoleName = itemRole.Name;
                        }
                        else
                        {
                            objResult.RoleName = objResult.RoleName + "," + itemRole.Name;
                        }
                    }

                    lstResults.Add(objResult);
                }



                return lstResults;

            }
        }
        
        public int UpdateActiveUserRequest(ActivateUserRequestDto activateUserRequest)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"UPDATE ActivateUserRequest SET UserID = @UserID, Key = @Key, DateCreated = @DateCreated WHERE ID = @ID ";

                return (dbContext.Execute(query, activateUserRequest));

            }
        }

        public IEnumerable<StateDto> GetStateByCountryId(int countryIdValue)
        {
            using (var dbContext = SQLServer.SqlServerWebRepository.OpenConnection())
            {
                string query = @"select * FROM [dbo].[State] WHERE countryId = @countryId;";

                return dbContext.Query<StateDto>(query, new { @countryId = countryIdValue }).ToList();
            }
        }
    }
}
