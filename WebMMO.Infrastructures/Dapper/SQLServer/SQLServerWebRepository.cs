﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace WebMMO.Infrastructures.Dapper.SQLServer
{
    public static class SqlServerWebRepository
    {
        private static readonly string strConnection =
            ConfigurationManager.ConnectionStrings["WebMMOConnectionstring"].ConnectionString;
        public static DbConnection OpenConnection()
        {
            var connString = strConnection;
            DbConnection connection = new SqlConnection(connString);
            connection.EnsureOpen();
            return connection;
        }

        public static IDisposable EnsureOpen(this DbConnection connection)
        {
            if (connection == null) throw new ArgumentNullException(nameof(connection));
            switch (connection.State)
            {
                case ConnectionState.Open:
                    return null;
                case ConnectionState.Closed:
                    connection.Open();
                    try
                    {
                        return new ConnectionCloser(connection);
                    }
                    catch
                    {
                        try { connection.Close(); }
                        catch { /* we're already trying to handle, kthxbye */ }
                        throw;
                    }

                default:
                    throw new InvalidOperationException("Cannot use EnsureOpen when connection is " + connection.State);
            }
        }

        public static int SetReadCommitted(this DbConnection connection)
        {
            if (connection == null) throw new ArgumentNullException(nameof(connection));
            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SET TRANSACTION ISOLATION LEVEL READ COMMITTED";
                cmd.ExecuteNonQuery();
            }
            return 1;
        }

        private class ConnectionCloser : IDisposable
        {
            DbConnection _connection;
            public ConnectionCloser(DbConnection connection)
            {
                _connection = connection;
            }
            public void Dispose()
            {
                var cn = _connection;
                _connection = null;
                try { cn?.Close(); }
                catch { /* throwing from Dispose() is so lame */ }
            }
        }
    }


}
