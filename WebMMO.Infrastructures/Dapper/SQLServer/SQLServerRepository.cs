﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace WebMMO.Infrastructures.Dapper.SQLServer
{
    public static class SqlServerRepository
    {
        private static readonly string strConnection =
            ConfigurationManager.ConnectionStrings["WebMMOConnectionstring"].ConnectionString;
        public static async Task<DbConnection> OpenConnection()
        {
            var connString = strConnection;
            DbConnection connection = new SqlConnection(connString);
            await connection.EnsureOpenAsync().ConfigureAwait(false);
            return connection;
        }

        public static async Task<IDisposable> EnsureOpenAsync(this DbConnection connection)
        {
            if (connection == null) throw new ArgumentNullException(nameof(connection));
            switch (connection.State)
            {
                case ConnectionState.Open:
                    return null;
                case ConnectionState.Closed:
                    await connection.OpenAsync().ConfigureAwait(false);
                    try
                    {
                        return new ConnectionCloser(connection);
                    }
                    catch
                    {
                        try { connection.Close(); }
                        catch { /* we're already trying to handle, kthxbye */ }
                        throw;
                    }

                default:
                    throw new InvalidOperationException("Cannot use EnsureOpen when connection is " + connection.State);
            }
        }

        public static async Task<int> SetReadCommittedAsync(this DbConnection connection)
        {
            if (connection == null) throw new ArgumentNullException(nameof(connection));
            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = "SET TRANSACTION ISOLATION LEVEL READ COMMITTED";
                await cmd.ExecuteNonQueryAsync().ConfigureAwait(false);
            }
            return 1;
        }

        private class ConnectionCloser : IDisposable
        {
            DbConnection _connection;
            public ConnectionCloser(DbConnection connection)
            {
                _connection = connection;
            }
            public void Dispose()
            {
                var cn = _connection;
                _connection = null;
                try { cn?.Close(); }
                catch { /* throwing from Dispose() is so lame */ }
            }
        }
    }

    
}
