﻿using System.Web.Mvc;

namespace WebMMO.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return RedirectToAction("Login", "Account", new { Area = "Home" });
        }

    }
}