﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using WebMMO.Core.DomainModels.Role;
using WebMMO.Core.DomainModels.User;
using WebMMO.Service;

namespace WebMMO.Web.Controllers
{
    [Authorize]
    public abstract class BaseController : Controller
    {
        protected ServicesManager RepositoryService => ServicesManager.RepositoryService;

        private UserDto _currentLoginUser;

        public UserDto CurrentLoginUser => _currentLoginUser;

        protected JsonResult ErrorModelToClient()
        {
            var validationErrors = new List<ValidationResult>();
            foreach (var key in ModelState.Keys)
            {
                if (ModelState.IsValidField(key)) continue;
                var errorMessage = ModelState[key].Errors[0].ErrorMessage;
                if (string.IsNullOrEmpty(errorMessage))
                {
                    errorMessage = "General Error";
                }

                validationErrors.Add(new ValidationResult(key, errorMessage));
            }

            return Json(new ResponseViewModel
            {
                IsSuccess = false,
                ValidationErrors = validationErrors.AsQueryable().ToDictionary(k => k.Property, v => v.ErrorMessage)
            });
        }

        protected JsonResult SuccessToClient(string message, string jsonExtra = null)
        {
            return Json(new ResponseViewModel
            {
                IsSuccess = true,
                Message = message,
                JsonExtra = jsonExtra
            });
        }

        protected JsonResult ErrorToClient(string message)
        {
            return Json(new ResponseViewModel
            {
                IsSuccess = false,
                Message = message
            });
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Store the current logged in user if available.
            if (!string.IsNullOrEmpty(HttpContext.User?.Identity.Name))
            {
                _currentLoginUser = RepositoryService.UserService.GetUserByEmail(this.HttpContext.User.Identity.Name).ResponseObject;
            }
            else
            {
                RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary
                {
                    {"action", "Login"},
                    {"controller", "Account"},
                    {"timeout", "true"}
                };

                filterContext.Result = new RedirectToRouteResult(redirectTargetDictionary);
            }

            base.OnActionExecuting(filterContext);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            //filterContext.ExceptionHandled = true;
            //filterContext.Result = RedirectToAction("Error", "Account", new { Area = "Home" });
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            //Check current status is under maintenance or not

            // Store the current logged in user and other information required for the view if available.
            if (_currentLoginUser != null)
            {

                ViewBag.CurrentLoginUser = this._currentLoginUser;

                // Find out what top level role is the current user/
                RoleDto[] topLevelRoles = RepositoryService.RoleService.GetRoleAll().ResponseObject.ToArray();

                foreach (RoleDto curRole in topLevelRoles)
                {
                    if (this.HttpContext.User.IsInRole(curRole.Name))
                    {
                        ViewBag.CurrentLoginRole = curRole;
                        break;
                    }
                }

            }
            else
            {
                ViewBag.CurrentLoginUser = null;
                ViewBag.CurrentLoginCreditAccount = null;
                ViewBag.CurrentLoginRole = null;
                ViewBag.CurrentLoginCouponValidity = null;
            }
        }

    }

    public class ValidationResult
    {
        public string Property { get; set; }
        public string ErrorMessage { get; set; }
        public ValidationResult(string property, string errorMessage)
        {
            if (string.IsNullOrWhiteSpace(property))
            {
                throw new ArgumentNullException(nameof(Property));
            }

            Property = property;
            ErrorMessage = errorMessage;
        }
    }

    public class ResponseViewModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public Dictionary<string, string> ValidationErrors { get; set; }
        public string JsonExtra { get; set; }
    }
}