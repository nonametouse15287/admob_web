﻿using System.Web.Http;

namespace WebMMO.Web.Controllers
{
    public interface IApiController
    {


    }


    public class ApiBaseController : ApiController, IApiController
    {
        protected const string Version = "v1";
        //protected ApiServicesManager RepositoryService => ApiServicesManager.RepositoryService;
    }
}