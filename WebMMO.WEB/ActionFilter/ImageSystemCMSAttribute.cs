﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebMMO.Web.ActionFilter
{
    public class WebMMOCmsAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.RequestContext.HttpContext.User.Identity.IsAuthenticated)
            {
                //Create permission string based on the requested controller name and action name in the format 'controllername-action'
                string requiredPermission = String.Format("{0}-{1}", filterContext.ActionDescriptor.ControllerDescriptor.ControllerName, filterContext.ActionDescriptor.ActionName);

                //Create an instance of our custom user authorization object passing requesting user's 'Windows Username' into constructor
                WebMMOCmsUser requestingUser = new WebMMOCmsUser(filterContext.RequestContext.HttpContext.User.Identity.Name);
                //Check if the requesting user has the permission to run the controller's action
                if (!requestingUser.HasPermission(requiredPermission) & !requestingUser.IsSysAdmin)
                {
                    //User doesn't have the required permission and is not a SysAdmin, return our custom “401 Unauthorized” access error
                    //Since we are setting filterContext.Result to contain an ActionResult page, the controller's action will not be run.
                    //The custom “401 Unauthorized” access error will be returned to the browser in response to the initial request.
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "action", "Index" }, { "controller", "Unauthorized" } });
                }
                //If the user has the permission to run the controller's action, then filterContext.Result will be uninitialized and
                //executing the controller's action is dependant on whether filterContext.Result is uninitialized.
            }
            else
            {
                //Current absolute Path
                string CurrentAbsolutePath = filterContext.RequestContext.HttpContext.Request.UrlReferrer != null ? filterContext.RequestContext.HttpContext.Request.UrlReferrer.AbsolutePath : "";

                //set defaultURL 
                var defaultLoginView = new RouteValueDictionary(new
                {
                    action = "Index",
                    controller = "Authentication",
                    returnUrl = filterContext.HttpContext.Request.Url.PathAndQuery // Page/Path required login

                    //System.Web.HttpContext.Current.Request.UrlReferrer
                });

                filterContext.Result = new RedirectToRouteResult(defaultLoginView);
            }
        }


    }
}