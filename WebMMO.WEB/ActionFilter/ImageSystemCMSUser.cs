﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebMMO.Core.DomainModels.Permission;
using WebMMO.Core.DomainModels.Role;
using WebMMO.Core.DomainModels.User;
using WebMMO.Service;

namespace WebMMO.Web.ActionFilter
{
    public class WebMMOCmsUser
    {
        protected ServicesManager RepositoryService => ServicesManager.RepositoryService;

        public int UserID { get; set; }
        public bool IsSysAdmin { get; set; }
        public string Username { get; set; }

        private List<RoleDto> Roles = new List<RoleDto>();

        public WebMMOCmsUser(string EmailAddress)
        {
            this.Username = EmailAddress;
            this.IsSysAdmin = false;
            GetDatabaseUserRolesPermissions();
        }

        private void GetDatabaseUserRolesPermissions()
        {

            UserDto user = RepositoryService.UserService.GetUserByEmail(this.Username).ResponseObject;

            if (user != null)
            {
                this.UserID = user.ID;
                List<UserRoleDto> userroles = RepositoryService.UserService.GetUserRolesByUserId(user.ID).ResponseObject.ToList();

                foreach (UserRoleDto _role in userroles)
                {
                    if (_role.RoleID != null)
                    {
                        RoleDto userRole = RepositoryService.RoleService.GetRoleById(_role.RoleID.Value).ResponseObject;

                        List<RolePermissionDto> permissions = RepositoryService.RoleService.GetRolePermissionById(null, _role.RoleID.Value, null).ResponseObject.ToList();


                        foreach (RolePermissionDto rolePermissionDto in permissions)
                        {
                            var permissionDto = new PermissionDto();
                            permissionDto.ID = rolePermissionDto.PermissionID.Value;
                            var objPermission = RepositoryService.RoleService.GetPermission(rolePermissionDto.PermissionID.Value, null, null, null).ResponseObject.FirstOrDefault();
                            permissionDto.PermissionDescription = objPermission.PermissionDescription;

                            RepositoryService.RoleService.InsertPermission(permissionDto);

                        }

                        Roles.Add(userRole);

                        if (!IsSysAdmin) IsSysAdmin = userRole.IsSysAdmin;
                    }
                }

            }
        }

        public bool HasPermission(string requiredPermission)
        {
            bool bFound = false;

            foreach (RoleDto role in this.Roles)
            {
                var xxx = RepositoryService.RoleService.GetPermission(null, null, requiredPermission, null).ResponseObject.ToList();
                bFound = (xxx.Count > 0);
                if (bFound)
                    break;
            }
            return bFound;
        }

        public bool HasRole(string role)
        {
            return (Roles.Where(p => p.Name == role).ToList().Count > 0);
        }

        public bool HasRoles(string roles)
        {
            bool bFound = false;
            string[] _roles = roles.ToLower().Split(';');
            foreach (RoleDto role in this.Roles)
            {
                try
                {
                    bFound = _roles.Contains(role.Name.ToLower());
                    if (bFound)
                        return bFound;
                }
                catch (Exception)
                {
                }
            }
            return bFound;
        }
    }


}