﻿using WebMMO.Core.DomainModels.SSH;
using WebMMO.Core.MessageResponse;
using WebMMO.Web.Areas.SshManage.Models;
using WebMMO.Web.Controllers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;

namespace WebMMO.Web.Areas.SshManage.Controllers
{
    public class ManageSSHController : BaseController
    {
        // GET: SshManage/ManageSSH
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult ManageSSHSearch(int draw = 1, int start = 0, int length = 10)
        {
            var tableParams = new FilterTableParams(Request, start, length, draw);
            var result = RepositoryService.SSHSystemService.SearchSSHSystem(tableParams);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ImportSSH()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ImportSSH(SShImportModels model)
        {
            if (model.FileUploads != null && model.FileUploads.ContentLength > 0)
            {
                List<SSHWareHouseDto> lstInsert = new List<SSHWareHouseDto>();
                if (model.FileUploads.FileName.EndsWith(".txt"))
                {
                    Stream stream = model.FileUploads.InputStream;
                    string m = "";
                    var xxx = new StreamReader(stream);
                    while (xxx.EndOfStream == false)
                    {
                        m = xxx.ReadLine();
                        if (!string.IsNullOrEmpty(m))
                        {
                            string[] arrayobj = m.Split('|');
                            SSHWareHouseDto obj = new SSHWareHouseDto();
                            obj.IpAddress = arrayobj[0];
                            obj.username = arrayobj[1];
                            obj.password = arrayobj[2];
                            obj.CountryCode = arrayobj[3];
                            obj.CreatedDate = DateTime.Now;
                            if (obj.CountryCode == "null")
                            {
                                obj.CountryCode = model.FileUploads.FileName.Replace(".txt", "");
                            }
                            lstInsert.Add(obj);
                        }
                    }
                    RepositoryService.SSHSystemService.InsertSShByList(lstInsert);
                }
                else
                {
                    ViewBag.ErrorMessage = "File khong dung dinh dang";
                }
            }
            return View();
        }
    }
}