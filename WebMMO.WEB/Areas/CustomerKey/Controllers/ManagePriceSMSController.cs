﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMMO.Common.Helpes;
using WebMMO.Common.Utils;
using WebMMO.Core.DomainModels.Sms;
using WebMMO.Core.MessageResponse;
using WebMMO.Web.Areas.CustomerKey.Models;
using WebMMO.Web.Controllers;

namespace WebMMO.Web.Areas.CustomerKey.Controllers
{
    public class ManagePriceSmsController : BaseController
    {
        // GET: CustomerKey/ManagePriceSMS
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult SearchManagePriceSms(int draw = 1, int start = 0, int length = 10, string Name = "")
        {
            var tableParams = new FilterTableParams(Request, start, length, draw);
            var result = RepositoryService.WebMMOService.SearchManagePriceSms(tableParams, Name);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddAndEdit(int? id)
        {
            SmsPriceModels model = new SmsPriceModels();

            if (id != null)
            {
                SmsPriceDto objNew = RepositoryService.WebMMOService.GetSmsPriceById(id.Value);
                model.Id = objNew.Id;
                model.ServicesName = objNew.ServicesName;
                model.ServicesKey = objNew.ServicesKey;
                model.Price = objNew.Price;
                model.ModifiedBy = objNew.ModifiedBy;
                model.ModifiedDate = objNew.ModifiedDate;
                model.CreatedBy = objNew.CreatedBy;
                model.CreatedDate = objNew.CreatedDate;
            }

            return View(model);
        }
        [HttpPost]
        public ActionResult AddAndEdit(SmsPriceModels model)
        {
            if (ModelState.IsValid)
            {
                model.ModifiedBy = CurrentLoginUser.ID;
                model.ModifiedDate = DateTime.UtcNow;
                if (model.Id == 0)
                {
                    model.CreatedBy = CurrentLoginUser.ID;
                    model.CreatedDate = DateTime.UtcNow;
                }

                SmsPriceDto objNew = new SmsPriceDto();

                objNew.Id = model.Id;
                objNew.ServicesName = model.ServicesName;
                objNew.ServicesKey = model.ServicesKey;
                objNew.Price = model.Price;
                objNew.ModifiedBy = model.ModifiedBy;
                objNew.ModifiedDate = model.ModifiedDate;
                objNew.CreatedBy = model.CreatedBy;
                objNew.CreatedDate = model.CreatedDate;


                RepositoryService.WebMMOService.AddAndEditSmsPrice(objNew);
                return RedirectToAction("", "ManagePriceSms", new { Area = "CustomerKey" });
            }

            return View(model);
        }

    }
}