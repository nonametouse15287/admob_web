﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMMO.Common.Helpes;
using WebMMO.Common.Utils;
using WebMMO.Core.DomainModels.Sms;
using WebMMO.Core.DomainModels.WebMMO;
using WebMMO.Core.MessageResponse;
using WebMMO.Web.Areas.CustomerKey.Models;
using WebMMO.Web.Areas.WebMMO.Models;
using WebMMO.Web.Controllers;

namespace WebMMO.Web.Areas.CustomerKey.Controllers
{
    public class ManageKeyController : BaseController
    {
        // GET: CustomerKey/ManageKey
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult SearchManageKey(int draw = 1, int start = 0, int length = 10, string Name = "")
        {
            var tableParams = new FilterTableParams(Request, start, length, draw);
            var result = RepositoryService.WebMMOService.SearchManageKey(tableParams, Name);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddAndEdit(string guidkey)
        {
            AccessTokenUserModels model = new AccessTokenUserModels();

            if (guidkey != null)
            {
                AccessTokenUserDto objNew = RepositoryService.WebMMOService.GetAccessTokenUserGuid(guidkey);
                model.ID = objNew.Id;
                model.AccessToken = objNew.AccessToken;
                model.UserId = objNew.UserId;
                model.BalanceMoney = objNew.BalanceMoney;
                model.BalanceUsed = objNew.BalanceUsed;
                model.BalanceError = objNew.BalanceErorr;

                model.ModifiedBy = objNew.ModifiedBy;
                model.ModifiedDate = objNew.ModifiedDate;
                model.CreatedBy = objNew.CreatedBy;
                model.CreatedDate = objNew.CreatedDate;
                model.Status = objNew.Status;
            }
            else
            {
                model.AccessToken = PasswordUtils.GenerateRequestKey2(26);
            }

            return View(model);
        }
        [HttpPost]
        public ActionResult AddAndEdit(AccessTokenUserModels model)
        {
            if (ModelState.IsValid)
            {
                model.ModifiedBy = CurrentLoginUser.ID;
                model.ModifiedDate = DateTime.UtcNow;
                if (model.ID == 0)
                {
                    model.CreatedBy = CurrentLoginUser.ID;
                    model.CreatedDate = DateTime.UtcNow;
                    model.Status = ConfigHelpers.StatusPending;
                }

                AccessTokenUserDto objNew = new AccessTokenUserDto();
                objNew.Id = model.ID;
                objNew.AccessToken = model.AccessToken;
                objNew.UserId = model.UserId;
                objNew.BalanceMoney = model.BalanceMoney ?? 0;
                objNew.BalanceUsed = model.BalanceUsed ?? 0;
                objNew.BalanceErorr = model.BalanceError ?? 0;
                objNew.ModifiedBy = model.ModifiedBy;
                objNew.ModifiedDate = model.ModifiedDate;
                objNew.CreatedBy = model.CreatedBy;
                objNew.CreatedDate = model.CreatedDate;
                objNew.Status = model.Status;

                RepositoryService.WebMMOService.AddAndEditAccessTokenUser(objNew);
                return RedirectToAction("", "ManageKey", new { Area = "CustomerKey" });
            }

            return View(model);
        }

        public JsonResult NapTienKhach(string keyApprove, int sotien)
        {
            var objUpdate = RepositoryService.WebMMOService.GetAccessTokenUserGuid(keyApprove);

            if (objUpdate != null)
            {
                objUpdate.ModifiedBy = CurrentLoginUser.ID;
                objUpdate.ModifiedDate = DateTime.UtcNow;
                objUpdate.BalanceMoney = objUpdate.BalanceMoney + sotien;
                RepositoryService.WebMMOService.AddAndEditAccessTokenUser(objUpdate);
            }
            return SuccessToClient("đã xong!");
        }
    }
}