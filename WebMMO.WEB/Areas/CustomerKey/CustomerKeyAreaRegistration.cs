﻿using System.Web.Mvc;

namespace WebMMO.Web.Areas.CustomerKey
{
    public class CustomerKeyAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "CustomerKey";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "CustomerKey_default",
                "CustomerKey/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}