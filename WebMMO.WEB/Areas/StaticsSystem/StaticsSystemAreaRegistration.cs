﻿using System.Web.Mvc;

namespace WebMMO.Web.Areas.StaticsSystem
{
    public class StaticsSystemAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "StaticsSystem";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "StaticsSystem_default",
                "StaticsSystem/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}