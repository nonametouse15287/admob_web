﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMMO.Web.Areas.Admin.Models
{
    public class UserRoleResult
    {
        public int RoleID { get; set; }

        public string RoleName { get; set; }

        public bool RoleSelected { get; set; }
    }
}