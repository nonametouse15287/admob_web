﻿using System.ComponentModel.DataAnnotations;

namespace WebMMO.Web.Areas.Admin.Models
{
    public class UserFilterForm
    {
        [Display(Name = "UserName")]
        public string Name { get; set; }
        [Display(Name = "Role")]
        public int? RoleID { get; set; }
        [Display(Name = "Status")]
        public string Status { get; set; }
    }
}