﻿using System.ComponentModel.DataAnnotations;
using WebMMO.Core.DomainModels.Role;

namespace WebMMO.Web.Areas.Admin.Models
{
    public class RoleModel
    {
        public int ID { get; set; }

        [Display(Name = "Role Name")]
        [Required(ErrorMessage = "Please specify the role name.")]
        public string Name { get; set; }

        public string JpName { get; set; }

        [Display(Name = "Role Description")]
        public string Description { get; set; }

        public bool IsSysAdmin { get; set; }

        [Display(Name = "External Role")]
        public bool IsExternal { get; set; }

        [Display(Name = "RankLevel")]
        public int? RankLevel { get; set; }

        [Display(Name = "Auditor")]
        public bool IsAuditor { get; set; }

        public void FromRoleDto(RoleDto objDto)
        {
            ID = objDto.ID;
            Name = objDto.Name;
            JpName = objDto.JpName;
            Description = objDto.Description;
            IsSysAdmin = objDto.IsSysAdmin;
            IsExternal = objDto.IsExternal;
            RankLevel = objDto.RankLevel;
            IsAuditor = objDto.IsAuditor;
        }
    }
}