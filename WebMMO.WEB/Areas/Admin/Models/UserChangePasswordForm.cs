﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebMMO.Web.Areas.Admin.Models
{
    public class UserChangePasswordForm
    {
        public int ID { get; set; }
        public string EmailAddress { get; set; }
        /// <summary>
        /// Gets or sets the new password of the <see cref="ChangePasswordForm"/>
        /// </summary>    
        [Display( Name = "New Password")]
        [Required(ErrorMessage = "Please enter the new password.")]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "Password must be at least 6 to 30 characters.")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the confirmed new password of the <see cref="ChangePasswordForm"/>
        /// </summary>    
        [Display(Name = "Retype New Password")]
        [Required(ErrorMessage = "Please re-enter the new password.")]
        [System.Web.Mvc.Compare("Password", ErrorMessage = "The new password and confirm password do not match.")]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "Password must be at least 6 to 30 characters.")]
        public string ConfirmPassword { get; set; }
    }

}