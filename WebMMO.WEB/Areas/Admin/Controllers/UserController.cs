﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMMO.Web.Areas.Admin.Models;
using WebMMO.Common.Helpes;
using WebMMO.Common.Utils;
using WebMMO.Core.DomainModels.Role;
using WebMMO.Core.DomainModels.User;
using WebMMO.Web.Controllers;

namespace WebMMO.Web.Areas.Admin.Controllers
{
    public class UserController : BaseController
    {
        // GET: Admin/User
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ManageCbUser()
        {
            UserFilterForm userFilterForm = new UserFilterForm();

            List<UserResult> lstUserDtos = RepositoryService.UserService.GetQueryCbUser(CurrentLoginUser.OrganizationID, userFilterForm.Status, userFilterForm.RoleID).ResponseObject.ToList();
            int rolerank =
                RepositoryService.RoleService.GetHighesRoleRankByUserId(CurrentLoginUser.ID).ResponseObject;

            List<RoleDto> lstRole = RepositoryService.RoleService.GetRoleQuery(false, false, rolerank).ResponseObject.ToList();

            ViewBag.Roles = new SelectList(lstRole, "ID", "Name");

            ViewBag.Statuses = SelectListModel.getStatuses();
            ViewBag.DataList = lstUserDtos;

            return View(userFilterForm);
        }

        [HttpPost]
        public ActionResult ManageCbUser(UserFilterForm userFilterForm)
        {

            return View(userFilterForm);
        }

        [HttpGet]
        public ActionResult EditCbUser(int id)
        {
            var currentHighestRoleRank = RepositoryService.RoleService.GetHighesRoleRankByUserId(CurrentLoginUser.ID).ResponseObject;
            var findHighesRoleRank = RepositoryService.RoleService.GetHighesRoleRankByUserId(id).ResponseObject;
            //Check if current login user can able to change role for the editted user
            if (currentHighestRoleRank < findHighesRoleRank)
            {
                ViewBag.CanEditRole = 1;
            }

            var user = RepositoryService.UserService.GetUserById(id).ResponseObject;

            return View(user);
        }

        [HttpPost]
        public ActionResult EditCbUser(UserDto user, string submitButton)
        {
            if (ModelState.IsValid)
            {
                var objUserDto = RepositoryService.UserService.GetUserById(user.ID).ResponseObject;
                objUserDto.FirstName = user.FirstName;
                objUserDto.LastName = user.LastName;
                objUserDto.ContactNumber = user.ContactNumber;
                switch (submitButton)
                {
                    case "Enable":
                        objUserDto.IsActive = true;
                        break;
                    case "Disable":
                        objUserDto.IsActive = false;
                        break;
                }

                RepositoryService.UserService.UpdateUser(objUserDto);

                return RedirectToAction("ManageCbUser");
            }
            return View(user);
        }

        [HttpGet]
        public ActionResult SelectListRoles(int UserID)
        {
            List<UserRoleResult> roles = new List<UserRoleResult>();
            int rolerank = RepositoryService.RoleService.GetHighesRoleRankByUserId(CurrentLoginUser.ID).ResponseObject;

            List<RoleDto> availableRoles = RepositoryService.RoleService.GetRoleQuery(false, false, rolerank).ResponseObject.ToList();
            foreach (RoleDto availableRole in availableRoles)
            {
                UserRoleResult result = new UserRoleResult();
                result.RoleID = availableRole.ID;
                result.RoleName = availableRole.Name;

                IEnumerable<UserRoleDto> objUserRole = RepositoryService.UserService.GetUserRolesById(UserID, availableRole.ID).ResponseObject;

                result.RoleSelected = objUserRole.Any();

                roles.Add(result);
            }

            return PartialView(roles);
        }

        [HttpPost]
        public ActionResult SelectListRoles(int UserID, List<int> SelectedRole)
        {

            //Delete UserRoles by Userid
            RepositoryService.UserService.DeleteUserRoleByUserId(UserID);

            foreach (int newRole in SelectedRole)
            {
                UserRoleDto newUserRole = new UserRoleDto();
                newUserRole.UserID = UserID;
                newUserRole.RoleID = newRole;
                RepositoryService.UserService.AddUserRole(newUserRole);
            }

            var role = RepositoryService.RoleService.GetRoleByUserId(UserID).ResponseObject;
            return Content(role, "Text/Plain");
        }

        [HttpGet]
        public ActionResult NewCbUser()
        {
            int rolerank = RepositoryService.RoleService.GetHighesRoleRankByUserId(CurrentLoginUser.ID).ResponseObject;

            List<RoleDto> lstRole = RepositoryService.RoleService.GetRoleQuery(false, false, rolerank).ResponseObject.ToList();

            ViewBag.UserTypes = new SelectList(lstRole, "ID", "Name");

            // Set the default organization and role for the new user.
            UserDto user = new UserDto();
            user.OrganizationID = CurrentLoginUser.OrganizationID ?? 1;

            return View(user);
        }


        [HttpPost]
        public ActionResult NewCbUser(UserDto user, int RoleID)
        {
            // Check if the email already existing.
            bool CheckEmail = RepositoryService.UserService.CheckEmailUser(user.EmailAddress).ResponseObject;

            if (CheckEmail)
            {
                ModelState.AddModelError("EmailAddress", "The specified email address is already taken.");
            }

            if (ModelState.IsValid)
            {

                string generatedPassword = PasswordUtils.CreateRandomPassword(8);
                //string defaultPassword = "password123";
                user.Password = PasswordUtils.EncryptPassword(user.EmailAddress, generatedPassword);
                user.IsActive = true;
                user.CreatedByUserID = CurrentLoginUser.ID;
                user.DateCreated = DateTime.Now;
                RepositoryService.UserService.AddUser(user);

                var userInsert = RepositoryService.UserService.GetUserByEmail(user.EmailAddress).ResponseObject;
                //Add Role
                UserRoleDto userrole = new UserRoleDto
                {
                    UserID = userInsert.ID,
                    RoleID = RoleID
                };

                RepositoryService.UserService.AddUserRole(userrole);


                return RedirectToAction("ManageCbUser");

            }

            int rolerank = RepositoryService.RoleService.GetHighesRoleRankByUserId(CurrentLoginUser.ID).ResponseObject;
            List<RoleDto> lstRole = RepositoryService.RoleService.GetRoleQuery(false, false, rolerank).ResponseObject.ToList();

            ViewBag.UserTypes = new SelectList(lstRole, "ID", "Name", RoleID);

            return View(user);
        }

        [HttpGet]
        public ActionResult ListStates(int? countryID)
        {
            List<StateDto> result = RepositoryService.UserService.GetStateByCountryId(countryID.Value).ResponseObject.ToList();
            // Convert the results to JSON.
            List<object> jsonResult = result.ConvertAll<object>(delegate (StateDto s)
            {
                return new { label = s.Name, value = s.ID };
            });

            return Json(jsonResult, "text/plain", JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditCbUserPassword(int id)
        {
            UserDto objUser = RepositoryService.UserService.GetUserById(id).ResponseObject;

            UserChangePasswordForm objUserChangePasswordForm = new UserChangePasswordForm();
            if (objUser != null)
            {
                objUserChangePasswordForm.ID = objUser.ID;
                objUserChangePasswordForm.EmailAddress = objUser.EmailAddress;
            }
            return View(objUserChangePasswordForm);
        }

        [HttpPost]
        public ActionResult EditCbUserPassword(UserChangePasswordForm userChangePasswordForm)
        {
            if (CurrentLoginUser.Password == null)
            {
                ModelState["CurrentPassword"].Errors.Clear();
            }

            if (ModelState.IsValid)
            {
                UserDto user = RepositoryService.UserService.GetUserById(userChangePasswordForm.ID).ResponseObject;
                user.Password = PasswordUtils.EncryptPassword(userChangePasswordForm.EmailAddress, userChangePasswordForm.Password);

                RepositoryService.UserService.UpdateUser(user);

                return RedirectToAction("ManageCbUser");
            }
            return View(userChangePasswordForm);
        }

        /// <summary>
        /// Action for editing the user profile information of current user.
        /// </summary>        
        [HttpGet]
        public ActionResult EditMyProfile()
        {
            var user = RepositoryService.UserService.GetUserById(CurrentLoginUser.ID).ResponseObject;
            return View(user);
        }

        /// <summary>
        /// Action for processing the postback when editing the user profile information of current user.
        /// </summary>        
        [HttpPost]
        public ActionResult EditMyProfile(UserDto user, HttpPostedFileBase photoFile)
        {
            // Validate the uploaded photo if they exist.
            if (photoFile != null)
            {
                if (photoFile.ContentType != "image/jpeg" && photoFile.ContentType != "image/png")
                {
                    ModelState.AddModelError("Photo", "Your uploaded photo is not a supported image format.");
                }
                else if (photoFile.ContentLength > 512000)
                {
                    ModelState.AddModelError("Photo", "Your uploaded photo exceeds the permitted size.");
                }
            }

            ModelState.Remove("ContactNumber");
            

            if (ModelState.IsValid)
            {
                // Resample the uploaded photo to 200x200 and save them as PNG.
                if (photoFile != null)
                {
                    Image photoImage = Image.FromStream(photoFile.InputStream);

                    double scaleFactor = Math.Min(200.0 / photoImage.Width, 200.0 / photoImage.Height);
                    Image finalPhotoImage = new Bitmap(photoImage, new Size((int)(scaleFactor * photoImage.Width), (int)(scaleFactor * photoImage.Height)));

                    // Save all uploaded photo to PNG format.
                    MemoryStream stream = new MemoryStream();
                    finalPhotoImage.Save(stream, ImageFormat.Png);
                    user.Photo = stream.ToArray();

                    // Clean up all image resources.
                    photoImage.Dispose();
                    finalPhotoImage.Dispose();
                }

                // Save to the database. We will flag the modified properties manually so that
                // we don't update the current email address and password.

                user.ID = CurrentLoginUser.ID;
                user.ModifiedByUserID = CurrentLoginUser.ID;
                user.DateModified = DateTime.Now;

                //Update User
                RepositoryService.UserService.UpdateUser(user);

                return View("EditProfile-Complete");
            }

            return View(user);
        }

        public ActionResult RemovePhoto()
        {
            //ActionResult result = Content("False", "Text/Plain");
            var user = RepositoryService.UserService.GetUserById(CurrentLoginUser.ID).ResponseObject;

            if (user != null)
            {
                user.Photo = null;
                RepositoryService.UserService.UpdateUser(user);
            }
            return RedirectToAction("EditMyProfile");
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            ViewBag.HasCurrentPassword = CurrentLoginUser.Password != null;
            return View();
        }

        /// <summary>
        /// Action for processing the postback when changing the password of current user.
        /// </summary>
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordForm changePasswordForm)
        {
            // Verifiy if the current password is correct.
            if (CurrentLoginUser.Password != null && !PasswordUtils.VerifyPassword(CurrentLoginUser.EmailAddress, changePasswordForm.CurrentPassword, CurrentLoginUser.Password))
            {
                ModelState.AddModelError("CurrentPassword", "You had entered an incorrect password.");
            }
            else if (CurrentLoginUser.Password == null)
            {
                // Remove validations on the current password if the user does not have one, i.e. social login.
                ModelState["CurrentPassword"].Errors.Clear();
            }

            if (ModelState.IsValid)
            {
                // Save to the database. We will flag to update only the password.
                UserDto user = new UserDto();
                user.ID = CurrentLoginUser.ID;
                user.Password = PasswordUtils.EncryptPassword(CurrentLoginUser.EmailAddress, changePasswordForm.Password);

                /*Update Password*/
                var resultUpdate = RepositoryService.UserService.UpdatePassword(user).ResponseObject;

                return View("ChangePassword-Complete");
            }

            ViewBag.HasCurrentPassword = CurrentLoginUser.Password != null;
            return View(changePasswordForm);
        }

    }
}