﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebMMO.Web.Areas.Admin.Models;
using WebMMO.Core.DomainModels.Menu;
using WebMMO.Core.DomainModels.Permission;
using WebMMO.Core.DomainModels.Role;
using WebMMO.Web.Controllers;

namespace WebMMO.Web.Areas.Admin.Controllers
{
    public class PermissionController : BaseController
    {
        // GET: Admin/Permission
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ManageMenu(int? MenuParentID)
        {
            if (MenuParentID == null)
            {
                MenuParentID = 0;
            }
            List<MenuDto> lstMenu = RepositoryService.MenuService.GetMenuList(MenuParentID, null).ResponseObject.ToList();

            if (MenuParentID == null || MenuParentID == 0)
            {
                ViewBag.Root = 1;
            }
            else
            {
                MenuDto currentMenu = RepositoryService.MenuService.GetMenuById(MenuParentID.Value).ResponseObject;
                ViewBag.MenuName = currentMenu.Name;
                ViewBag.BackMenuID = currentMenu.MenuParentID;
            }

            ViewBag.MenuParentID = MenuParentID;
            ViewBag.MaxSeq = lstMenu.Count;
            return View(lstMenu);
        }

        public ActionResult MenuMoveUp(int id)
        {
            MenuDto menu = RepositoryService.MenuService.GetMenuById(id).ResponseObject;
            int currentSeq = menu.Seq;

            var sortMenu = RepositoryService.MenuService.GetMenuList(menu.MenuParentID, currentSeq - 1).ResponseObject.FirstOrDefault();

            menu.Seq = currentSeq - 1;
            if (sortMenu != null)
            {
                sortMenu.Seq = currentSeq;

                RepositoryService.MenuService.UpdateMenu(menu);
                RepositoryService.MenuService.UpdateMenu(sortMenu);
            }

            return RedirectToAction("ManageMenu", new { MenuParentID = menu.MenuParentID });
        }

        public ActionResult MenuMoveDown(int id)
        {
            MenuDto menu = RepositoryService.MenuService.GetMenuById(id).ResponseObject;
            int currentSeq = menu.Seq;

            var sortMenu = RepositoryService.MenuService.GetMenuList(menu.MenuParentID, currentSeq + 1).ResponseObject.FirstOrDefault();

            menu.Seq = currentSeq + 1;

            if (sortMenu != null)
            {
                sortMenu.Seq = currentSeq;

                RepositoryService.MenuService.UpdateMenu(menu);
                RepositoryService.MenuService.UpdateMenu(sortMenu);
            }

            return RedirectToAction("ManageMenu", new { MenuParentID = menu.MenuParentID });
        }

        [HttpGet]
        public ActionResult AddMenu(int? MenuParentID)
        {
            if (MenuParentID == null)
            {
                MenuParentID = 0;
            }
            MenuDto menu = new MenuDto();
            if (MenuParentID != null)
            {
                menu.MenuParentID = MenuParentID;

                int Seq = RepositoryService.MenuService.GetMenuByParentId(MenuParentID.Value).ResponseObject.ToList().Count + 1;

                menu.Seq = Seq;
            }
            else
            {
                menu.Seq = 1;
            }

            ViewBag.MenuParentID = MenuParentID;
            return View(menu);
        }

        [HttpPost]
        public ActionResult AddMenu(MenuDto menu)
        {
            if (ModelState.IsValid)
            {
                var menuId = RepositoryService.MenuService.InsertMenu(menu).ResponseObject;

                if (!string.IsNullOrEmpty(menu.Link))
                {
                    //Add default permission
                    string permissionDesc = menu.Link.Replace("/", "-").ToLower();
                    PermissionDto permission = new PermissionDto
                    {
                        Name = "View",
                        PermissionDescription = permissionDesc,
                        MenuID = menuId,
                        IsDefault = true
                    };

                    RepositoryService.RoleService.InsertPermission(permission);
                }

                return RedirectToAction("ManageMenu", new { MenuParentID = menu.MenuParentID });
            }

            ViewBag.MenuParentID = menu.MenuParentID;
            return View(menu);
        }

        [HttpGet]
        public ActionResult EditMenu(int id)
        {
            MenuDto menu = RepositoryService.MenuService.GetMenuById(id).ResponseObject;

            List<PermissionDto> permissions =
                RepositoryService.RoleService.GetPermission(null, id, null, null).ResponseObject.ToList();

            ViewBag.Permissions = permissions;
            ViewBag.HasSubMenu = RepositoryService.MenuService.GetMenuByParentId(id).ResponseObject.Any();
            return View(menu);
        }

        [HttpPost]
        public ActionResult EditMenu(MenuDto menu)
        {
            if (ModelState.IsValid)
            {
                MenuDto menuUpdate = RepositoryService.MenuService.GetMenuById(menu.ID).ResponseObject;
                menuUpdate.Link = menu.Link;
                menuUpdate.MenuParentID = menu.MenuParentID;
                menuUpdate.Seq = menu.Seq;
                menuUpdate.Name = menu.Name;
                RepositoryService.MenuService.UpdateMenu(menuUpdate);

                //Change the default permission link
                if (!string.IsNullOrEmpty(menu.Link))
                {
                    string permissionDesc = menu.Link.Replace("/", "-").ToLower();

                    PermissionDto permission = RepositoryService.RoleService.GetPermission(null, menu.ID, null, null).ResponseObject.FirstOrDefault();

                    if (permission != null && permission.IsDefault == true)
                    {
                        permission.PermissionDescription = permissionDesc;
                    }
                    else
                    {
                        //add new because permission not found, but menu found
                        PermissionDto insertPermission = new PermissionDto();
                        insertPermission.MenuID = menu.ID;
                        insertPermission.Name = menu.Name;
                        insertPermission.IsDefault = true; ;
                        insertPermission.PermissionDescription = permissionDesc;
                        RepositoryService.RoleService.InsertPermission(insertPermission);
                    }

                }

                return RedirectToAction("ManageMenu", new { MenuParentID = menu.MenuParentID });
            }
            return View(menu);
        }

        public ActionResult DeleteMenu(int id)
        {

            MenuDto menu = RepositoryService.MenuService.GetMenuById(id).ResponseObject;

            int? MenuParentID = menu.MenuParentID;

            if (MenuParentID == null)
            {
                List<MenuDto> MenuSubList = RepositoryService.MenuService.GetMenuByParentId(id).ResponseObject.ToList();

                foreach (var objSubMenu in MenuSubList)
                {
                    //Need to remove Permissions related to the menu

                    List<PermissionDto> subpermissions = RepositoryService.RoleService.GetPermission(null, objSubMenu.ID, null, null).ResponseObject.ToList();

                    foreach (var permission in subpermissions)
                    {
                        //remove from RolePermission

                        List<RolePermissionDto> lstRolePermissionDtos =
                            RepositoryService.RoleService.GetRolePermissionById(null, null, permission.ID).ResponseObject.ToList();

                        foreach (var objRolePermission in lstRolePermissionDtos)
                        {
                            //db.RolePermissions.Remove(objRolePermission);
                            //db.SaveChanges();
                            RepositoryService.RoleService.DeleteRolePermissionById(objRolePermission.ID);

                        }

                        RepositoryService.RoleService.DeletePermissionById(permission.ID);

                    }
                    //remove in RoleMenu

                    List<RoleMenuDto> RoleSubMenuList = RepositoryService.MenuService.GetRoleMenu(objSubMenu.ID, null).ResponseObject.ToList();

                    foreach (var objRoleMenu in RoleSubMenuList)
                    {
                        RepositoryService.MenuService.DeleteRoleMenu(objRoleMenu.ID);
                    }
                }
            }
            //Need to remove Permissions related to the menu
            //List<Permission> permissions = db.Permissions.Where(p => p.MenuID == id).ToList();

            List<PermissionDto> permissions = RepositoryService.RoleService.GetPermission(null, id, null, null).ResponseObject.ToList();
            foreach (var permission in permissions)
            {
                List<RolePermissionDto> lstRolePermissionDtos =
                           RepositoryService.RoleService.GetRolePermissionById(null, null, permission.ID).ResponseObject.ToList();

                foreach (var objRolePermission in lstRolePermissionDtos)
                {
                    RepositoryService.RoleService.DeleteRolePermissionById(objRolePermission.ID);
                }

                RepositoryService.RoleService.DeletePermissionById(permission.ID);
            }

            //remove in RoleMenu
            List<RoleMenuDto> RoleMenuList = RepositoryService.MenuService.GetRoleMenu(menu.ID, null).ResponseObject.ToList();

            foreach (var objRoleMenu in RoleMenuList)
            {
                RepositoryService.MenuService.DeleteRoleMenu(objRoleMenu.ID);
            }

            RepositoryService.MenuService.DeleteMenu(menu.ID);

            return RedirectToAction("ManageMenu", new { MenuParentID = MenuParentID });
        }

        public ActionResult ListPermissions(int MenuID)
        {
            List<PermissionDto> permissions = RepositoryService.RoleService.GetPermission(null, MenuID, null, null).ResponseObject.ToList();

            return PartialView(permissions);
        }

        [HttpGet]
        public ActionResult AddPermission(int MenuID)
        {
            return View(new PermissionDto() { MenuID = MenuID, IsDefault = false });
        }

        [HttpPost]
        public ActionResult AddPermission(PermissionDto permission)
        {
            //check before insert
            List<PermissionDto> permissions =
                RepositoryService.RoleService.GetPermission(null, permission.MenuID, permission.PermissionDescription,
                    null).ResponseObject.ToList();

            if (permissions.Count > 0)
            {
                ModelState.AddModelError("PermissionDescription", "Cannot add duplicate permission description");
            }

            if (ModelState.IsValid)
            {

                RepositoryService.RoleService.InsertPermission(permission);

                return RedirectToAction("EditMenu", new { id = permission.MenuID });
            }
            return View(permission);
        }

        [HttpGet]
        public ActionResult EditPermission(int PermissionID)
        {
            PermissionDto permission =
                RepositoryService.RoleService.GetPermission(PermissionID, null, null, null)
                    .ResponseObject.FirstOrDefault();
            return View(permission);
        }

        [HttpPost]
        public ActionResult EditPermission(PermissionDto permission)
        {
            //check before update
            List<PermissionDto> permissions =
                RepositoryService.RoleService.GetPermission(null, null, permission.PermissionDescription, null)
                    .ResponseObject.ToList();
            permissions = permissions.Where(p => p.ID != permission.ID).ToList();

            if (permissions.Count > 0)
            {
                ModelState.AddModelError("PermissionDescription", "Cannot add duplicate permission description");
            }

            if (ModelState.IsValid)
            {
                RepositoryService.RoleService.UpdatePermission(permission);
                return RedirectToAction("EditMenu", new { id = permission.MenuID });
            }
            return View(permission);
        }

        [HttpGet]
        public ActionResult DeletePermission(int id)
        {
            //ActionResult result = Content("False", "Text/Plain");
            PermissionDto permission = RepositoryService.RoleService.GetPermission(id, null, null, null).ResponseObject.FirstOrDefault();
            if (permission != null)
            {
                if (permission.IsDefault != true)
                {
                    RepositoryService.RoleService.DeletePermissionById(permission.ID);
                }
            }

            return RedirectToAction("EditMenu", new { id = permission.MenuID });
        }

        public ActionResult ManageRole()
        {
            List<RoleDto> roles = RepositoryService.RoleService.GetRoleAll().ResponseObject.ToList();
            List<RoleModel> lstRole = new List<RoleModel>();
            foreach (var item in roles)
            {
                RoleModel objNew = new RoleModel();
                objNew.FromRoleDto(item);
                lstRole.Add(objNew);
            }
            return View(lstRole);
        }

        [HttpGet]
        public ActionResult AddRole()
        {
            RoleModel objDto = new RoleModel();
            return View(objDto);
        }

        [HttpPost]
        public ActionResult AddRole(RoleModel objDto)
        {
            if (ModelState.IsValid)
            {
                RoleDto objNew = new RoleDto
                {
                    ID = objDto.ID,
                    Name = objDto.Name,
                    JpName = objDto.JpName,
                    Description = objDto.Description,
                    IsSysAdmin = objDto.IsSysAdmin,
                    IsExternal = objDto.IsExternal,
                    RankLevel = objDto.RankLevel,
                    IsAuditor = objDto.IsAuditor,
                };

                RepositoryService.RoleService.InsertRoles(objNew);

                return RedirectToAction("ManageRole");
            }
            return View(objDto);
        }

        public ActionResult EditRole(int id)
        {
            RoleDto role = RepositoryService.RoleService.GetRoleById(id).ResponseObject;
            RoleModel objModel = new RoleModel();
            objModel.FromRoleDto(role);
            return View(objModel);
        }

        [HttpPost]
        public ActionResult EditRole(RoleModel objDto)
        {
            if (ModelState.IsValid)
            {
                var currentRole = RepositoryService.RoleService.GetRoleById(objDto.ID).ResponseObject;
                currentRole.ID = objDto.ID;
                currentRole.Name = objDto.Name;
                currentRole.JpName = objDto.JpName;
                currentRole.Description = objDto.Description;
                currentRole.IsSysAdmin = objDto.IsSysAdmin;
                currentRole.IsExternal = objDto.IsExternal;
                currentRole.RankLevel = objDto.RankLevel;
                currentRole.IsAuditor = objDto.IsAuditor;
                RepositoryService.RoleService.UpdateRole(currentRole);
                return RedirectToAction("ManageRole");
            }
            return View(objDto);
        }

        public ActionResult DeleteRole(int id)
        {

            ////remove RoleMenu, RolePermission,UserRole
            //List<RoleMenu> roleMenus = db.RoleMenus.Where(p => p.RoleID == id).ToList();
            //foreach (var rolemenu in roleMenus)
            //{
            //    db.RoleMenus.Remove(rolemenu);
            //    db.SaveChanges();
            //}

            //List<RolePermission> rolePermissions = db.RolePermissions.Where(p => p.RoleID == id).ToList();
            //foreach (var rolepermission in rolePermissions)
            //{
            //    db.RolePermissions.Remove(rolepermission);
            //    db.SaveChanges();
            //}


            //List<UserRole> userRoles = db.UserRoles.Where(p => p.RoleID == id).ToList();
            //foreach (var userroles in userRoles)
            //{
            //    db.UserRoles.Remove(userroles);
            //    db.SaveChanges();

            //    List<NotificationAlert> objNotificationList = db.NotificationAlerts.Where(c => c.UserID == userroles.UserID).ToList();
            //    foreach (var objNotification in objNotificationList)
            //    {
            //        db.NotificationAlerts.Remove(objNotification);
            //        db.SaveChanges();
            //    }


            //    List<User> usersList = db.Users.Where(c => c.ID == userroles.UserID).ToList();
            //    foreach (var userRemove in usersList)
            //    {
            //        db.Users.Remove(userRemove);
            //        db.SaveChanges();
            //    }


            //}

            //Role role = db.Roles.SingleOrDefault(r => r.ID == id);
            //db.Roles.Remove(role);
            //db.SaveChanges();

            return RedirectToAction("ManageRole");
        }

        public ActionResult RoleAccess(int id, int MenuParentID)
        {
            RoleDto role = RepositoryService.RoleService.GetRoleById(id).ResponseObject;
            ViewBag.MenuParentID = MenuParentID;

            RoleModel model = new RoleModel();
            model.FromRoleDto(role);
            return View(model);
        }

        public ActionResult ListMenuAccess(int RoleID, int MenuParentID)
        {
            List<MenuDto> menus;

            if (MenuParentID == -1 || MenuParentID == 0)
            {
                menus = RepositoryService.MenuService.GetMenuList(0, null).ResponseObject.ToList();
                ViewBag.Root = 1;
            }
            else
            {
                menus = RepositoryService.MenuService.GetMenuByParentId(MenuParentID)
                        .ResponseObject.OrderBy(p => p.Seq)
                        .ToList();
                MenuDto currentMenu = RepositoryService.MenuService.GetMenuById(MenuParentID).ResponseObject;
                ViewBag.MenuName = currentMenu.Name;
            }

            List<MenuAccess> accesslist = new List<MenuAccess>();

            foreach (var menu in menus)
            {
                MenuAccess access = new MenuAccess();

                access.ID = menu.ID;
                access.Name = menu.Name;
                access.HasSubMenu = RepositoryService.MenuService.GetMenuByParentId(menu.ID).ResponseObject.Any();
                access.Enable = checkRoleMenuAccess(RoleID, menu.ID);

                accesslist.Add(access);
            }

            ViewBag.RoleID = RoleID;

            return PartialView(accesslist);
        }

        private bool checkRoleMenuAccess(int RoleID, int MenuID)
        {
            RoleMenuDto access = RepositoryService.MenuService.GetRoleMenu(MenuID, RoleID).ResponseObject.FirstOrDefault();

            if (access == null)
                return false;
            else
                return true;
        }

        public ActionResult EnableMenuAccess(int RoleID, int MenuID)
        {
            RoleMenuDto newaccess = new RoleMenuDto
            {
                RoleID = RoleID,
                MenuID = MenuID
            };
            RepositoryService.MenuService.InsertRoleMenu(newaccess);

            //Also Add into default view permission
            List<PermissionDto> lstPermissionDtos =
                RepositoryService.RoleService.GetPermission(null, MenuID, null, null).ResponseObject.ToList();

            PermissionDto permission = lstPermissionDtos.FirstOrDefault(p => p.IsDefault == true);

            if (permission != null)
            {
                RolePermissionDto priv = new RolePermissionDto();
                priv.PermissionID = permission.ID;
                priv.RoleID = RoleID;
                RepositoryService.RoleService.InsertRolePermission(priv);
            }

            MenuDto menu = RepositoryService.MenuService.GetMenuById(MenuID).ResponseObject;

            int menuParentId;
            if (menu.MenuParentID == null)
                menuParentId = -1;
            else
                menuParentId = (int)menu.MenuParentID;


            return RedirectToAction("RoleAccess", new { id = RoleID, MenuParentID = menuParentId });
        }

        public ActionResult EnableAllMenuAccess(int RoleID, int MenuID)
        {
            RoleMenuDto access = RepositoryService.MenuService.GetRoleMenu(MenuID, RoleID).ResponseObject.FirstOrDefault();

            if (access == null)
            {
                RoleMenuDto newaccess = new RoleMenuDto();
                newaccess.RoleID = RoleID;
                newaccess.MenuID = MenuID;
                RepositoryService.MenuService.InsertRoleMenu(newaccess);

            }

            List<MenuDto> menus = RepositoryService.MenuService.GetMenuByParentId(MenuID).ResponseObject.ToList();

            foreach (var menu in menus)
            {
                RoleMenuDto existaccess = RepositoryService.MenuService.GetRoleMenu(MenuID, RoleID).ResponseObject.FirstOrDefault();

                if (existaccess == null)
                {
                    RoleMenuDto newaccess = new RoleMenuDto();
                    newaccess.RoleID = RoleID;
                    newaccess.MenuID = menu.ID;
                    RepositoryService.MenuService.InsertRoleMenu(newaccess);

                    //Also Add into default view permission
                    List<PermissionDto> lstPermissionDtos = RepositoryService.RoleService.GetPermission(null, MenuID, null, null).ResponseObject.ToList();
                    PermissionDto permission = lstPermissionDtos.FirstOrDefault(p => p.IsDefault == true);

                    if (permission != null)
                    {
                        RolePermissionDto priv = new RolePermissionDto();
                        priv.PermissionID = permission.ID;
                        priv.RoleID = RoleID;
                        RepositoryService.RoleService.InsertRolePermission(priv);
                    }
                }

            }

            MenuDto curmenu = RepositoryService.MenuService.GetMenuById(MenuID).ResponseObject;
            int menuParentId;
            if (curmenu.MenuParentID == null)
                menuParentId = -1;
            else
                menuParentId = (int)curmenu.MenuParentID;


            return RedirectToAction("RoleAccess", new { id = RoleID, MenuParentID = menuParentId });
        }

        public ActionResult DisableMenuAccess(int RoleID, int MenuID)
        {

            RoleMenuDto access = RepositoryService.MenuService.GetRoleMenu(MenuID, RoleID).ResponseObject.FirstOrDefault();

            if (access != null)
            {
                RepositoryService.MenuService.DeleteRoleMenu(access.ID);
            }

            List<PermissionDto> permissions =
                RepositoryService.RoleService.GetPermission(null, MenuID, null, null).ResponseObject.ToList();

            if (permissions.Count > 0)
            {
                foreach (var permission in permissions)
                {
                    RolePermissionDto priv = RepositoryService.RoleService.GetRolePermissionById(null, RoleID, permission.ID).ResponseObject.FirstOrDefault();

                    if (priv != null)
                    {
                        RepositoryService.RoleService.DeleteRolePermissionById(priv.ID);
                    }
                }
            }

            MenuDto menu = RepositoryService.MenuService.GetMenuById(MenuID).ResponseObject;
            int menuParentId;
            if (menu.MenuParentID == null)
                menuParentId = -1;
            else
                menuParentId = (int)menu.MenuParentID;


            return RedirectToAction("RoleAccess", new { id = RoleID, MenuParentID = menuParentId });
        }
        [HttpGet]
        public ActionResult ViewRolePermission(int RoleID, int MenuID)
        {

            List<PermissionDto> permissions =
                 RepositoryService.RoleService.GetPermission(null, MenuID, null, null).ResponseObject.ToList();

            List<RolePermissionResult> result = new List<RolePermissionResult>();

            foreach (var permission in permissions)
            {
                RolePermissionResult curResult = new RolePermissionResult();

                RolePermissionDto rolepermission =
                    RepositoryService.RoleService.GetRolePermissionById(null, RoleID, permission.ID)
                        .ResponseObject.FirstOrDefault();

                curResult.HasPermission = rolepermission != null;
                curResult.PermissionID = permission.ID;
                curResult.PermissionName = permission.Name;

                result.Add(curResult);
            }
            RoleDto role = RepositoryService.RoleService.GetRoleById(RoleID).ResponseObject;

            MenuDto menu = RepositoryService.MenuService.GetMenuById(MenuID).ResponseObject;

            ViewBag.RoleID = RoleID;
            ViewBag.MenuID = MenuID;
            ViewBag.RoleName = role.Name;
            ViewBag.MenuName = menu.Name;

            return View(result);
        }

        [HttpPost]
        public ActionResult ViewRolePermission(int RoleID, int MenuID, List<RolePermissionResult> result)
        {
            foreach (var curResult in result)
            {
                RolePermissionDto permission =
                    RepositoryService.RoleService.GetRolePermissionById(null, RoleID, curResult.PermissionID)
                        .ResponseObject.FirstOrDefault();

                if (curResult.HasPermission)
                {
                    if (permission == null)
                    {
                        //Add permission record
                        RolePermissionDto newpermission = new RolePermissionDto();
                        newpermission.RoleID = RoleID;
                        newpermission.PermissionID = curResult.PermissionID;
                        RepositoryService.RoleService.InsertRolePermission(newpermission);
                    }
                }
                else
                {
                    if (permission != null)
                    {
                        //Remove permission record
                        RepositoryService.RoleService.DeleteRolePermissionById(permission.ID);
                    }
                }
            }

            MenuDto menu = RepositoryService.MenuService.GetMenuById(MenuID).ResponseObject;

            return RedirectToAction("RoleAccess", new { id = RoleID, MenuParentID = menu.MenuParentID });
        }
    }
}