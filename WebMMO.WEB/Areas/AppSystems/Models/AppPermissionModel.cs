﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebMMO.Web.Areas.AppSystems.Models
{
    public class AppPermissionModel
    {

        [Display(Name = "ID")]
        public int ID { get; set; }
        [Key]
        [Display(Name = "Guid")]
        public Guid Guid { get; set; }
        [MaxLength(150)]
        [Display(Name = "App Name")]
        [Required]
        public string AppName { get; set; }
        [MaxLength(150)]
        [Display(Name = "App Package")]
        [Required]
        public string AppPackage { get; set; }
        [MaxLength]
        [Display(Name = "List Permission")]
        public string ListPermission { get; set; }
        [Display(Name = "Created Date")]
        public DateTime? CreatedDate { get; set; }
        [Display(Name = "Created By")]
        public int? CreatedBy { get; set; }
        [Display(Name = "Modified Date")]
        public DateTime? ModifiedDate { get; set; }
        [Display(Name = "Modified By")]
        public int? ModifiedBy { get; set; }

        public int Status { get; set; }
        public List<CategoriesSelect> LstCategoriesSelect { get; set; }

    }
}