﻿namespace WebMMO.Web.Areas.AppSystems.Models
{
    public class CategoriesSelect
    {
        public int ID { get; set; }
        public string CategoriesName { get; set; }
        public bool IsSelected { get; set; }
    }
}