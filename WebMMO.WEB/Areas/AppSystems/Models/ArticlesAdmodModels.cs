﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebMMO.Web.Areas.AppSystems.Models
{
    public class ArticlesAdmodModels
    {
        public int? Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string BodyContent { get; set; }
        public string CountryCode { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }

    }
}