﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMMO.Common.Helpes;
using WebMMO.Core.DomainModels.Appsetting;
using WebMMO.Core.MessageResponse;
using WebMMO.Web.Areas.AppSystems.Models;
using WebMMO.Web.Controllers;

namespace WebMMO.Web.Areas.AppSystems.Controllers
{
    public class ArticlesAdmodController : BaseController
    {
        // GET: AppSystems/ArticlesAdmod
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult SearchArticlesAdmod(int draw = 1, int start = 0, int length = 10, string Name = "")
        {
            var tableParams = new FilterTableParams(Request, start, length, draw);
            var result = RepositoryService.AppSystemService.SearchArticlesAdmod(tableParams, Name);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddAndEdit(int? guidkey)
        {
            ArticlesAdmodModels model = new ArticlesAdmodModels();
            model.Id = 0;
            if (guidkey != null)
            {
                ArticlesAdmodDto objNew = RepositoryService.AppSystemService.ArticlesAdmodById(guidkey.Value);
                model.Id = objNew.Id;
                model.Title = objNew.Title;
                model.BodyContent = objNew.BodyContent;
                model.CountryCode = objNew.CountryCode;
                model.Status = objNew.Status;
                model.CreatedDate = objNew.CreatedDate;
                model.CreatedBy = objNew.CreatedBy;
            }

            return View(model);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult AddAndEdit(ArticlesAdmodModels model)
        {
            if (ModelState.IsValid)
            {

                if (model.Id == 0)
                {
                    model.CreatedBy = CurrentLoginUser.ID;
                    model.CreatedDate = DateTime.UtcNow;
                    model.Status = ConfigHelpers.StatusPending;
                }

                ArticlesAdmodDto objNew = new ArticlesAdmodDto();
                objNew.Id = model.Id;
                objNew.Title = model.Title;
                objNew.BodyContent = model.BodyContent;
                objNew.CountryCode = model.CountryCode;
                objNew.Status = model.Status;
                objNew.CreatedDate = model.CreatedDate;
                objNew.CreatedBy = model.CreatedBy;
                RepositoryService.AppSystemService.AddAndEditArticlesAdmod(objNew);
                return RedirectToAction("", "ArticlesAdmod", new { Area = "AppSystems" });
            }

            return View(model);
        }
    }
}