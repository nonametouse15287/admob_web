﻿using WebMMO.Common.Helpes;
using WebMMO.Core.DomainModels.AppManage;
using WebMMO.Core.MessageResponse;
using WebMMO.Web.Areas.AppSystems.Models;
using WebMMO.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace WebMMO.Web.Areas.AppSystems.Controllers
{
    public class AppPermissionController : BaseController
    {
        // GET: AppSystems/AppPermission
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult SearchAppSystems(int draw = 1, int start = 0, int length = 10, string Name = "")
        {
            var tableParams = new FilterTableParams(Request, start, length, draw);
            var result = RepositoryService.AppSystemService.SearchAppSystems(tableParams, Name);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddAndEdit(Guid? guidkey)
        {
            AppPermissionModel model = new AppPermissionModel();

            if (guidkey != null)
            {
                AppPermissionDto objNew = RepositoryService.AppSystemService.AppPermissionByGuid(guidkey.Value);
                model.ID = objNew.ID;
                model.Guid = objNew.Guid;
                model.AppName = objNew.AppName;
                model.AppPackage = objNew.AppPackage;
                model.ListPermission = objNew.ListPermission;
                model.CreatedDate = objNew.CreatedDate;
                model.CreatedBy = objNew.CreatedBy;
                model.ModifiedDate = objNew.ModifiedDate;
                model.ModifiedBy = objNew.ModifiedBy;
                model.Status = objNew.Status;
            }

            var lstCategories = RepositoryService.WebMMOService.GetAllCategories();
            model.LstCategoriesSelect = new List<CategoriesSelect>();

            foreach (var item in lstCategories)
            {
                CategoriesSelect objNew = new CategoriesSelect();
                objNew.ID = item.ID;
                objNew.CategoriesName = item.CategoriesName;
                objNew.IsSelected = false;
                model.LstCategoriesSelect.Add(objNew);
            }

            if (!string.IsNullOrEmpty(model.ListPermission))
            {
                string[] arrStrings = model.ListPermission.Split(',');
                if (arrStrings.Length > 0)
                {
                    foreach (var itemid in arrStrings)
                    {
                        foreach (var item in model.LstCategoriesSelect)
                        {
                            if (item.ID.ToString() == itemid.Trim())
                            {
                                item.IsSelected = true;
                            }
                        }
                    }
                }
            }

            return View(model);
        }
        [HttpPost]
        public ActionResult AddAndEdit(AppPermissionModel model)
        {
            if (ModelState.IsValid)
            {
                model.ModifiedBy = CurrentLoginUser.ID;
                model.ModifiedDate = DateTime.UtcNow;
                if (model.ID == 0)
                {
                    model.CreatedBy = CurrentLoginUser.ID;
                    model.CreatedDate = DateTime.UtcNow;
                    model.Status = ConfigHelpers.StatusPending;
                    model.Guid = Guid.NewGuid();
                }

                model.ListPermission = string.Join(",", model.LstCategoriesSelect.Where(u => u.IsSelected).Select(u => u.ID).ToList());

                AppPermissionDto objNew = new AppPermissionDto();
                objNew.ID = model.ID;
                objNew.Guid = model.Guid;
                objNew.AppName = model.AppName;
                objNew.AppPackage = model.AppPackage;
                objNew.ListPermission = model.ListPermission;
                objNew.CreatedDate = model.CreatedDate;
                objNew.CreatedBy = model.CreatedBy;
                objNew.ModifiedDate = model.ModifiedDate;
                objNew.ModifiedBy = model.ModifiedBy;
                objNew.Status = model.Status;
                RepositoryService.AppSystemService.AddAndEdit(objNew);
                return RedirectToAction("", "AppPermission", new { Area = "AppSystems" });
            }

            return View(model);
        }
    }
}