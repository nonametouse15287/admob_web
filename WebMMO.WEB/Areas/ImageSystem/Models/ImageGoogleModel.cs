﻿namespace WebMMO.Web.Areas.WebMMO.Models
{
    public class ImageGoogleModel
    {
        public int ID { get; set; }
        public string imagethumb { get; set; }
        public string imagelink { get; set; }
    }
}