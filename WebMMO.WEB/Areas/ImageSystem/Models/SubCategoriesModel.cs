﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebMMO.Web.Areas.WebMMO.Models
{
    public class SubCategoriesModel
    {
       
        [Display(Name = "ID")]
        public int ID { get; set; }
        [Key]
        [Display(Name = "Guid")]
        public Guid Guid { get; set; }
        [MaxLength(150)]
        [Display(Name = "Sub Categories Name")]
        [Required]
        public string SubCategoriesName { get; set; }
        [MaxLength(250)]
        [Display(Name = "Sub Categories Search")]
        [Required]
        public string SubCategoriesSearch { get; set; }
        [MaxLength(150)]
        [Display(Name = "Sub Categories Elastic")]
        public string SubCategoriesElastic { get; set; }
        [Required]
        [Display(Name = "Categories Id")]
        public int CategoriesId { get; set; }
        [Display(Name = "Created Date")]
        public DateTime? CreatedDate { get; set; }
        [Display(Name = "Created By")]
        public int? CreatedBy { get; set; }
        [Display(Name = "Modified Date")]
        public DateTime? ModifiedDate { get; set; }
        [Display(Name = "Modifed By")]
        public int? ModifedBy { get; set; }
        [Display(Name = "Status")]
        public int? Status { get; set; }
        [Display(Name = "Sync Elastic Status")]
        public int? SyncElasticStatus { get; set; }
    }
}