﻿using System.ComponentModel.DataAnnotations;

namespace WebMMO.Web.Areas.WebMMO.Models
{
    public class ImageListModel
    {
        [Display(Name = "Categories Id")]
        [Required]
        public int? CategoriesId { get; set; }
        [Display(Name = "Sub Categories Id")]
        [Required]
        public int? SubCategoriesId { get; set; }
        [Display(Name = "Key Search")]
        [Required]
        public string KeyElasticSearch { get; set; }
        [Display(Name = "Image Link")]
        [Required]
        public string ImageLink { get; set; }
    }
}