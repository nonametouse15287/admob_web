﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebMMO.Web.Areas.WebMMO.Models
{
    public class CategoriesModel
    {
        [Display(Name = "ID")]
        public int ID { get; set; }
        [Key]
        [Display(Name = "Guid")]
        public Guid Guid { get; set; }
        [Required]
        [MaxLength(150)]
        [Display(Name = "Categories Name")]
        public string CategoriesName { get; set; }
        [Required]
        [MaxLength(250)]
        [Display(Name = "Categories Search")]
        public string CategoriesSearch { get; set; }
        [MaxLength(150)]
        [Display(Name = "Categories Elastic")]
        public string CategoriesElastic { get; set; }
        [Display(Name = "Created Date")]
        public DateTime? CreatedDate { get; set; }
        [Display(Name = "Created By")]
        public int? CreatedBy { get; set; }
        [Display(Name = "Modified Date")]
        public DateTime? ModifiedDate { get; set; }
        [Display(Name = "Modifed By")]
        public int? ModifedBy { get; set; }
        [Display(Name = "Status")]
        public int? Status { get; set; }
        [Display(Name = "Sync Elastic Status")]
        public int? SyncElasticStatus { get; set; }

    }
}