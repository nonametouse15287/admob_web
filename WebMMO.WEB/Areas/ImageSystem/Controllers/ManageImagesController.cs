﻿using WebMMO.Common.Helpes;
using WebMMO.Core.MessageResponse;
using WebMMO.Web.Controllers;
using System;
using System.Web.Mvc;

namespace WebMMO.Web.Areas.WebMMO.Controllers
{
    public class ManageImagesController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.Categories = RepositoryService.WebMMOService.GetAllCategories();
            return View();
        }

        public JsonResult ManageImagesSearch(int draw = 1, int start = 0, int length = 10, string Name = "", string Status = "", int? Categories = 0, int? SubCategories = 0)
        {
            var tableParams = new FilterTableParams(Request, start, length, draw);
            var result = RepositoryService.WebMMOService.SearchImages(tableParams, Name, Status, Categories.Value, SubCategories.Value);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ApproveWebMmoAction(int keyApprove, Guid valueGuid, int valueKeyId)
        {
            var objUpdate = RepositoryService.WebMMOService.GetWebMmoByGuid(valueGuid);

            if (objUpdate != null)
            {
                objUpdate.Status = keyApprove;
                objUpdate.ModifiedBy = CurrentLoginUser.ID;
                objUpdate.ModifiedDate = DateTime.UtcNow;
                objUpdate.SyncElasticStatus = ConfigHelpers.StatusPending;
                RepositoryService.WebMMOService.AddAndEditImageSearch(objUpdate);
            }
            return SuccessToClient("đã xong!");
        }
    }
}