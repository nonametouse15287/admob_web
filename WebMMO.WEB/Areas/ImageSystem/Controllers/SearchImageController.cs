﻿using WebMMO.Common.Helpes;
using WebMMO.Common.Utils;
using WebMMO.Core.DomainModels.WebMMO;
using WebMMO.Web.Areas.WebMMO.Models;
using WebMMO.Web.Controllers;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using WebMMO.Core.DomainModels.ImageSystem;

namespace WebMMO.Web.Areas.WebMMO.Controllers
{
    public class SearchImageController : BaseController
    {
        private static readonly ILog Logger = LogManager.GetLogger(System.Environment.MachineName);
        // GET: WebMMO/SearchImage
        public ActionResult Index()
        {
            List<ImageCategoriesDto> lstObj = new List<ImageCategoriesDto>();
            lstObj = RepositoryService.WebMMOService.GetAllCategories();
            ViewBag.Categories = lstObj;
            return View();
        }

        //public JsonResult SearchImage(int draw = 1, int start = 0, int length = 10, string Name = "")
        public JsonResult SearchImage(string Name = "", int Categoriesid = 0, int SubCategoriesid = 0, string NameKeyWebsite = "")
        {
            /*Get top 1 API key*/

            SearchLogDto objSearch = new SearchLogDto
            {
                KeySearch = Name,
                CategoriesId = Categoriesid,
                SubCategoriesId = SubCategoriesid,
                UserId = CurrentLoginUser.ID,
                CreatedDate = DateTime.UtcNow
            };
            RepositoryService.WebMMOService.AddAndEditSearchLog(objSearch);

            ApiKeyDto objKey = RepositoryService.WebMMOService.GetTop1Key();
            string cx = "001075864114036468803:7ryi1aet3fi";
            string apiKey = "AIzaSyBmONHmFMa0EnEfQu5xdsU3r961k3F8KGA";

            if (objKey != null)
            {
                apiKey = objKey.ApiKey.Trim();
            }

            /*AIzaSyBmONHmFMa0EnEfQu5xdsU3r961k3F8KGA*/
            /*AIzaSyDEfu7mOmbdld8LOPnYD0_T3CN-7XG8mZk*/

            string urlQuery = $@"https://www.googleapis.com/customsearch/v1?key={apiKey}&cx={cx}&q={Name}&hl=en&as_st=y&tbm=isch&tbs=isz:l&searchType=image&disableWebSearch=true&start=";
            List<ImageGoogleModel> lstImage = new List<ImageGoogleModel>();

            if (!string.IsNullOrEmpty(Name))
            {
                for (int i = 1; i <= 10; i++)
                {
                    Thread.Sleep(200);
                    try
                    {
                        var url = urlQuery + (i * 10 - 9).ToString();
                        var request = WebRequest.Create(url);
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        Stream dataStream = response.GetResponseStream();
                        StreamReader reader = new StreamReader(dataStream);
                        string responseString = reader.ReadToEnd();
                        dynamic jsonData = JsonConvert.DeserializeObject(responseString);

                        foreach (var item in jsonData.items)
                        {
                            try
                            {
                                string imagelink = item.link;
                                if (imagelink.HasImageExtension())
                                {
                                    ImageGoogleModel obj = new ImageGoogleModel();
                                    obj.imagelink = imagelink;
                                    obj.ID = lstImage.Count + 1;
                                    lstImage.Add(obj);
                                }
                            }
                            catch (System.Exception ex)
                            {

                            }
                        }
                        RepositoryService.WebMMOService.UpdateCountApiKey(objKey.ID);
                    }
                    catch (System.Exception error)
                    {                        
                        /*Key da het dung luong su dung trong
                         Can code them phan thay doi key */
                        RepositoryService.WebMMOService.UpdateCountApiKey(objKey.ID);
                        objKey = RepositoryService.WebMMOService.GetTop1Key();
                        if (objKey != null)
                        {
                            apiKey = objKey.ApiKey.Trim();
                        }
                        //Logger.Error(ex.Message, ex);
                        Logger.Error("SearchImage_Error" + error.Message + "\n InnerException:" + (error.InnerException != null ? error.InnerException.Message : "") + "\n StackTrace:" + error.StackTrace, error);
                    }
                }
            }


            // var tuple = new Tuple<int, List<ImageSubCategoriesResult>>(total, list);
            //var result = new DataTableViewModel<ImageGoogleModel>(tableParams.draw, lstImage.Count, lstImage.Count, lstImage);

            /*Insert vao db*/
            if (lstImage.Count > 0)
            {
                Guid guidkeySearch = Guid.NewGuid();
                foreach (var item in lstImage)
                {
                    ImageListDto objInsert = new ImageListDto();
                    objInsert.Guid = Guid.NewGuid();
                    objInsert.GuidSearch = guidkeySearch;
                    objInsert.CategoriesId = Categoriesid;
                    objInsert.SubCategoriesId = SubCategoriesid;
                    objInsert.KeyElasticSearch = Name;
                    objInsert.ImageThumb = item.imagethumb;
                    objInsert.ImageLink = item.imagelink;
                    objInsert.CreatedDate = DateTime.UtcNow;
                    objInsert.CreatedBy = CurrentLoginUser.ID;
                    objInsert.ModifiedDate = DateTime.UtcNow;
                    objInsert.ModifiedBy = CurrentLoginUser.ID;
                    objInsert.Status = ConfigHelpers.StatusPending;
                    objInsert.SyncElasticStatus = ConfigHelpers.StatusPending;
                    if (!string.IsNullOrEmpty(NameKeyWebsite))
                    {
                        objInsert.KeyElasticSearch = NameKeyWebsite;
                    }
                    int result = RepositoryService.WebMMOService.AddAndEditImageSearch(objInsert);
                }
            }


            return SuccessToClient($"Đã có {lstImage.Count} được tìm thấy và lưu vào DB");
        }

        public JsonResult GetSubCategoriesByGroup(int CategoriesId)
        {
            List<ImageSubCategoriesDto> lstState = RepositoryService.WebMMOService.GetSubCategoriesByGroup(CategoriesId);

            var jsSerial = new JavaScriptSerializer();

            string result = jsSerial.Serialize(lstState);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddAndEdit()
        {
            ImageListModel model = new ImageListModel();

            List<ImageCategoriesDto> lstObj = new List<ImageCategoriesDto>();
            lstObj = RepositoryService.WebMMOService.GetAllCategories();
            ViewBag.Categories = new SelectList(lstObj, "ID", "CategoriesName");

            return View(model);
        }
        [HttpPost]
        public ActionResult AddAndEdit(ImageListModel model)
        {
            if (ModelState.IsValid)
            {
                ImageListDto objNew = new ImageListDto();
                objNew.Guid = Guid.NewGuid();
                objNew.GuidSearch = Guid.NewGuid();
                objNew.CategoriesId = model.CategoriesId;
                objNew.SubCategoriesId = model.SubCategoriesId;
                objNew.KeyElasticSearch = model.KeyElasticSearch;
                objNew.CreatedDate = DateTime.UtcNow;
                objNew.CreatedBy = CurrentLoginUser.ID;
                objNew.ModifiedDate = DateTime.UtcNow;
                objNew.ModifiedBy = CurrentLoginUser.ID;
                objNew.ImageLink = model.ImageLink;
                objNew.Status = ConfigHelpers.StatusPending;
                objNew.SyncElasticStatus = ConfigHelpers.StatusPending;
                RepositoryService.WebMMOService.AddAndEditImageSearch(objNew);

                return RedirectToAction("", "ManageImages", new { Area = "WebMMO" });
            }

            List<ImageCategoriesDto> lstObj = new List<ImageCategoriesDto>();
            lstObj = RepositoryService.WebMMOService.GetAllCategories();
            ViewBag.Categories = new SelectList(lstObj, "ID", "CategoriesName");

            return View(model);
        }
    }
}