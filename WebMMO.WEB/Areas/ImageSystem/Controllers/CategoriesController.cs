﻿using WebMMO.Common.Helpes;
using WebMMO.Common.Utils;
using WebMMO.Core.DomainModels.WebMMO;
using WebMMO.Core.MessageResponse;
using WebMMO.Web.Areas.WebMMO.Models;
using WebMMO.Web.Controllers;
using System;
using System.Web.Mvc;

namespace WebMMO.Web.Areas.WebMMO.Controllers
{
    public class CategoriesController : BaseController
    {
        // GET: WebMMO/Categories
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult SearchCategories(int draw = 1, int start = 0, int length = 10, string Name = "")
        {
            var tableParams = new FilterTableParams(Request, start, length, draw);
            var result = RepositoryService.WebMMOService.SearchCategories(tableParams, Name);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddAndEdit(Guid? guidkey)
        {
            CategoriesModel model = new CategoriesModel();

            if (guidkey != null)
            {
                ImageCategoriesDto objNew = RepositoryService.WebMMOService.GetCategoriesByGuid(guidkey.Value);
                model.ID = objNew.ID;
                model.Guid = objNew.Guid;
                model.CategoriesName = objNew.CategoriesName;
                model.CategoriesSearch = objNew.CategoriesSearch;
                model.CreatedDate = objNew.CreatedDate;
                model.CreatedBy = objNew.CreatedBy;
                model.ModifiedDate = objNew.ModifiedDate;
                model.ModifedBy = objNew.ModifedBy;
                model.Status = objNew.Status;
            }

            return View(model);
        }
        [HttpPost]
        public ActionResult AddAndEdit(CategoriesModel model)
        {
            if (ModelState.IsValid)
            {
                model.ModifedBy = CurrentLoginUser.ID;
                model.ModifiedDate = DateTime.UtcNow;
                if (model.ID == 0)
                {
                    model.CreatedBy = CurrentLoginUser.ID;
                    model.CreatedDate = DateTime.UtcNow;
                    model.Status = ConfigHelpers.StatusPending;
                    model.Guid = Guid.NewGuid();
                }

                ImageCategoriesDto objNew = new ImageCategoriesDto();
                objNew.ID = model.ID;
                objNew.Guid = model.Guid;
                objNew.CategoriesName = model.CategoriesName;
                objNew.CategoriesSearch = model.CategoriesSearch;
                objNew.CategoriesElastic = UtilsFuntion.chuan_xau(UtilsFuntion.Bo_Dau(model.CategoriesName)).Replace(" ", "");
                objNew.CreatedDate = model.CreatedDate;
                objNew.CreatedBy = model.CreatedBy;
                objNew.ModifiedDate = model.ModifiedDate;
                objNew.ModifedBy = model.ModifedBy;
                objNew.Status = model.Status;
                objNew.SyncElasticStatus = ConfigHelpers.StatusPending;
                RepositoryService.WebMMOService.AddAndEdit(objNew);
                return RedirectToAction("", "Categories", new { Area = "WebMMO" });
            }

            return View(model);
        }
    }
}