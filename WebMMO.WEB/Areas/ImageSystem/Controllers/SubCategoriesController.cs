﻿
using WebMMO.Common.Helpes;
using WebMMO.Common.Utils;
using WebMMO.Core.DomainModels.WebMMO;
using WebMMO.Core.MessageResponse;
using WebMMO.Web.Areas.WebMMO.Models;
using WebMMO.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using WebMMO.Core.DomainModels.ImageSystem;

namespace WebMMO.Web.Areas.WebMMO.Controllers
{
    public class SubCategoriesController : BaseController
    {
        // GET: WebMMO/SubCategories
        public ActionResult Index()
        {
            ViewBag.Categories = RepositoryService.WebMMOService.GetAllCategories();
            return View();
        }

        public JsonResult SearchSubCategories(int draw = 1, int start = 0, int length = 10, string Name = "", string Status = "", int? Categories = 0)
        {
            var tableParams = new FilterTableParams(Request, start, length, draw);
            var result = RepositoryService.WebMMOService.SearchSubCategories(tableParams, Name, Status, Categories.Value);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddAndEdit(Guid? guidkey)
        {
            SubCategoriesModel model = new SubCategoriesModel();

            List<ImageCategoriesDto> lstObj = new List<ImageCategoriesDto>();
            lstObj = RepositoryService.WebMMOService.GetAllCategories();
            ViewBag.Categories = new SelectList(lstObj, "ID", "CategoriesName");

            if (guidkey != null)
            {
                ImageSubCategoriesDto objNew = RepositoryService.WebMMOService.GetSubCategoriesByGuid(guidkey.Value);
                model.ID = objNew.ID;
                model.Guid = objNew.Guid;
                model.SubCategoriesName = objNew.SubCategoriesName;
                model.SubCategoriesSearch = objNew.SubCategoriesSearch;
                model.CreatedDate = objNew.CreatedDate;
                model.CreatedBy = objNew.CreatedBy;
                model.ModifiedDate = objNew.ModifiedDate;
                model.ModifedBy = objNew.ModifedBy;
                model.Status = objNew.Status;
                model.CategoriesId = objNew.CategoriesId.Value;
            }

            return View(model);
        }
        [HttpPost]
        public ActionResult AddAndEdit(SubCategoriesModel model)
        {
            if (ModelState.IsValid)
            {
                model.ModifedBy = CurrentLoginUser.ID;
                model.ModifiedDate = DateTime.UtcNow;
                if (model.ID == 0)
                {
                    model.CreatedBy = CurrentLoginUser.ID;
                    model.CreatedDate = DateTime.UtcNow;
                    model.Status = ConfigHelpers.StatusPending;
                    model.Guid = Guid.NewGuid();
                }

                ImageSubCategoriesDto objNew = new ImageSubCategoriesDto();
                objNew.ID = model.ID;
                objNew.Guid = model.Guid;
                objNew.SubCategoriesName = model.SubCategoriesName;
                objNew.SubCategoriesSearch = model.SubCategoriesSearch;
                objNew.SubCategoriesElastic = UtilsFuntion.chuan_xau(UtilsFuntion.Bo_Dau(model.SubCategoriesName)).Replace(" ", "");
                objNew.CreatedDate = model.CreatedDate;
                objNew.CreatedBy = model.CreatedBy;
                objNew.ModifiedDate = model.ModifiedDate;
                objNew.ModifedBy = model.ModifedBy;
                objNew.Status = model.Status;
                objNew.SyncElasticStatus = ConfigHelpers.StatusPending;
                objNew.CategoriesId = model.CategoriesId;
                RepositoryService.WebMMOService.AddAndEditSubCategories(objNew);
                return RedirectToAction("", "SubCategories", new { Area = "WebMMO" });
            }

            List<ImageCategoriesDto> lstObj = new List<ImageCategoriesDto>();
            lstObj = RepositoryService.WebMMOService.GetAllCategories();
            ViewBag.Categories = new SelectList(lstObj, "ID", "CategoriesName");

            return View(model);
        }
    }
}