﻿using System.Web.Mvc;

namespace WebMMO.Web.Areas.WebMMO
{
    public class WebMMOAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "WebMMO";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "WebMMO_default",
                "WebMMO/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}