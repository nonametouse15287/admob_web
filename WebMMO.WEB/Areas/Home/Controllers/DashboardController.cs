﻿using WebMMO.Web.Controllers;
using System.Web.Mvc;

namespace WebMMO.Web.Areas.Home.Controllers
{
    public class DashboardController : BaseController
    {
        // GET: Home/Dashboard
        public ActionResult Index()
        {
            return View();
        }
    }
}