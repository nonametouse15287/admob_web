﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMMO.Web.Areas.Home.Models;
using WebMMO.Common.Utils;
using WebMMO.Core.DomainModels.User;
using WebMMO.Service;

namespace WebMMO.Web.Areas.Home.Controllers
{
    public class AccountController : Controller
    {
        protected ServicesManager RepositoryService => ServicesManager.RepositoryService;
        public ActionResult Error()
        {
            return View();
        }

        // GET: Home/Account
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel loginForm)
        {
          
            if (ModelState.IsValid)
            {
                string[] roles = Authenticate(loginForm.EmailAddress, loginForm.Password);
                if (roles != null)
                {
                    string status = CheckStatus(loginForm.EmailAddress);

                    if (status == "Disable")
                    {
                        ModelState.AddModelError("Password", "Account is disabled.");
                    }
                    else
                    {
                        ProcessLogin(loginForm.EmailAddress, roles);

                        //Update Last Login Date
                        RepositoryService.UserService.UpdateLastLoginDate(loginForm.EmailAddress);

                        return RedirectToAction("Index", "Dashboard", new { area = "Home" });
                    }
                }
                else
                {
                    ModelState.AddModelError("Password", "Invalid email address or password.");
                }

            }

            return View();
        }

        private string CheckStatus(string email)
        {
            string result = null;

            var user = RepositoryService.UserService.GetUserByEmail(email).ResponseObject;

            result = user.IsActive == true ? "Enable" : "Disable";

            return result;
        }

        private string[] Authenticate(string email, string password)
        {
            string[] result = null;

            // Attempt to retrieve the user based on the specified username (email address) and password.

            UserDto user = RepositoryService.UserService.GetUserByEmail(email).ResponseObject;
            if (user != null && PasswordUtils.VerifyPassword(email, password, user.Password))
            {
                result = GetAllRoleTypes(user);
            }

            return result;
        }

        private string[] GetAllRoleTypes(UserDto user)
        {
            var lstRole = RepositoryService.RoleService.GetRoleByUserId(user.ID).ResponseObject;
            List<string> result;
            result = lstRole.Split(',').ToList();
            return result.ToArray();
        }
        private void ProcessLogin(string email, string[] roles)
        {
            // Create the authentication ticket and store the roles.
            FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(2, email, DateTime.Now, DateTime.Now.AddMinutes(30), false, string.Join("|", roles));

            // Encrypt the ticket and add it to the cookie.
            string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            Response.Cookies.Add(cookie);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Login", "Account", new { area = "Home" });
        }


    }
}