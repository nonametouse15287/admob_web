﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebMMO.Core.DomainModels.Menu;
using WebMMO.Web.Controllers;

namespace WebMMO.Web.Areas.Home.Controllers
{
    public class MenuController : BaseController
    {
        // GET: Home/Menu
        public ActionResult Index()
        {
            return View();
        }

      
        public ActionResult MenuList()
        {

            List<MenuDto> menus = RepositoryService.MenuService.GetMenu_HIAS_GET_MENUACCESS(CurrentLoginUser.ID, 0).ResponseObject.OrderBy(o => o.Seq).ToList();

            if (CurrentLoginUser.OrganizationID != 1 && CurrentLoginUser.OrganizationID != null)
            {
                ViewBag.OrgName =
                    RepositoryService.OrganizationService.GetOrganizationById(CurrentLoginUser.OrganizationID.Value).ResponseObject.Name;
            }
            else
            {
                ViewBag.OrgName = "";
            }

            return PartialView(menus);
        }

        public ActionResult SubMenu(int parentMenuId)
        {
            //List<Menu> menus = db.Database.SqlQuery<Menu>("dbo.HIAS_GET_MENUACCESS @intUserID = {0}, @intMenuParentID = {1}", CurrentLoginUser.ID, ParentMenuID).OrderBy(o => o.Seq).ToList();
            List<MenuDto> menus = RepositoryService.MenuService.GetMenu_HIAS_GET_MENUACCESS(CurrentLoginUser.ID, parentMenuId).ResponseObject.OrderBy(o => o.Seq).ToList();
            return PartialView(menus);
        }

    }
}