﻿using System.ComponentModel.DataAnnotations;

namespace WebMMO.Web.Areas.Home.Models
{
    public class LoginModel
    {
        /// <summary>
        /// Gets or sets the email address of the <see cref="LoginForm"/>
        /// </summary>    
        [Required(ErrorMessage = "Please specify the email address to login.")]
        public string EmailAddress { get; set; }

        /// <summary>
        /// Gets or sets the password of the <see cref="LoginForm"/>
        /// </summary>    
        [Required(ErrorMessage = "Please specify the password to login.")]
        public string Password { get; set; }
    }
}