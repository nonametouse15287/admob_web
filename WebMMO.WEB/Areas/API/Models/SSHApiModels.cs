﻿using WebMMO.Core.DomainModels.QueryApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMMO.Web.Areas.API.Models
{
    public class SSHApiRequestModels : QueryInfo
    {
        public string CountryCode { get; set; }
    }

    public class GmailProfilesApiRequestModels : QueryInfo
    {
        public string CountryCode { get; set; }
    }
    public class GmailUpdateApiRequestModels : QueryInfo
    {
        public int Id { get; set; }
        public int? statusprofile { get; set; }
        public int? statusdelete { get; set; }
        public string description { get; set; }
    }
}