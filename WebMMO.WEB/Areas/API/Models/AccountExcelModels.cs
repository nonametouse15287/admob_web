﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMMO.Web.Areas.API.Models
{
    public class AccountExcelModels
    {
        public string NexonEmail { set; get; }
        public string NexonPasswords { get; set; }
        public string NexonBirthday { get; set; }
        public string YahooMail { get; set; }
        public string YahooPasswords { get; set; }
        public string Combine { get; set; }
        public string TypeEmail { get; set; }
    }
}