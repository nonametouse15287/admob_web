﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMMO.Core.DomainModels.QueryApi;

namespace WebMMO.Web.Areas.API.Models
{
    public class GmailRequest : QueryInfo
    {
        public string token { get; set; }
        public int amount { get; set; }
        public string type { get; set; }
    }

    public class GmailPushRequest : QueryInfo
    {
        public string token { get; set; }
        public string u { get; set; }
        public string p { get; set; }
        public string c { get; set; }
        public string r { get; set; }
    }
}