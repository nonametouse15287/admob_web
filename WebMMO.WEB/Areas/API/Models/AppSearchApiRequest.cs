﻿using WebMMO.Core.DomainModels.QueryApi;
using System;

namespace WebMMO.Web.Areas.API.Models
{
    public class AppSearchApiRequest : QueryInfo
    {
        public string SearchKey { get; set; }
        public int CategoriesId { get; set; }
        public int SubCategoriesId { get; set; }
        public string AppPackage { get; set; }
    }
    public class AppSearchCategoriesApiRequest
    {
        public string AppPackage { get; set; }
    }

    public class AppSearchImageApiRequest
    {
        public Guid Guid { get; set; }
    }
}