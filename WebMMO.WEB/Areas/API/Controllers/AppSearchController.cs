﻿using WebMMO.Common.Converters;
using WebMMO.Core.DomainModels.WebMMO;
using WebMMO.Core.MessageResponse;
using WebMMO.Service;
using WebMMO.Service.Helpes;
using WebMMO.Web.Areas.API.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.Mvc;

namespace WebMMO.Web.Areas.API.Controllers
{
    public class AppSearchController : Controller
    {
        protected ServicesManager RepositoryService => ServicesManager.RepositoryService;
        // GET: API/AppSearch
       
        public JsonResult GetCategories(AppSearchCategoriesApiRequest apirequest)
        {
            var result = RepositoryService.WebMMOService.GetAppCategories(apirequest.AppPackage);
            var response = new ResultDetail(ErrorConstants.Success, "Success", "v1", 0, result);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSubCategories(AppSearchApiRequest apirequest)
        {
            var result = RepositoryService.WebMMOService.GetAppSubCategories(apirequest.AppPackage, apirequest.CategoriesId);
            var response = new ResultDetail(ErrorConstants.Success, "Success", "v1", 0, result);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
              
        public JsonResult GetImagesElasticSearch(AppSearchApiRequest apirequest)
        {
            var strquery = ElasticSearchHelper.BuildQuerySearch(apirequest.PageNumber, apirequest.PageSize, apirequest.SearchKey, apirequest.CategoriesId.ToString(), apirequest.SubCategoriesId.ToString());
            var objSearch = ElasticSearchHelper.SearhByQuery(strquery, "Images", "SubImages");            
            List<ImageListDto> lstPlaceRes = new List<ImageListDto>();
            int total = 0;
            if (objSearch?.hits?.hits != null)
            {
                total = objSearch.hits.total;

                foreach (var item in objSearch.hits.hits)
                {
                    ImageListDto objInsert = new ImageListDto();

                    objInsert.ID = item._source.ID;
                    objInsert.Guid = item._source.Guid;
                    objInsert.GuidSearch = item._source.GuidSearch;
                    objInsert.CategoriesId = item._source.CategoriesId;
                    objInsert.SubCategoriesId = item._source.SubCategoriesId;
                    objInsert.KeyElasticSearch = item._source.KeyElasticSearch;
                    objInsert.ImageThumb = item._source.ImageThumb;
                    objInsert.ImageLink = item._source.ImageLink;
                    objInsert.CreatedDate = item._source.CreatedDate;
                    objInsert.CreatedBy = item._source.CreatedBy;
                    objInsert.ModifiedDate = item._source.ModifiedDate;
                    objInsert.ModifiedBy = item._source.ModifiedBy;
                    objInsert.Status = item._source.Status;
                    lstPlaceRes.Add(objInsert);
                }
            }

            var response = new ResultDetail(ErrorConstants.Success, "Success", "v1", total, lstPlaceRes);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetImageByGuid(AppSearchImageApiRequest apirequest)
        {
            var result = RepositoryService.WebMMOService.GetWebMmoByGuid(apirequest.Guid);
            var response = new ResultDetail(ErrorConstants.Success, "Success", "v1", 0, result);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}