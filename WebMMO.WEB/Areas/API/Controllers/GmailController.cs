﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using WebMMO.Common.Converters;
using WebMMO.Core.DomainModels.Gmail;
using WebMMO.Core.DomainModels.Sms;
using WebMMO.Core.MessageResponse;
using WebMMO.Service;
using WebMMO.Web.Areas.API.Models;

namespace WebMMO.Web.Areas.API.Controllers
{
    public class GmailController : Controller
    {
        protected ServicesManager RepositoryService => ServicesManager.RepositoryService;

        public string GmailPush(GmailPushRequest apirequest)
        {
            string result = "ERROR_API_KEY";
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            var chkCheckApi = CheckToken(apirequest.token);
            if (true)
            {
                GmailInfoDto objGmailPush = new GmailInfoDto();
                objGmailPush.username = apirequest.u;
                objGmailPush.password = apirequest.p;
                objGmailPush.emailrecover = apirequest.r;
                objGmailPush.cookies = apirequest.c;
                objGmailPush.emailType = "1h";
                objGmailPush.token = apirequest.token;
                objGmailPush.createddate = DateTime.Now;
                objGmailPush.status = 0;
                RepositoryService.WebMMOService.GmailPush(objGmailPush);
                result = "SUCCESS";
            }
            return result;
        }

        public string GmailGet(GmailRequest apirequest)
        {
            string result = "ERROR_API_KEY";
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            var chkCheckApi = CheckToken(apirequest.token);
            if (chkCheckApi != null)
            {
                GmailPriceDto objGmailPrice = RepositoryService.WebMMOService.GmailPriceGetByType(apirequest.type);
                if (chkCheckApi.BalanceMoney < 1000 ||
                    chkCheckApi.BalanceMoney < (apirequest.amount * objGmailPrice.gmailprice))
                {
                    result = "NOT_ENOUGH_MONEY";
                }
                else
                {
                    result = "";
                    List<GmailInfoDto> lstGmail = new List<GmailInfoDto>();
                    lstGmail = RepositoryService.WebMMOService.GmailGet(apirequest.token, apirequest.type, apirequest.amount);
                    foreach (var item in lstGmail)
                    {
                        result = result + $"{item.username}|{item.password}|none{Environment.NewLine}";
                    }
                }

            }
            return result;
        }

        public string GmailGetByKey(GmailRequest apirequest)
        {
            string result = "ERROR_API_KEY";
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            var chkCheckApi = CheckToken(apirequest.token);
            if (chkCheckApi != null)
            {
                result = "";
                List<GmailInfoDto> lstGmail = new List<GmailInfoDto>();
                lstGmail = RepositoryService.WebMMOService.GmailGetByKey(apirequest.token, apirequest.type, apirequest.amount);
                foreach (var item in lstGmail)
                {
                    result = result + $"{item.username}|{item.password}|none{Environment.NewLine}";
                }
            }
            return result;
        }

        private AccessTokenUserDto CheckToken(string token)
        {
            var tokenCheck = RepositoryService.WebMMOService.CheckToken(token);
            return tokenCheck;
        }
    }
}