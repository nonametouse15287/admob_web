﻿using WebMMO.Common.Converters;
using WebMMO.Core.DomainModels.WebMMO;
using WebMMO.Core.MessageResponse;
using WebMMO.Service;
using WebMMO.Service.Helpers.ElasticSearch;
using WebMMO.Service.Helpes;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.Mvc;

namespace WebMMO.Web.Areas.API.Controllers
{
    //[Route("api/[controller]")]
    public class CategoriesController : Controller
    {
        protected ServicesManager RepositoryService => ServicesManager.RepositoryService;
        // GET: API/Categories
        //[HttpPost, Route("GetCategories")]
        [System.Web.Http.HttpPost]
        public JsonResult GetCategories()
        {
            var result = RepositoryService.WebMMOService.GetAllCategories();
            var response = new ResultDetail(ErrorConstants.Success, "Success", "v1", result.Count, result);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.HttpPost]
        public JsonResult GetSubCategories(CategoriesApiRequest apirequest)
        {
            var result = RepositoryService.WebMMOService.GetSubCategoriesByGroup(apirequest.CategoriesId);
            var response = new ResultDetail(ErrorConstants.Success, "Success", "v1", result.Count, result);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.HttpPost]
        public JsonResult GetImages(CategoriesApiRequest apirequest)
        {
            var result = RepositoryService.WebMMOService.GetSubCategoriesByGroup(apirequest.CategoriesId);
            var response = new ResultDetail(ErrorConstants.Success, "Success", "v1", result.Count, result);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.HttpPost]
        public JsonResult GetImagesElasticSearch(CategoriesApiRequest apirequest)
        {
            SeacrhByQueryPaging queryMain = new SeacrhByQueryPaging();
            int pageNum = (apirequest.PageNumber - 1) < 0 ? 0 : (apirequest.PageNumber - 1);
            queryMain.from = pageNum * apirequest.PageSize;
            queryMain.size = apirequest.PageSize;
            queryMain.query = new QueryPaging();

            if (!string.IsNullOrEmpty(apirequest.SearchKey))
            {
                var queryobj = new QueryPaging();
                queryMain.query._bool = new BoolByQueryPaging();
                queryMain.query._bool.must = new List<object>();

                MustByQueryPaging queryMatch = new MustByQueryPaging();
                queryMatch.match = new MatchByQueryPaging();
                queryMatch.match.KeyElasticSearch = new NameByQueryPaging();
                queryMatch.match.KeyElasticSearch.query = apirequest.SearchKey;
                queryMatch.match.KeyElasticSearch._operator = "and";
                queryMain.query._bool.must.Add(queryMatch);
            }

            string strJsonQuery = JsonConvert.SerializeObject(queryMain);

            var result = ElasticSearchHelper.SearhByQuery(strJsonQuery, "Images", "SubImages");
            //var response = new ResultDetail(ErrorConstants.Success, "Success", "v1", result.Count, result);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}