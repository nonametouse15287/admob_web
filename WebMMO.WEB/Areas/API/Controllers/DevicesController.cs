﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMMO.Common.Converters;
using WebMMO.Common.Utils;
using WebMMO.Core.DomainModels.devices;
using WebMMO.Core.MessageResponse;
using WebMMO.Service;
using WebMMO.Web.Code;

namespace WebMMO.Web.Areas.API.Controllers
{
    public class DevicesController : Controller
    {
        protected ServicesManager RepositoryService => ServicesManager.RepositoryService;

        // GET: API/Devices
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult DevicesPush(string deviceinfo)
        {
            RepositoryService.WebMMOService.DevicesPush(deviceinfo);
            var response = new ResultDetail(ErrorConstants.Success, "Success", "v1", 0, "");
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DevicesGet(string CountryCode)
        {
            DevicesInfoDto objResult = RepositoryService.WebMMOService.DevicesGet();
            AndroidDevicesResult objInfoResult = new AndroidDevicesResult();
            AndroidDevice objDeviceResult = new AndroidDevice();

            if (objResult != null)
            {
                DeviceTemp objDevice = new DeviceTemp();
                string jsonDevices = PasswordUtils.DecodeFrom64(objResult.DevicesInfo);
                objDevice = JsonConvert.DeserializeObject<DeviceTemp>(jsonDevices);
                objDeviceResult = DeviceHelper.BuildRandomDevice(objDevice, CountryCode);
                //Todo: function auto get old fake devices

                #region insert devices fake to db
                /*insert devices fake to db*/
                DevicesFakeInfoDto objFakeInfo = new DevicesFakeInfoDto();

                objFakeInfo.androidID = objDeviceResult.AndroidID;
                objFakeInfo.androidSerial = objDeviceResult.AndroidSerial;
                objFakeInfo.base_OS = objDeviceResult.Base_OS;
                objFakeInfo.board = objDeviceResult.Board;
                objFakeInfo.bootloader = objDeviceResult.Bootloader;
                objFakeInfo.brand = objDeviceResult.Brand;
                objFakeInfo.buildID = objDeviceResult.BuildID;
                objFakeInfo.buildTime = objDeviceResult.BuildTime;
                objFakeInfo.codeName = objDeviceResult.CodeName;
                objFakeInfo.countryISO = objDeviceResult.CountryISO;
                objFakeInfo.display = objDeviceResult.Display;
                objFakeInfo.fingerPrint = objDeviceResult.FingerPrint;
                objFakeInfo.gsFID = objDeviceResult.GsFID;
                objFakeInfo.hardWare = objDeviceResult.HardWare;
                objFakeInfo.host = objDeviceResult.Host;
                objFakeInfo.imei = objDeviceResult.Imei;
                objFakeInfo.incremental = objDeviceResult.Incremental;
                objFakeInfo.kernelVersion = objDeviceResult.KernelVersion;
                objFakeInfo.languageCode = objDeviceResult.LanguageCode;
                objFakeInfo.manufacture = objDeviceResult.Manufacture;
                objFakeInfo.model = objDeviceResult.Model;
                objFakeInfo.previewSDK = objDeviceResult.PreviewSDK;
                objFakeInfo.previewSDKInt = objDeviceResult.PreviewSDKInt;
                objFakeInfo.product = objDeviceResult.Product;
                objFakeInfo.radio = objDeviceResult.Radio;
                objFakeInfo.release = objDeviceResult.Release;
                objFakeInfo.resolution = objDeviceResult.Resolution;
                objFakeInfo.sdkInt = objDeviceResult.SdkInt;
                objFakeInfo.serial = objDeviceResult.Serial;
                objFakeInfo.tags = objDeviceResult.Tags;
                objFakeInfo.time = objDeviceResult.Time.ToString();
                objFakeInfo.type = objDeviceResult.Type;
                objFakeInfo.user = objDeviceResult.User;
                objFakeInfo.userAgent = objDeviceResult.UserAgent;
                //objFakeInfo.desc = objDeviceResult.Description;
                //objFakeInfo.dev = objDeviceResult.Device;
                //objFakeInfo.ID = objDeviceResult.Id;
                //objFakeInfo.InstallTime = objDeviceResult.InstallTime;
                objFakeInfo.CreatedDate = DateTime.UtcNow;
                objFakeInfo.securityPatch = DateTime.UtcNow;
                objFakeInfo.Status = 0;
                RepositoryService.WebMMOService.DevicesFakeInsert(objFakeInfo);
                #endregion

                bool clickGet = GetStatusClick.GetClick(CountryCode);
                objInfoResult.DeviceInfo = objDeviceResult;
                objInfoResult.click = clickGet ? 1 : 0;
            }

            var response = new ResultDetail(ErrorConstants.Success, "Success", "v1", 0, objInfoResult);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetKeyNews()
        {
            string objInfoResult = "";

            List<string> objResult = RepositoryService.WebMMOService.GetKeyNews();
            Random r = new Random();
            objInfoResult = objResult[r.Next(0, objResult.Count - 1)];

            return Json(objInfoResult, JsonRequestBehavior.AllowGet);
        }
    }
}