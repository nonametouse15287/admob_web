﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMMO.Common.Converters;
using WebMMO.Core.DomainModels.Sms;
using WebMMO.Core.DomainModels.WebMMO;
using WebMMO.Core.MessageResponse;
using WebMMO.Service;

namespace WebMMO.Web.Areas.API.Controllers
{
    public class SmsActivateRuController : Controller
    {
        protected ServicesManager RepositoryService => ServicesManager.RepositoryService;

        [System.Web.Http.HttpPost]
        public JsonResult balance(CreateApiRequest apirequest)
        {
            var response = new ResultDetail(ErrorConstants.Success, "ERROR_API_KEY", "v1", 0, "");
            var chkCheckApi = CheckToken(apirequest.Key);
            if (chkCheckApi == null) return Json(response, JsonRequestBehavior.AllowGet);
            response = new ResultDetail(ErrorConstants.Success, "Success", "v1", 0, chkCheckApi.BalanceMoney);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Http.HttpPost]
        public JsonResult create(CreateApiRequest apirequest)
        {
            var response = new ResultDetail(ErrorConstants.Success, "ERROR_API_KEY", "v1", 0, "");
            var chkCheckApi = CheckToken(apirequest.Key);
            /*Check API access*/

            /*Call request*/
            if (chkCheckApi == null) return Json(response, JsonRequestBehavior.AllowGet);
            if (chkCheckApi.BalanceMoney < 3000)
            {
                response = new ResultDetail(ErrorConstants.Success, "ERROR_NOT_ENOUGH_MONEY", "v1", 0, "");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            var result = RepositoryService.WebMMOService.GetNumberActivateRu("0", "zz");
            if (!string.IsNullOrEmpty(result.tzid))
            {
                //response = new ResultDetail(ErrorConstants.Success, "Success", "v1", 0, "ERROR_API_KEY");
                /*Tru tien*/
                chkCheckApi.BalanceMoney = chkCheckApi.BalanceMoney - 3000;
                chkCheckApi.BalanceUsed = chkCheckApi.BalanceUsed + 3000;

                RepositoryService.WebMMOService.UpdateBalanceApiKey(chkCheckApi);
            }
            response = new ResultDetail(ErrorConstants.Success, "Success", "v1", 0, result);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
           
        public AccessTokenUserDto CheckToken(string token)
        {
            var tokenCheck = RepositoryService.WebMMOService.CheckToken(token);
            return tokenCheck;
        }
    }
}