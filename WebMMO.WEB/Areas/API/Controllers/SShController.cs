﻿using WebMMO.Common.Converters;
using WebMMO.Core.MessageResponse;
using WebMMO.Service;
using WebMMO.Web.Areas.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebMMO.Web.Areas.API.Controllers
{
    public class SShController : Controller
    {
        protected ServicesManager RepositoryService => ServicesManager.RepositoryService;
        // GET: API/SSh
        public JsonResult GetSShPro(SSHApiRequestModels apirequest)
        {
            var result = RepositoryService.SSHSystemService.GetSSHSystem(apirequest.AccessToken, apirequest.CountryCode);
            var response = new ResultDetail(ErrorConstants.Success, "Success", "v1", 0, result);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetGmailNoneProfiles(GmailProfilesApiRequestModels apirequest)
        {
            var result = RepositoryService.SSHSystemService.GetGmailNoneProfiles(apirequest.AccessToken, apirequest.CountryCode);
            var response = new ResultDetail(ErrorConstants.Success, "Success", "v1", 0, result);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateGmailStatus(GmailUpdateApiRequestModels apirequest)
        {
            var result = RepositoryService.SSHSystemService.UpdateGmailStatus(apirequest.Id, apirequest.statusprofile, apirequest.description);
            var response = new ResultDetail(ErrorConstants.Success, "Success", "v1", 0, result);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetYoutubeInfo(GmailProfilesApiRequestModels apirequest)
        {
            var result = RepositoryService.SSHSystemService.GetYoutubeInfo(apirequest.AccessToken, apirequest.CountryCode);
            var response = new ResultDetail(ErrorConstants.Success, "Success", "v1", 0, result);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetArticlesAdmod(GmailProfilesApiRequestModels apirequest)
        {
            var result = RepositoryService.SSHSystemService.GetArticlesAdmod(apirequest.CountryCode);
            var response = new ResultDetail(ErrorConstants.Success, "Success", "v1", 0, result);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}