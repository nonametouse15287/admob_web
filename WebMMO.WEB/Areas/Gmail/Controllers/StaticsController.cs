﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMMO.Core.DomainModels.Gmail;
using WebMMO.Service;

namespace WebMMO.Web.Areas.Gmail.Controllers
{
    public class StaticsController : Controller
    {
        protected ServicesManager RepositoryService => ServicesManager.RepositoryService;
        // GET: Gmail/Statics
        public ActionResult Index(string token)
        {
            GmailStatictisResult objModel = new GmailStatictisResult();
            var tokenCheck = RepositoryService.WebMMOService.CheckToken(token);
            if (tokenCheck != null)
            {
                objModel = RepositoryService.WebMMOService.GmailStatistics(token);
            }
            return View(objModel);
        }
    }
}