﻿(function (common, $, undefined) {

    common.parseDateFormat = function (asp_net_mvc_date, format) {
        if (asp_net_mvc_date != null) {
            return moment(asp_net_mvc_date).format(format);
        }
        return null;
    }

    common.parseDate = function (asp_net_mvc_date) {
        if (asp_net_mvc_date != null) {
            return moment(asp_net_mvc_date).format("DD/MM/YYYY");
        }
        return null;
    }

    common.parseDateTime = function (asp_net_mvc_date) {
        if (asp_net_mvc_date != null) {
            return moment(asp_net_mvc_date).format("DD/MM/YYYY HH:mm:ss");
        }
        return null;
    }

    common.post = function (url, data, successCallBackFn) {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                //if (response.Message && response.Message.length > 0) {
                //    if (response.IsSuccess) {
                //        //showNotification("success", response.Message);
                //    } else {
                //        //showNotification("error", response.Message);
                //    }
                //}
                successCallBackFn(response);
            },

            error: function (jqXHR, exception) {
                if (jqXHR.status === 403) {
                    //window.location.replace("/Home/Account/NotAuthorized");
                }
                else if (jqXHR.status === 500) {
                    showError("500: System Error");
                }
            }
        });
    }

    common.postFormWithImage = function (url, data, successCallBackFn) {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            processData: false,
            contentType: false,
            success: function (response) {
                //if (response.Message && response.Message.length > 0) {
                //    if (response.IsSuccess) {
                //        //showNotification("success", response.Message);
                //    } else {
                //        //showNotification("error", response.Message);
                //    }
                //}
                successCallBackFn(response);
            },

            error: function (jqXHR, exception) {
                if (jqXHR.status === 403) {
                    //window.location.replace("/Home/Account/NotAuthorized");
                }
                else if (jqXHR.status === 500) {
                    showError("500: System Error");
                }
            }
        });
    }

    common.get = function (url, data, successCallBackFn) {
        $.ajax({
            url: url,
            type: 'GET',
            data: data,
            success: function (response) {
                //if (response.Message && response.Message.length > 0) {
                //    if (response.IsSuccess) {
                //        //showNotification("success", response.Message);
                //    } else {
                //        //showNotification("error", response.Message);
                //    }
                //}
                successCallBackFn(response);
            },

            error: function (jqXHR, exception) {
                if (jqXHR.status === 403) {
                    //window.location.replace("/Home/Account/NotAuthorized");
                }
                else if (jqXHR.status === 500) {
                    showError("500: System Error");
                }
            }
        });
    }

    common.fnReadMore = function (dots, more, myBtn) {
        var dots = document.getElementById(dots);
        var moreText = document.getElementById(more);
        var btnText = document.getElementById(myBtn);

        if (dots.style.display === "none") {
            dots.style.display = "inline";
            btnText.innerHTML = "[+]";
            moreText.style.display = "none";
        } else {
            dots.style.display = "none";
            btnText.innerHTML = "[-]";
            moreText.style.display = "inline";
        }
    }

    common.fnShowStatus = function (Status, Pending, Reject, Verified, Approve, Pushed, Deleted) {
        var htmlReturn = ' ';
        htmlReturn = "<span style='color:#808080;font-weight: bold'>" + Pending + "</span>";
        if (Status === 0) {
            htmlReturn = "<span style='color:#808080;font-weight: bold'>" + Pending + "</span>";
        }

        if (Status === 1) {
            htmlReturn = "<span style='color:#da190b;font-weight: bold'>" + Reject + "</span>";
        }

        if (Status === 2) {
            htmlReturn = "<span style='color:#ff9800;font-weight: bold'>" + Verified + "</span>";
        }

        if (Status === 3) {
            htmlReturn = "<span style='font-weight: bold'>" + Pushed + "</span>";
        }

        if (Status === 4) {
            htmlReturn = "<span style='color:#46a049;font-weight: bold'>" + Approve + "</span>";
        }

        if (Status === 5) {
            htmlReturn = "<span style='color:#da190b;font-weight: bold'>" + Deleted + "</span>";
        }

        return htmlReturn;
    }

    common.tinyMCEConfig = {
        height: 300,
        theme: "modern", mode: "textareas",
        plugins: [
             "advlist autolink link lists charmap print preview hr pagebreak",
             "searchreplace wordcount visualblocks visualchars insertdatetime nonbreaking",
             "table contextmenu directionality emoticons paste textcolor fullscreen image code"
        ],
        toolbar1: "| fontselect | fontsizeselect | undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect | insert",
        toolbar2: "| cut | copy | paste | link unlink anchor | image | forecolor backcolor  | print preview fullscreen | image | code",
        image_advtab: true
    }

})(window.common = window.common || {}, jQuery);