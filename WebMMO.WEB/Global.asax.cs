﻿using WebMMO.Web.ActionFilter;
using WebMMO.Web.SyncData;
using Quartz;
using Quartz.Impl;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebMMO.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            #region Dong bo du lieu len ElasticSearch

            //IJobDetail jobSyncElasticSearch = JobBuilder.Create<SyncImagesElasticSearch>()
            //          .WithIdentity("jobSyncElasticSearch", "MixInfor")
            //          .Build();

            //ITrigger triggerSyncElasticSearch = TriggerBuilder.Create()
            //           .WithIdentity("triggerSyncElasticSearch", "MixInfor")
            //           .StartNow()
            //           .WithSimpleSchedule(x => x
            //                .WithIntervalInSeconds(30)
            //                .RepeatForever())
            //           .Build();
            //scheduler.ScheduleJob(jobSyncElasticSearch, triggerSyncElasticSearch);

            #endregion

            #region Cao SSH

            //IJobDetail jobSyncSSH = JobBuilder.Create<SyncSSH>()
            //          .WithIdentity("jobSyncSSH", "MixInfor")
            //          .Build();

            //ITrigger triggerSyncSSH = TriggerBuilder.Create()
            //           .WithIdentity("triggerSyncSSH", "MixInfor")
            //           .StartNow()
            //           .WithSimpleSchedule(x => x
            //                .WithIntervalInSeconds(600)
            //                .RepeatForever())
            //           .Build();
            //scheduler.ScheduleJob(jobSyncSSH, triggerSyncSSH);

            #endregion

            #region Check SSH

            //IJobDetail jobSyncCheckSSH = JobBuilder.Create<SyncCheckSSH>()
            //          .WithIdentity("jobSyncCheckSSH", "MixInfor")
            //          .Build();

            //ITrigger triggerSyncCheckSSH = TriggerBuilder.Create()
            //           .WithIdentity("triggerSyncCheckSSH", "MixInfor")
            //           .StartNow()
            //           .WithSimpleSchedule(x => x
            //                .WithIntervalInSeconds(30)
            //                .RepeatForever())
            //           .Build();
            //scheduler.ScheduleJob(jobSyncCheckSSH, triggerSyncCheckSSH);

            #endregion

        }

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new SecurityFilter());
        }
    }
}
