﻿using WebMMO.Common.Helpes;
using WebMMO.Service;
using WebMMO.Service.Helpes;
using Quartz;
using System;

namespace WebMMO.Web.SyncData
{
    [PersistJobDataAfterExecution]
    [DisallowConcurrentExecution]
    public class SyncImagesElasticSearch : IJob
    {
        protected ServicesManager RepositoryService => ServicesManager.RepositoryService;
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var resultData = RepositoryService.WebMMOService.GetAllImagesSync(ConfigHelpers.StatusPending);
                if (resultData.Count > 0)
                {
                    //send to ElasticSearch
                    if (resultData.Count != 0)
                    {
                        foreach (var item in resultData)
                        {
                            //ElasticSearchHelper.DeleteByAll(item.ID.ToString(), "Images", "SubImages");
                            var objectReturnOther = ElasticSearchHelper.InsertOne(item, item.ID.ToString(), "Images", "SubImages");
                            item.SyncElasticStatus = ConfigHelpers.StatusPushed;
                            RepositoryService.WebMMOService.AddAndEditImageSearch(item);
                        }
                    }
                }
                /*Remove ban ghi*/
                resultData = RepositoryService.WebMMOService.GetAllImagesSync(ConfigHelpers.StatusDeleted);
                if (resultData.Count > 0)
                {
                    //send to ElasticSearch
                    if (resultData.Count != 0)
                    {
                        foreach (var item in resultData)
                        {
                            var objectReturnOther = ElasticSearchHelper.DeleteById(item.ID.ToString(), "Images", "SubImages");
                            item.SyncElasticStatus = ConfigHelpers.StatusPushed;
                            RepositoryService.WebMMOService.AddAndEditImageSearch(item);
                        }
                    }
                }

                /*Refresh key*/

                if (DateTime.UtcNow.Hour == 0 && DateTime.UtcNow.Minute == 1)
                {
                    RepositoryService.WebMMOService.RefreshApiKey();
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}