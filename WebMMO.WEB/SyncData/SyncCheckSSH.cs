﻿using WebMMO.Common.Helpes;
using WebMMO.Core.DomainModels.SSH;
using WebMMO.Service;
using Quartz;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Threading;

namespace WebMMO.Web.SyncData
{
    [PersistJobDataAfterExecution]
    [DisallowConcurrentExecution]
    public class SyncCheckSSH : IJob
    {
        protected ServicesManager RepositoryService => ServicesManager.RepositoryService;
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                /*Get from server*/
                List<SSHWareHouseDto> lstSSH = new List<SSHWareHouseDto>();
                lstSSH = RepositoryService.SSHSystemService.GetTopSSH();
                if (lstSSH != null && lstSSH.Count > 0)
                {
                    foreach (var item in lstSSH)
                    {
                        ThreadPool.QueueUserWorkItem(new WaitCallback(fnWorkMethod), item);
                    }
                }
                /*resetssh*/
                if (DateTime.UtcNow.Hour == 23 && DateTime.UtcNow.Minute == 59)
                {
                    RepositoryService.SSHSystemService.ResetSSH();
                }

            }
            catch (Exception ex)
            {

            }
        }

        private bool CheckSSHLive(string username, string password, string ipaddress)
        {
            Random rd = new Random();
            int PortForwarded = rd.Next(1000, 9999);

            using (var client = new SshClient(ipaddress, username, password))
            {
                try
                {
                    //client.KeepAliveInterval = new TimeSpan(0, 0, 10);
                    //client.ConnectionInfo.Timeout = new TimeSpan(0, 0, 10);

                    client.Connect();
                    client.Disconnect();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        private void fnWorkMethod(object stateInfo)
        {
            SSHWareHouseDto item = (SSHWareHouseDto)stateInfo;
            item.CreatedDate = DateTime.UtcNow.Date;
            if (!CheckSSHLive(item.username, item.password, item.IpAddress))
            {
                item.Status = ConfigHelpers.StatusDeleted;
            }
            else
            {
                item.Status = ConfigHelpers.StatusApproved;
            }
            RepositoryService.SSHSystemService.UpdateStatusSSH(item);
        }

    }
}