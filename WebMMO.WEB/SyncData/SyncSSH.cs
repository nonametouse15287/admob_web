﻿using WebMMO.Common.Helpes;
using WebMMO.Core.DomainModels.SSH;
using WebMMO.Service;
using Newtonsoft.Json;
using Quartz;
using RestSharp;
using System;
using System.Collections.Generic;

namespace WebMMO.Web.SyncData
{
    [PersistJobDataAfterExecution]
    [DisallowConcurrentExecution]
    public class SyncSSH : IJob
    {
        protected ServicesManager RepositoryService => ServicesManager.RepositoryService;
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                /*Get from server*/
                var request = new RestRequest("APIv2?token=7324ebde771cc91ab14266cd62e1858f&code=ALL&size=100", Method.POST);
                SShModels objSSH = ExecuteREST<SShModels>(request);
                SSHWareHouseDto objSSHInsert = new SSHWareHouseDto();
                List<SSHWareHouseDto> lstSSH = new List<SSHWareHouseDto>();
                if (objSSH != null && objSSH.status)
                {
                    foreach (var item in objSSH.listSSH)
                    {
                        //87.92.89.87|admin|admin|Finland (FI)|90520|08|Oulu
                        string[] arraySSH = item.Split('|');
                        objSSHInsert = new SSHWareHouseDto();
                        objSSHInsert.username = arraySSH[1];
                        objSSHInsert.password = arraySSH[2];
                        objSSHInsert.IpAddress = arraySSH[0];
                        objSSHInsert.CountryCode = arraySSH[3];
                        objSSHInsert.CreatedDate = DateTime.UtcNow;
                        objSSHInsert.Status = ConfigHelpers.StatusPending;
                        objSSHInsert.FullText = item;
                        lstSSH.Add(objSSHInsert);
                    }
                    RepositoryService.SSHSystemService.InsertSShByList(lstSSH);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static T ExecuteREST<T>(RestRequest request) where T : new()
        {
            var client = new RestClient();
            client.BaseUrl = new System.Uri("https://bitsocks.us");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            var response = client.Execute<T>(request);
            response.Data = JsonConvert.DeserializeObject<T>(((RestSharp.RestResponseBase)(response)).Content);
            return response.Data;
        }
    }
}