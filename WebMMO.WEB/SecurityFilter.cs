﻿using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace WebMMO.Web
{
    public class SecurityFilter : IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            HttpCookie authCookie = filterContext.HttpContext.Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie != null)
            {
                // Get the roles of the logged in user.
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                string[] roles = authTicket.UserData.Split('|');

                // Create the identity and principal object and attach it to the current HttpContext.
                FormsIdentity identity = new FormsIdentity(authTicket);
                GenericPrincipal principal = new GenericPrincipal(identity, roles);
                filterContext.HttpContext.User = principal;



            }
            else
            {

            }
        }
    }
}