﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMMO.Web.Code
{
    public class Cities
    {
        public string Country { get; set; }
        public string Name { get; set; }
        public string Lat { get; set; }
        public string Lng { get; set; }
    }
}