﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMMO.Web.Code
{
    public class AndroidVersion
    {
        public int API { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public AndroidVersion()
        {

        }
        public AndroidVersion(int api, string name, string code)
        {
            this.API = api;
            this.Name = name;
            this.Code = code;
        }
    }
}