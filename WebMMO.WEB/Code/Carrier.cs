﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMMO.Web.Code
{
    public class Carrier
    {
        public string Carrier_Name { get; set; }
        public string Company { get; set; }
        public string Country { get; set; }
        public string Country_ISO { get; set; }
        public string Country_Code { get; set; }
        public string[] Mobile_Prefixes { get; set; }
        public string Size_Of_PhoneNumber { get; set; }
        public string[] Number_Formats { get; set; }
        public string[] Carrier_Codes { get; set; }

    }
}