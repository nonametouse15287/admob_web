﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMMO.Web.Code
{
    public class Sim
    {
        public string OperatorName { get; set; }
        public string LineNumber1 { get; set; }
        public string SimSubcribleId { get; set; }
        public string NetworkCode { get; set; }
        public string SimCountryISO { get; set; }
        public string SimSerialNumber { get; set; }
        public string SimState { get; set; }
        public string Mcc { get; set; }
        public string Mnc { get; set; }
        public string IMSI { get; set; }
    }
}