﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMMO.Web.Code
{
    public class AndroidDevicesResult
    {
        public int click { get; set; }
        public AndroidDevice DeviceInfo { get; set; }
    }
    public class AndroidDevice
    {
        public string CountryISO { get; set; }
        public string LanguageCode { get; set; }
        public string TimeZone { get; set; }
        public string Imei { get; set; }
        public string Imsi { get; set; }
        public string AndroidID { get; set; }
        public string AndroidSerial { get; set; }
        public string GaID { get; set; }
        public string GsFID { get; set; }
        public string Mac { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Board { get; set; }
        public string Brand { get; set; }
        public string Device { get; set; }
        public string Display { get; set; }
        public string FingerPrint { get; set; }
        public string HardWare { get; set; }
        public string Id { get; set; }
        public string Manufacture { get; set; }
        public string Model { get; set; }
        public string Product { get; set; }
        public string Bootloader { get; set; }
        public string Host { get; set; }
        public string Type { get; set; }
        public string Incremental { get; set; }
        public string Release { get; set; }
        public string CodeName { get; set; }
        public string Serial { get; set; }
        public string Description { get; set; }
        public string InstallTime { get; set; }
        public string Tags { get; set; }
        public string Resolution { get; set; }
        public long Time { get; set; }
        public string User { get; set; }
        public string UserAgent { get; set; }
        public string Base_OS { get; set; }
        public string SecurityPatch { get; set; }
        public string PreviewSDK { get; set; }
        public string SdkInt { get; set; }
        public string ApiVersion { get; set; }
        public string BuildID { get; set; }
        public string PreviewSDKInt { get; set; }
        public string BuildTime { get; set; }
        public string KernelVersion { get; set; }
        public string Radio { get; set; }
        public string Other1 { get; set; }
        public string Other2 { get; set; }
        public string Other3 { get; set; }
        public string OperatorName { get; set; }
        public string LineNumber1 { get; set; }
        public string SimSubcribleId { get; set; }
        public string NetworkCode { get; set; }
        public string SimCountryISO { get; set; }
        public string SimSerialNumber { get; set; }
        public string SimState { get; set; }
        public int NetworkType { get; set; }
        public string Ssid { get; set; }
        public string BSsid { get; set; }
        public string WifiName { get; set; }
        public string MacAddress { get; set; }
        public string Mcc { get; set; }
        public string Mnc { get; set; }
        public string DeviceId { get; set; }
        public int DPI { get; set; }
    }
}