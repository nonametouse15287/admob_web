﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebMMO.Web.Code
{
    public static class GetStatusClick
    {
        public static bool GetClick(string CountryCode)
        {
            bool checkClick = true;
            Random r = new Random();
            int iTrongSo = 950;
            int iRandomClick = r.Next(0, 10000);

            int HourOfDay = DateTime.UtcNow.Hour;

            if (HourOfDay >= 23 || HourOfDay <= 6)
            {
                //Ty le duoi 0.3%
                checkClick = iRandomClick <= 3 * iTrongSo;
            }

            if (6 < HourOfDay && HourOfDay < 11)
            {
                //Ty le duoi 1%
                checkClick = iRandomClick <= 10 * iTrongSo;
            }

            if (11 <= HourOfDay && HourOfDay <= 14)
            {
                //Ty le duoi 2%
                checkClick = iRandomClick <= 20 * iTrongSo;
            }

            if (15 <= HourOfDay && HourOfDay <= 19)
            {
                //Ty le duoi 1%
                checkClick = iRandomClick <= 10 * iTrongSo;
            }

            if (20 <= HourOfDay && HourOfDay <= 22)
            {
                //Ty le duoi 0.5%
                checkClick = iRandomClick <= 5 * iTrongSo;
            }

            return checkClick;
        }
    }
}