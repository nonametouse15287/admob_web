﻿using GeoTimeZone;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using WebMMO.Core.DomainModels.devices;
using WebMMO.Service;

namespace WebMMO.Web.Code
{
    public class DeviceHelper
    {
     

        public static string RandomNumber(int length)
        {
            var numbers = "0123456789";
            var stringChars = new char[length];
            var random = new Random();

            for (int i = 0; i < length; i++)
            {
                stringChars[i] = numbers[random.Next(0, numbers.Length)];
            }

            var finalString = new String(stringChars);
            return finalString;
        }

        public static string RandomText(int length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var stringChars = new char[length];
            var random = new Random();

            for (int i = 0; i < length; i++)
            {
                stringChars[i] = chars[random.Next(0, chars.Length)];
            }
            var finalString = new String(stringChars);
            finalString = finalString.ToLower();
            return finalString;
        }

        public static string RandomString(int length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[length];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            return finalString;
        }

        public static string GetRandomMacAddress()
        {
            var random = new Random();
            var buffer = new byte[6];
            random.NextBytes(buffer);
            var result = String.Concat(buffer.Select(x => string.Format("{0}:", x.ToString("X2"))).ToArray());
            return result.TrimEnd(':').ToUpper();
        }

        public static string GetTimeZoneByCountry(string countryCode)
        {
            return "";
        }

        public static string GetTimeZoneByIpRequest(string ipRequest)
        {
            return "";
        }

        public static string GetGPSByIpRequest(string ipRequest)
        {
            return "";
        }

        public static string GetGPSByCountry(string countryCode)
        {
            return "x";
        }

        public static string GetRandomGAID()
        {
            //8(number) + "-" + 4(char) + "-" + 4 char  +"-" + 12 char
            return RandomNumber(8) + "-" + RandomString(4) + "-" + RandomString(4) + "-" + RandomString(4) + "-" + RandomString(12);
        }

        public static string GetGSFID()
        {
            return RandomString(16);
        }

        public static string GetAndroidID()
        {
            return RandomString(16);
        }

        /// <summary>
        /// 1 = 2G
        /// 2 = 3G
        /// 3 = 4G
        /// 4 = WIFI
        /// </summary>
        /// <returns></returns>
        public static string GetMobileType()
        {
            Random random = new Random();
            return random.Next(1, 5).ToString();
        }


        /// <summary>
        /// BrowserType: 1 Chrome
        ///              2 Firefox
        ///              3 UCBrowser
        /// </summary>
        /// <param name="browserType"></param>
        /// <param name="model"></param>
        /// <param name="release"></param>
        /// <param name="build"></param>
        /// <param name="patchVersion"></param>
        /// <param name="browserVersion"></param>
        /// <returns></returns>
        public static string GetRandomtUA(int browserType, string model, string release, string build, string patchVersion, string browserVersion)
        {
            //string userAgent = "Mozilla/5.0 (Linux; Android {Release}; {Model} Build/{Build_ID}; wv) AppleWebKit/{Path_Version} (KHTML, like Gecko) Version/4.0 Chrome/{Version_Chrome} Mobile Safari/{Path_Version}";
            string userAgent = string.Empty;
            string userAgentChromeTemp = "Mozilla/5.0 (Linux; Android {0}; {1} Build/{2}; wv) AppleWebKit/{3} (KHTML, like Gecko) Version/4.0 Chrome/{4} Mobile Safari/{5}";
            switch (browserType)
            {
                case 1:
                    userAgent = string.Format(userAgentChromeTemp, release, model, build, patchVersion, browserVersion, patchVersion);
                    break;
                case 2:
                    break;
                case 3:
                    break;
            }
            return userAgent;
        }

        public static string GetFingerPrint(string manufacture, string product, string productSub, string releaseVersion, string model, string incremental, string type, string tags)
        {
            return manufacture + "/" + product + "/" + productSub.Substring(0, productSub.Length - 1) + ":" + releaseVersion + "/" + model + "/" + incremental + ":" + type + "/" + tags;
        }

        public static List<Carrier> loadJsonToObject()
        {
            string strContent = string.Empty;
            List<string> lstChromeVersion = new List<string>();
            string filePath = HttpContext.Current.Server.MapPath(@"~/DataConfig/carrier.json");
            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }
            using (StreamReader reader = new StreamReader(filePath))
            {
                strContent = reader.ReadToEnd();
            }
            var listObjects = JsonConvert.DeserializeObject<List<Carrier>>(strContent) as List<Carrier>;
            return listObjects;
        }

        public static List<CitiesCodeDto> loadJsonCitiesToObject(string countryCode)
        {
            ServicesManager RepositoryService = ServicesManager.RepositoryService;          
            List<CitiesCodeDto> listCities = RepositoryService.WebMMOService.GetCitiesCode(countryCode);         
            return listCities;
        }

        public static Carrier getCarrierByCountryCode(string code)
        {
            var listCarries = loadJsonToObject();
            var listCarriersByCountryCode = listCarries.Where(x => x.Country_ISO == code.ToUpper());
            Random r = new Random();
            if (listCarriersByCountryCode != null)
            {
                int index = r.Next(0, listCarriersByCountryCode.Count());
                return listCarriersByCountryCode.ElementAt(index);
            }
            return null;
        }

        public static CitiesCodeDto getCitiesByCountryCode(string code)
        {
            var listCitiesCode = loadJsonCitiesToObject(code);
            var listCarriersByCountryCode = listCitiesCode.Where(x => x.country == code.ToUpper());
            Random r = new Random();
            if (listCarriersByCountryCode != null)
            {
                int index = r.Next(0, listCarriersByCountryCode.Count());
                return listCarriersByCountryCode.ElementAt(index);
            }
            return null;
        }

        public static List<AndroidVersion> BuildListAndroid()
        {

            string filePath = HttpContext.Current.Server.MapPath(@"~/DataConfig/android_version.json");
            string strContent = File.ReadAllText(filePath);
            var listObjects = JsonConvert.DeserializeObject<List<AndroidVersion>>(strContent) as List<AndroidVersion>;
            return listObjects;
        }

        public static string BuildChromeVersion()
        {
            Random r = new Random();

            List<string> lstChromeVersion = new List<string>();
            string filePath = HttpContext.Current.Server.MapPath(@"~/DataConfig/chromeversion.txt");

            using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None)) { }
            using (StreamReader reader = new StreamReader(filePath))
            {
                while (!reader.EndOfStream)
                {
                    string vVersion = reader.ReadLine();
                    if (!string.IsNullOrEmpty(vVersion))
                    {
                        lstChromeVersion.Add(vVersion);
                    }
                }
            }

            return lstChromeVersion[r.Next(0, lstChromeVersion.Count - 1)];
        }


        public static Sim getSimByCountryCode(string code)
        {
            Sim sim = new Sim();
            Carrier carrier = getCarrierByCountryCode(code);
            if (carrier != null)
            {
                string[] carrierCodes = carrier.Carrier_Codes;

                sim.Mcc = carrierCodes[0].Split(' ')[1];
                sim.Mnc = carrierCodes[0].Split(' ')[0];
                sim.OperatorName = carrier.Carrier_Name;
                sim.NetworkCode = carrierCodes[0].Split(' ')[0] + carrierCodes[0].Split(' ')[1];
                sim.SimCountryISO = code;
                sim.SimState = "";
                sim.SimSerialNumber = "89" + carrier.Country_Code.Replace("+", "") + RandomNumber(16);
                sim.SimSubcribleId = carrierCodes[0].Split(' ')[0] + carrierCodes[0].Split(' ')[1] + RandomNumber(10);
                string[] mobilePrefix = carrier.Mobile_Prefixes;
                string[] mobileFormat = carrier.Number_Formats;
                int randomPrefix = new Random().Next(0, mobilePrefix.Length);
                int sizeOfNumber = int.Parse(carrier.Size_Of_PhoneNumber);
                sim.LineNumber1 = carrier.Country_Code + "" + mobilePrefix[randomPrefix].Replace("x", RandomNumber(1).ToString()) + RandomNumber(sizeOfNumber - mobilePrefix[randomPrefix].Length);
                sim.IMSI = carrierCodes[0].Split(' ')[0] + carrierCodes[0].Split(' ')[1] + RandomNumber(10);
            }
            return sim;
        }



        public static string getBssid()
        {
            return GetRandomMacAddress().ToLower();
        }

        public static string getSsid()
        {
            return RandomString(1).ToUpper() + RandomString(15);
        }

        public static AndroidDevice BuildRandomDevice(DeviceTemp deviceTemp, string code)
        {
            AndroidDevice androidDevice = new AndroidDevice();
            androidDevice.AndroidID = GetAndroidID();
            androidDevice.AndroidSerial = RandomString(deviceTemp.AndroidSerial.Length).ToUpper();

            AndroidVersion androidVersion = new AndroidVersion();
            androidVersion = BuildListAndroid().ElementAt(new Random().Next(0, BuildListAndroid().Count));
            androidDevice.ApiVersion = androidVersion.API.ToString();
            androidDevice.Base_OS = deviceTemp.Base_OS;
            androidDevice.Board = deviceTemp.Board;
            androidDevice.Bootloader = deviceTemp.Bootloader;
            androidDevice.Brand = deviceTemp.Brand;
            androidDevice.BSsid = getBssid();
            androidDevice.BuildID = deviceTemp.BuildID;
            androidDevice.BuildTime = deviceTemp.BuildTime;
            androidDevice.CodeName = deviceTemp.CodeName;
            androidDevice.CountryISO = code;
            androidDevice.Device = deviceTemp.Device;
            androidDevice.Display = deviceTemp.Display;
            androidDevice.FingerPrint = GetFingerPrint(deviceTemp.Manufacture, deviceTemp.Product, deviceTemp.Product, androidVersion.Code, deviceTemp.Model, deviceTemp.Incremental, deviceTemp.Type, deviceTemp.Tags);
            androidDevice.GaID = GetRandomGAID();
            androidDevice.GsFID = GetGSFID();
            androidDevice.HardWare = deviceTemp.Hardware;
            androidDevice.Host = deviceTemp.Host;

            ChangeImei changeImei = new ChangeImei();
            string imei = changeImei.getRandomImei(deviceTemp.Imei);
            androidDevice.Imei = imei;
            androidDevice.Imsi = imei;
            androidDevice.Incremental = deviceTemp.Incremental;
            androidDevice.InstallTime = deviceTemp.InstallTime;
            androidDevice.KernelVersion = deviceTemp.KernelVersion;
            androidDevice.LanguageCode = code;
            androidDevice.Latitude = GetGPSByCountry("").Split('x')[0].ToString();
            androidDevice.Longitude = GetGPSByCountry("").Split('x')[1].ToString();
            androidDevice.Model = deviceTemp.Model;
            androidDevice.Manufacture = deviceTemp.Manufacture;

            Sim sim = getSimByCountryCode(code);
            androidDevice.LineNumber1 = sim.LineNumber1;
            androidDevice.Mnc = sim.Mnc;
            androidDevice.Mcc = sim.Mcc;
            androidDevice.SimCountryISO = code;
            androidDevice.SimSerialNumber = sim.SimSerialNumber;
            androidDevice.SimState = sim.SimState;
            androidDevice.SimSubcribleId = sim.SimSubcribleId;

            androidDevice.OperatorName = sim.OperatorName;
            androidDevice.NetworkCode = sim.NetworkCode;
            androidDevice.NetworkType = int.Parse(GetMobileType());
            androidDevice.PreviewSDK = deviceTemp.PreviewSDK;
            androidDevice.PreviewSDKInt = deviceTemp.PreviewSDKInt;
            androidDevice.Product = deviceTemp.Product;
            androidDevice.Radio = deviceTemp.Radio;
            androidDevice.Release = androidVersion.Code;
            androidDevice.Resolution = deviceTemp.Resolution;
            androidDevice.SdkInt = androidVersion.API.ToString();
            androidDevice.SecurityPatch = deviceTemp.SecurityPath;
            androidDevice.Serial = deviceTemp.Serial;
            androidDevice.Ssid = getSsid();
            androidDevice.Tags = deviceTemp.Tags;
            androidDevice.Time = 0;// deviceTemp.Time;
            androidDevice.TimeZone = GetTimeZoneByCountry(code);
            androidDevice.Type = deviceTemp.Type;
            androidDevice.User = deviceTemp.User;
            androidDevice.Mac = GetRandomMacAddress();
            androidDevice.Description = deviceTemp.Model + "-user " + androidVersion.Code + " " + deviceTemp.Display + " " + deviceTemp.Incremental + " release=keys";// "";// deviceTemp.Description;
            androidDevice.MacAddress = GetRandomMacAddress().ToUpper();
            androidDevice.WifiName = RandomString(16).ToUpper();
            androidDevice.LanguageCode = code;
            androidDevice.DPI = 480;
            androidDevice.DeviceId = androidDevice.Imei;
            androidDevice.Device = deviceTemp.Model;
            androidDevice.Id = deviceTemp.Display;
            CitiesCodeDto cities = getCitiesByCountryCode(code);
            if (cities != null)
            {
                androidDevice.Latitude = cities.lat.ToString();
                androidDevice.Longitude = cities.lng.ToString();
                string tz = TimeZoneLookup.GetTimeZone(Double.Parse(androidDevice.Latitude), Double.Parse(androidDevice.Longitude)).Result;
                androidDevice.TimeZone = tz;
            }
          
            //Todo: them bang chrome version 1:version, pack number, Browser Type
            androidDevice.UserAgent = GetRandomtUA(1, androidVersion.Code, deviceTemp.Model, deviceTemp.BuildID, "537.36", BuildChromeVersion());
            return androidDevice;
        }
    }
}