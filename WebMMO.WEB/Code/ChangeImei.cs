﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace WebMMO.Web.Code
{
    public class ChangeImei
    {
        private string getModelImei(string model)
        {
            return model.Substring(2, 4);
        }

        private string getLocationImei(string model)
        {
            return model.Substring(6, 2);
        }

        private string buildRandom6Character(int numberOfRandom)
        {
            string result = string.Empty;
            for (int i = 0; i < numberOfRandom; i++)
            {
                Random r = new Random();
                int rInt = r.Next(3, 9);
                Thread.Sleep(200);
                result += rInt.ToString();
            }

            return result;
        }

        private string getLastLuhnDigit(string imei)
        {
            string sub = "";
            int total = 0;
            int length = imei.Length;// - 1;

            if (imei.Length == 14 || imei.Length == 16)
            {
                for (int i = 0; i < length; i++)
                {
                    if (i % 2 != 0)
                    {
                        sub = ((int)char.GetNumericValue(imei[i]) * 2).ToString();
                        total += sub.Length == 2 ? (int)char.GetNumericValue(sub[0]) + (int)char.GetNumericValue(sub[1]) : (int)char.GetNumericValue(sub[0]);

                    }
                    else
                        total += ((int)char.GetNumericValue(imei[i]));
                }
            }
            return ((((total / 10) + 1) * 10) - total).ToString(); //((total / 10) + 1) * 10) - total));
        }

        public string getRandomImei(string tempImei)
        {
            string[] arrayIm = null;
            try
            {
                arrayIm = tempImei.Replace("\n", "|").Split('|');
                tempImei = arrayIm[0];
            }
            catch
            {   
            }

            int numOfRandom = 0;
            if (tempImei.Length == 15)
            {
                numOfRandom = 6;
            }
            if (tempImei.Length == 17)
            {
                numOfRandom = 8;
            }
            string prefix = tempImei.Substring(0, 2);
            string model = getModelImei(tempImei);
            string location = getLocationImei(tempImei);
            string random6Digis = buildRandom6Character(numOfRandom);
            string newImei = prefix + "" + model + "" + location + "" + random6Digis + "" + getLastLuhnDigit(prefix + "" + model.ToString() + location.ToString() + random6Digis.ToString());
            return newImei;
        }
    }
}