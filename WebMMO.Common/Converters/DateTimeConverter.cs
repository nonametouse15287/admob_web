﻿using System;
using WebMMO.Common.Helpes;

namespace WebMMO.Common.Converters
{
    public static class DateTimeConverter
    {
        public static DateTime? ConvertDateTime(string strValue, string strFormat)
        {
            DateTime? dt = null;
            string[] arrDt;
            switch (strFormat)
            {
                case ConfigHelpers.FormatDate:
                    arrDt = strValue.Split('/');
                    dt = new DateTime(int.Parse(arrDt[2]), int.Parse(arrDt[1]), int.Parse(arrDt[0]));
                    break;
                case ConfigHelpers.FormatDateTime:
                    var arrDtBegin = strValue.Split(' ');
                    arrDt = arrDtBegin[0].Split('/');
                    var arrTime = arrDtBegin[1].Split(':');
                    dt = new DateTime(int.Parse(arrDt[2]), int.Parse(arrDt[1]), int.Parse(arrDt[0]),
                        int.Parse(arrTime[0]), int.Parse(arrTime[1]), int.Parse(arrTime[1]));
                    break;
            }
            return dt;
        }
    }
}
