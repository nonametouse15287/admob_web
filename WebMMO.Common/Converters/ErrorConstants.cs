﻿namespace WebMMO.Common.Converters
{
    public static class ErrorConstants
    {
        public const int Success = 0;
        public const int Unknown = Success - 1;
        public const int LoginExpried = Unknown - 1;
        public const int SearchNotFound = LoginExpried - 1;
        
    }
}
