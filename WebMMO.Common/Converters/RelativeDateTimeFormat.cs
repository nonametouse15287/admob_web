﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebMMO.Common.Converters
{
    /// <summary>
    /// A custom formatter for displaying DateTime in relative date / time format.
    /// </summary>
    public class RelativeDateTimeFormat : IFormatProvider, ICustomFormatter
    {
        public object GetFormat(Type formatType)
        {
            return formatType == typeof(ICustomFormatter) ? this : null;
        }

        public string Format(string fmt, object arg, IFormatProvider formatProvider)
        {
            if (arg.GetType() != typeof(DateTime))
            {
                throw new FormatException("Invalid type for RelativeDateTimeFormat.");
            }

            TimeSpan oSpan = DateTime.Now.Subtract((DateTime)arg);
            var totalMinutes = oSpan.TotalMinutes;
            var suffix = " ago";

            if (totalMinutes < 0.0)
            {
                totalMinutes = Math.Abs(totalMinutes);
                suffix = " from now";
            }

            Dictionary<double, string> aValue = new Dictionary<double, string>
            {
                {0.75, "less than a minute"},
                {1.5, "about a minute"},
                {45, $"{Math.Round(totalMinutes)} minutes"},
                {90, "about an hour"},
                {1440, $"about {Math.Round(Math.Abs(oSpan.TotalHours))} hours"},
                {2880, "a day"},
                {43200, $"{Math.Floor(Math.Abs(oSpan.TotalDays))} days"},
                {86400, "about a month"},
                {525600, $"{Math.Floor(Math.Abs(oSpan.TotalDays / 30))} months"},
                {1051200, "about a year"},
                {double.MaxValue, $"{Math.Floor(Math.Abs(oSpan.TotalDays / 365))} years"}
            };
        
            return aValue.First(n => totalMinutes < n.Key).Value + suffix;
        }
    }
}