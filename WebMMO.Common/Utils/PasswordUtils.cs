﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace WebMMO.Common.Utils
{
    public static class PasswordUtils
    {
        private const string charset = "0123456789abcdefghijklmnopqrstuvwxyz";
        private const string charset2 = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public static string GenerateRequestKey()
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random(Guid.NewGuid().GetHashCode());

            for (var i = 0; i < 10; i++)
            {
                var index = random.Next(0, charset.Length);
                builder.Append(charset[index]);
            }

            return builder.ToString();
        }

        public static string GenerateRequestKey2(int icharrequest)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random(Guid.NewGuid().GetHashCode());

            for (var i = 0; i < icharrequest; i++)
            {
                var index = random.Next(0, charset2.Length);
                builder.Append(charset2[index]);
            }
            return builder.ToString();
        }

        public static byte[] EncryptPassword(string emailAddress, string password)
        {
            MD5 md5 = MD5.Create();
            return md5.ComputeHash(Encoding.UTF8.GetBytes($"{password}{emailAddress}"));
        }

        public static bool VerifyPassword(string emailAddress, string password, byte[] currentPassword)
        {
            MD5 md5 = MD5.Create();
            byte[] encryptedPassword = md5.ComputeHash(Encoding.UTF8.GetBytes($"{password}{emailAddress}"));

            return (currentPassword != null && currentPassword.SequenceEqual(encryptedPassword));
        }

        public static string CreateRandomPassword(int passwordLength)
        {
            string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
            Random randNum = new Random();
            char[] chars = new char[passwordLength];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < passwordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(chars);
        }

        public static string DecodeFrom64(string encodedData)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);
            string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
            return returnValue;
        }

        public static string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }
    }
}
