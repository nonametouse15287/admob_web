﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BaseWeb.Common.Utils
{
    public static class ExtensionMethods
    {
        public static string ToSnakeCase(this string camel, char delimiter = '_')
        {
            return string.IsNullOrWhiteSpace(camel) ? camel : new string(InsertDelimiterBeforeCaps(camel, delimiter).ToArray());
        }

        private static IEnumerable<char> InsertDelimiterBeforeCaps(IEnumerable input, char delimiter)
        {
            var lastCharWasUppper = false;
            var isFirst = true;
            foreach (char c in input)
            {
                if (char.IsUpper(c))
                {
                    if (!lastCharWasUppper && !isFirst)
                    {
                        yield return delimiter;
                        lastCharWasUppper = true;
                    }
                    yield return char.ToLower(c);
                    continue;
                }
                isFirst = false;
                yield return c;
                lastCharWasUppper = false;
            }
        }
    }
}
