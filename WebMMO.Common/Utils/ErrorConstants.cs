﻿
namespace BaseWeb.Common.Utils
{
    public static class ErrorConstants
    {
        /**/
        public const int Success = 0;
        public const int Unknown = Success - 1;
        public const int LoginExpried = Unknown - 1;

        /*System Error*/

        /*Authentication*/
        public const int SystemInsufficientPermission = -1000;

        /*Property*/
        public const int InvalidPropertyValue = -2000;

        /*Ojbect Not found*/
        public const int OjectNotFound = -3000;
        /*Place*/
        public const int PlaceError = -4000;
        public const int FavoriteExist = PlaceError - 1;
        public const int FavoriteInsertFail = FavoriteExist - 1;
        public const int ReviewInsertFail = FavoriteInsertFail - 1;
        public const int CheckinInsertFail = ReviewInsertFail - 1;
        public const int FavoriteDeleteFail = CheckinInsertFail - 1;
        public const int ShareInsertFail = FavoriteDeleteFail - 1;
        public const int SearchPlaceNotFound = ShareInsertFail - 1;
        public const int SearchPlaceFavoriteNotFound = SearchPlaceNotFound - 1;
        public const int SearchPlaceReviewNotFound = SearchPlaceFavoriteNotFound - 1;
        public const int SearchPlaceChecInNotFound = SearchPlaceReviewNotFound - 1;

        /*Suggesttion*/
        public const int SuggestionError = -5000;
        public const int SuggestionDeleteFail = SuggestionError - 1;
        public const int SuggestionInsertFail = SuggestionDeleteFail - 1;
        public const int SuggestionUpdateFail = SuggestionInsertFail - 1;
        public const int SearchSuggestionNotFound = SuggestionUpdateFail - 1;
        /*User*/
        public const int UserError = -6000;
        public const int UserEmailExist = UserError - 1;
        public const int UserLoginFail = UserEmailExist - 1;
        public const int UserUpdateFail = UserLoginFail - 1;

        /*Product*/
        public const int ProductError = -7000;
        public const int SearchProductNotfound = ProductError - 1;

        /*Event*/
        public const int BaseEvent = -8000;
        public const int EventBadgestNofFound = BaseEvent - 1;
        public const int EventBadgestDetailsNofFound = EventBadgestNofFound - 1;
        

    }
}
