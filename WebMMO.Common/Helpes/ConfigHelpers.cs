﻿namespace WebMMO.Common.Helpes
{
    public static class ConfigHelpers
    {
        public const string FormEnctype = "multipart/form-data";
        public const string CacheViewmenu = "VIEWMENU";
        public const string FormatDate = "dd/MM/yyyy";
        public const string FormatDateTime = "dd/MM/yyyy HH:mm:ss";

        public static int UserroleCeo = 1;

        public static string UserroleHa = "";
        public static string IsUnderMaintenanceProperty= "isUnderMaintenance";
        public static string IsUnderMaintenanceValue = "1";
        public static string _ElasticSearchURL = "http://128.199.176.219:9200/";

        public static int StatusPending = 0;
        public static int StatusDisapproved = 1;
        public static int StatusVerified = 2;
        public static int StatusPushed = 3;
        public static int StatusApproved = 4;
        public static int StatusDeleted = 5;

        public static string StatusPendingText = "Mới tạo";
        public static string StatusDisapprovedText = "Đã từ chối";
        public static string StatusVerifiedText = "Đã Duyệt";
        public static string StatusPushedText = "Đã đẩy";
        public static string StatusApprovedText = "Đã chấp nhận";
        public static string StatusDeletedText = "Đã xóa";

    }
}
