﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace WebMMO.Common.Helpes
{
    public class SelectListModel
    {
        public static SelectList getStatuses()
        {
            return new SelectList(
                new List<SelectListItem>
                {
                    new SelectListItem { Selected = false, Text = "Enabled", Value = "Enabled" },
                    new SelectListItem { Selected = false, Text = "Disabled", Value = "Disabled" }
                }, "Value", "Text");
        }

        public static SelectList getStatusV1()
        {
            return new SelectList(
                new List<SelectListItem>
                {
                    new SelectListItem { Selected = true, Text = "--Chọn Trạng thái--", Value = "0,1,2,3,4,5" },
                    new SelectListItem { Selected = false, Text = "Mới tạo", Value = ConfigHelpers.StatusPending.ToString() },
                    new SelectListItem { Selected = false, Text = "Đã từ chối", Value = ConfigHelpers.StatusDisapproved.ToString() },
                    new SelectListItem { Selected = false, Text = "Đã Duyệt", Value = ConfigHelpers.StatusVerified.ToString() },
                    new SelectListItem { Selected = false, Text = "Đã đẩy", Value = ConfigHelpers.StatusPushed.ToString() },
                    new SelectListItem { Selected = false, Text = "Đã chấp nhận", Value = ConfigHelpers.StatusApproved.ToString() },
                    new SelectListItem { Selected = false, Text = "Đã xóa", Value = ConfigHelpers.StatusDeleted.ToString() },
                }, "Value", "Text");
        }
    }
}
