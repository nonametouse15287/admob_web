﻿using System;
using System.Net;
using BaseWeb.Common.Utils;
using BaseWeb.Core.MessageResponse;

namespace BaseWeb.Core.Exceptions
{
    public class BusinessRuleException : Exception
    {
        public BusinessRuleException(int code, string message, string version, object detail = null) : base(message)
        {
            ResultDetail = new ResultDetail(code, message, version, detail);
        }

        public BusinessRuleException(ResultDetail resultDetails) : base(resultDetails.Message)
        {
            ResultDetail = resultDetails;
        }

        public BusinessRuleException(ResultDetail resultDetails, Exception innerException) : base(resultDetails.Message, innerException)
        {
            ResultDetail = resultDetails;
        }

        protected BusinessRuleException(string message, string version, Exception innerException = null) : base(message, innerException)
        {
            ResultDetail = new ResultDetail(ErrorConstants.Unknown, message, version);
        }

        public ResultDetail ResultDetail { get; private set; }
    }


    public class BaseHttpException : BusinessRuleException
    {
        public HttpStatusCode HttpCode { get; private set; }

        public BaseHttpException(HttpStatusCode code, ResultDetail result, Exception innerException = null) : base(result, innerException)
        {
            HttpCode = code;
        }

        public static BaseHttpException BadRequest(int code, string message, string detail = "")
        {
            return BadRequest(new ResultDetail(code, message, detail));
        }

        public static BaseHttpException BadRequest(ResultDetail details)
        {
            return new BaseHttpException(HttpStatusCode.BadRequest, details);
        }
    }
}
