﻿using System.Collections.Generic;
using System.Web;

namespace WebMMO.Core.MessageResponse
{
    public class ServiceResponse<T>
    {
        public ServiceResponse(T respObject)
        {
            ResponseObject = respObject;
        }

        public ServiceResponse(T respObject, string property, string message, bool isError)
        {
            ResponseObject = respObject;
            Property = property;
            Message = message;
            IsError = isError;
        }

        public T ResponseObject { get; set; }
        public string Property { get; set; }
        public string Message { get; set; }
        public bool IsError { get; set; }
    }

    public class DataTableViewModel<T> where T : class
    {
        // base on jquery datatables
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<T> data { get; set; }

        public DataTableViewModel(int draw, int recordsTotal, int recordsFiltered, List<T> data)
        {
            this.data = data;
            this.draw = draw;
            this.recordsFiltered = recordsFiltered;
            this.recordsTotal = recordsTotal;
        }
    }

    public class FilterTableParams
    {
        public string Search { get; set; }

        public int SortColum { get; set; } // colum position on client view
        public string SortDirection { get; set; } // asc , desc

        public int Offset { get; set; }
        public int PageSize { get; set; }

        public int draw { get; set; }

        public FilterTableParams(HttpRequestBase request, int start, int length, int drawValue)
        {
            Search = request.QueryString["search[value]"];
            SortColum = int.Parse(request.QueryString["order[0][column]"]);
            SortDirection = request.QueryString["order[0][dir]"];
            Offset = start;
            PageSize = length;
            draw = drawValue;
        }
    }

}
