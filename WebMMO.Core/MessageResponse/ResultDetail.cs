﻿namespace WebMMO.Core.MessageResponse
{
    public class ResultDetail
    {
        public ResultDetail(int code, string message, string version, int total = 0, object details = null)
        {
            Code = code;
            Message = message;
            Details = details;
            Version = version;
            Total = total;
        }


        public int Code { get; }


        public string Message { get; }


        public string Version { get; }
        public int Total { get; }
        public object Details { get; }

    }

}
