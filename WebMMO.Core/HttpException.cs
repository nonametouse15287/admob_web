﻿using WebMMO.Common.Converters;
using WebMMO.Core.MessageResponse;
using System;
using System.Net;

namespace WebMMO.Core
{
    public class BusinessRuleException : Exception
    {
        public BusinessRuleException(int code, string message, string version, int total, object detail = null) : base(message)
        {
            ResultDetail = new ResultDetail(code, message, version, total, detail);
        }

        public BusinessRuleException(ResultDetail resultDetails) : base(resultDetails.Message)
        {
            ResultDetail = resultDetails;
        }

        public BusinessRuleException(ResultDetail resultDetails, Exception innerException) : base(resultDetails.Message, innerException)
        {
            ResultDetail = resultDetails;
        }

        protected BusinessRuleException(string message, string version, Exception innerException = null) : base(message, innerException)
        {
            ResultDetail = new ResultDetail(ErrorConstants.Unknown, message, version, 0);
        }

        public ResultDetail ResultDetail { get; private set; }
    }


    public class WebMMOHttpException : BusinessRuleException
    {
        public HttpStatusCode HttpCode { get; private set; }

        public WebMMOHttpException(HttpStatusCode code, ResultDetail result, Exception innerException = null) : base(result, innerException)
        {
            HttpCode = code;
        }

        public static WebMMOHttpException BadRequest(int code, string message, string detail = "")
        {
            return BadRequest(new ResultDetail(code, message, "", 0, detail));
        }

        public static WebMMOHttpException BadRequest(ResultDetail details)
        {
            return new WebMMOHttpException(HttpStatusCode.OK, details);
        }
    }
}
