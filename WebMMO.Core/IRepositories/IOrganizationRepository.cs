﻿using System.Collections.Generic;
using WebMMO.Core.DomainModels.Organization;

namespace WebMMO.Core.IRepositories
{
    public interface IOrganizationRepository
    {
       OrganizationDto GetOrganizationById(int id);

        List<OrganizationDto> GetOrganizationByCompanyCode(string firstLetter);
        int UpdateOrganization(OrganizationDto organization);
    }
}
