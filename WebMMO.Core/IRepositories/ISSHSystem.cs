﻿using System;
using System.Collections.Generic;
using WebMMO.Core.DomainModels.AppManage;
using WebMMO.Core.MessageResponse;
using WebMMO.Core.DomainModels.SSH;
using WebMMO.Core.DomainModels.Email;
using WebMMO.Core.DomainModels.Youtube;
using WebMMO.Core.DomainModels.Appsetting;

namespace WebMMO.Core.IRepositories
{
    public interface ISSHSystemRepository
    {
        Tuple<int, List<SSHWareHouseDto>> SearchSSHSystem(FilterTableParams tableParams);
        int InsertSShByList(List<SSHWareHouseDto> lstInsert);
        SSHWareHouseDto GetSSHSystem(string accessToken, string countryCode);
        List<SSHWareHouseDto> GetSSHSystem();
        void UpdateStatusSSH(SSHWareHouseDto item);
        void ResetSSH();
        GmailEmailResult GetGmailNoneProfiles(string accessToken, string countryCode);
        QuanLyKenhYoutubeResult GetYoutubeInfo();
        int UpdateGmailStatus(int id, int? statusprofile, string description);
        List<ArticlesAdmodDto> GetArticlesAdmod(string countryCode);
    }
}
