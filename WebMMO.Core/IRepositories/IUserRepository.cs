﻿using System.Collections.Generic;
using WebMMO.Core.DomainModels.User;
using WebMMO.Core.DomainModels.Role;

namespace WebMMO.Core.IRepositories
{
    public interface IUserRepository
    {
        UserDto GetUserByUsername(string userName);

        UserDto GetUserById(int id);
        UserDto GetUserByEmail(string email);
        int UpdatePassword(UserDto user);
        int UpdateLastLoginDate(UserDto user);
        ActivateUserRequestDto GetActivateUserByKey(string id);
        int UpdateUser(UserDto user);
        int RemoveActivateUserRequest(ActivateUserRequestDto activateUserRequest);
        ActivateUserRequestDto GetActivateUserByUserId(int id);
        List<ActivateUserRequestDto> GetActivateUserAll();
        ResetPasswordRequestDto GetResetPasswordByKey(string key);
        int RemoveResetPassword(ResetPasswordRequestDto resetPasswordRequest);
        int InsertResetPasswordRequest(ResetPasswordRequestDto resetPasswordRequest);
        int AddUser(UserDto user);
        int AddUserRole(UserRoleDto userrole);
        IEnumerable<UserRoleDto> GetUserRolesByUserId(int userId);
        int DeleteUserRole(UserRoleDto currentRole);
        int DeleteUserRoleByUserId(int userId);
        IEnumerable<UserRoleDto> GetUserRolesById(int userId, int roleId);
        IEnumerable<UserResult> GetQueryCbUser(int? organizationId, string status, int? roleId);
        int UpdateActiveUserRequest(ActivateUserRequestDto activateUserRequest);
        IEnumerable<StateDto> GetStateByCountryId(int countryIdValue);
    }
}
