﻿using System.Collections.Generic;
using WebMMO.Core.DomainModels.Permission;
using WebMMO.Core.DomainModels.Role;

namespace WebMMO.Core.IRepositories
{
    public interface IRoleRepository
    {
        IEnumerable<RoleDto> GetRoleByUserId(int id);

        RoleDto GetRoleById(int roleRoleId);
        int InsertPermission(PermissionDto permissionDto);
        IEnumerable<RoleDto> GetRoleAll();
        int InsertRoles(RoleDto objNew);
        int UpdateRole(RoleDto currentRole);
        IEnumerable<RoleDto> GetRoleQuery(bool isSysAdmin, bool isExternal, int rolerank);
        int DeleteRolePermissionById(int id);
        int DeletePermissionById(int permissionId);
        IEnumerable<RolePermissionDto> GetRolePermissionById(int? id, int? roleid, int? permissionId);
        IEnumerable<PermissionDto> GetPermission(int? id, int? menuId, string permissionDescription, string name);
        int InsertRolePermission(RolePermissionDto rolePermission);
        int UpdatePermission(PermissionDto permission);
        bool CheckPermission(int userId, string strPermission);
    }


}
