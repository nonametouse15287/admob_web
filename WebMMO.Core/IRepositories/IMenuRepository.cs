﻿using System.Collections.Generic;
using WebMMO.Core.DomainModels.Menu;

namespace WebMMO.Core.IRepositories
{
    public interface IMenuRepository
    {
        IEnumerable<MenuDto> GetMenuList(int? menuParentId, int? Seq);
        MenuDto GetMenuById(int id);
        int UpdateMenu(MenuDto menu);
        IEnumerable<RoleMenuDto> GetRoleMenu(int? menuId, int? roleId);
        int DeleteRoleMenu(int id);
        int InsertRoleMenu(RoleMenuDto newaccess);
        IEnumerable<MenuDto> GetMenuByParentId(int parentId);
        int InsertMenu(MenuDto menu);
        int DeleteMenu(int menuId);
        IEnumerable<MenuDto> GetMenu_HIAS_GET_MENUACCESS(int id, int? intMenuParentId);
    }


}
