﻿using System;
using System.Collections.Generic;
using WebMMO.Core.DomainModels.AppManage;
using WebMMO.Core.DomainModels.Appsetting;
using WebMMO.Core.MessageResponse;

namespace WebMMO.Core.IRepositories
{
    public interface IAppSystemRepository
    {
        Tuple<int, List<AppPermissionResult>> SearchAppSystems(FilterTableParams tableParams, string name);
        AppPermissionDto AppPermissionByGuid(Guid value);
        int AddAndEdit(AppPermissionDto objNew);
        int AddAndEditArticlesAdmod(ArticlesAdmodDto objNew);
        ArticlesAdmodDto ArticlesAdmodById(int value);
        Tuple<int, List<ArticlesAdmodDto>> SearchArticlesAdmod(FilterTableParams tableParams, string name);
    }
}
