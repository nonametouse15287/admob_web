﻿using System;
using System.Collections.Generic;
using WebMMO.Core.DomainModels.devices;
using WebMMO.Core.DomainModels.Gmail;
using WebMMO.Core.DomainModels.ImageSystem;
using WebMMO.Core.DomainModels.Sms;
using WebMMO.Core.DomainModels.WebMMO;
using WebMMO.Core.MessageResponse;

namespace WebMMO.Core.IRepositories
{
    public interface IWebMMORepository
    {
        Tuple<int, List<ImageCategoriesResult>> SearchCategories(FilterTableParams tableParams, string name);
        int AddAndEdit(ImageCategoriesDto model);
        Tuple<int, List<ImageSubCategoriesResult>> SearchSubCategories(FilterTableParams tableParams, string name, string Status, int Categories);
        int AddAndEditSubCategories(ImageSubCategoriesDto objNew);
        ImageCategoriesDto GetCategoriesByGuid(Guid value);
        ImageSubCategoriesDto GetSubCategoriesByGuid(Guid value);
        List<ImageCategoriesDto> GetAllCategories();
        List<ImageSubCategoriesDto> GetAllSubCategories();
        List<ImageSubCategoriesDto> GetSubCategoriesByGroup(int categoriesId);
        int AddAndEditImageSearch(ImageListDto objInsert);
        List<ImageListDto> GetAllImagesSync(int status);
        Tuple<int, List<ImageListResult>> SearchImages(FilterTableParams tableParams, string name, string status, int categories, int subCategories);
        int AddAndEditSearchLog(SearchLogDto objSearch);
        ImageListDto GetWebMMOByGuid(Guid valueGuid);
        List<ImageCategoriesDto> GetAppCategories(string appPackage);
        ApiKeyDto GetTop1Key();
        int UpdateCountApiKey(int iD);
        int RefreshApiKey();
        AccessTokenUserDto CheckToken(string token);
        int UpdateBalanceApiKey(AccessTokenUserDto chkCheckApi);
        AccessTokenUserDto GetAccessTokenUserGuid(string key);
        int AddAndEditAccessTokenUser(AccessTokenUserDto objNew);
        Tuple<int, List<AccessTokenUserDto>> SearchManageKey(FilterTableParams tableParams, string name);
        int AddAndEditSmsPrice(SmsPriceDto objNew);
        SmsPriceDto GetSmsPriceById(int id);
        Tuple<int, List<SmsPriceDto>> SearchManagePriceSms(FilterTableParams tableParams, string name);
        int GmailPush(GmailInfoDto objGmailPush);
        List<GmailInfoDto> GmailGet(string token, string emailtype, int amount);
        GmailPriceDto GmailPriceGetByType(string type);
        List<GmailInfoDto> GmailGetByKey(string token, string type, int amount);
        GmailStatictisResult GmailStatistics(string token);
        int DevicesPush(string deviceinfo);
        DevicesInfoDto DevicesGet();
        void DevicesFakeInsert(DevicesFakeInfoDto objFakeInfo);
        List<CitiesCodeDto> GetCitiesCode(string countryCode);
        List<string> GetKeyNews();
    }
}
