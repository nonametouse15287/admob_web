﻿using WebMMO.Core.DomainModels.Appsetting;

namespace WebMMO.Core.IRepositories
{
    public interface IAppSettingRepository
    {
        AppSettingDto GetAppSettingByName(string isUnderMaintenanceProperty);
        int UpdateAppSetting(AppSettingDto appSetting);
    }
}
