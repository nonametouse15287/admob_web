﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WebMMO.Core.DomainModels.Sms
{
    public class SmsOnlineSimResult
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string response { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string country { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string sum { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string service { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string number { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string tzid { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? time { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string msg { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string form { get; set; }
    }
}
