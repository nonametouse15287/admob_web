﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMMO.Core.DomainModels.Sms
{
    public class AccessTokenUserDto
    {
        public int Id { get; set; }
        public string AccessToken { get; set; }
        public int? UserId { get; set; }
        public int? BalanceMoney { get; set; }
        public int? BalanceUsed { get; set; }
        public int? BalanceErorr { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public int? Status { get; set; }
    }
}
