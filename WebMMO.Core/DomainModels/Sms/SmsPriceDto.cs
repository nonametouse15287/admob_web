﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMMO.Core.DomainModels.Sms
{
    public class SmsPriceDto
    {
        public int Id { get; set; }
        public string ServicesName { get; set; }
        public string ServicesKey { get; set; }
        public Decimal? Price { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
    }
}
