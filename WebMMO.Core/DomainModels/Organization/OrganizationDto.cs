﻿using System;

namespace WebMMO.Core.DomainModels.Organization
{
    public class OrganizationDto
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string CbCode { get; set; }
        public string BusinessRegNumber { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string Postcode { get; set; }
        public string City { get; set; }
        public int? StateID { get; set; }
        public int? CountryID { get; set; }
        public string ContactNumber { get; set; }
        public string FaxNumber { get; set; }
        public int? BusinessTypeID { get; set; }
        public int? ApplicationTypeID { get; set; }
        public int? CompanySizeID { get; set; }
        public int? IndustryTypeID { get; set; }
        public string Website { get; set; }
        public byte[] Logo { get; set; }
        public byte[] HalalLogo { get; set; }
        public byte[] SampleCertificate { get; set; }
        public int? Status { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public int? CreatedByUserID { get; set; }
        public int? ModifiedByUserID { get; set; }
        public string VatNo { get; set; }
        public string TradingName { get; set; }
        public string CompanyEmail { get; set; }
        public string OperatingHours { get; set; }
        public string CompanyCode { get; set; }
        public string ECNumber { get; set; }
        public string FactoryStreet1 { get; set; }
        public string FactoryStreet2 { get; set; }
        public string FactoryPostCode { get; set; }
        public string FactoryCity { get; set; }
        public int? FactoryStateID { get; set; }
        public int? FactoryCountryID { get; set; }
        public string FactoryName { get; set; }
        public bool? AlwaysOpen { get; set; }
        public string NoofEmployees { get; set; }
        public int? HCMSID { get; set; }
        public string LogoPath { get; set; }
        public string HalalLogoPath { get; set; }
        public DateTime? LastSyncDate { get; set; }


    }

}
