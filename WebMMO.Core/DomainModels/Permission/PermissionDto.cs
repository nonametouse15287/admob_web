﻿namespace WebMMO.Core.DomainModels.Permission
{
    public class PermissionDto
    {
   
        public int ID { get; set; }

        public string Name { get; set; }
        
        public string PermissionDescription { get; set; }

        public int? MenuID { get; set; }

        public bool IsDefault { get; set; }

    }
}