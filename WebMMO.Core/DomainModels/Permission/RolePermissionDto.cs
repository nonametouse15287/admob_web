﻿namespace WebMMO.Core.DomainModels.Permission
{
    public class RolePermissionDto
    {
        public int ID { get; set; }
        public int? RoleID { get; set; }
        public int? PermissionID { get; set; }

    }
}
