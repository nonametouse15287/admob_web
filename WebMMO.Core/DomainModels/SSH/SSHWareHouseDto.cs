﻿using System;

namespace WebMMO.Core.DomainModels.SSH
{
    public class SSHWareHouseDto
    {
        public int Id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string IpAddress { get; set; }
        public string CountryCode { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int Status { get; set; }
        public string FullText { get; set; }
    }

}
