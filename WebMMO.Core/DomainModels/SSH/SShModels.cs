﻿using System.Collections.Generic;

namespace WebMMO.Core.DomainModels.SSH
{
    public class SShModels
    {
        public bool status { get; set; }
        public List<string> listSSH { get; set; }
    }
}
