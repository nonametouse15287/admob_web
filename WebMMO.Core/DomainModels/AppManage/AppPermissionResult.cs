﻿using System;

namespace WebMMO.Core.DomainModels.AppManage
{
    public class AppPermissionResult
    {
        public int ID { get; set; }
        public Guid Guid { get; set; }
        public string AppName { get; set; }
        public string AppPackage { get; set; }
        public string ListPermission { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public int Status { get; set; }
    }

}
