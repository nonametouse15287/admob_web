﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMMO.Core.DomainModels.Youtube
{
    public class QuanLyKenhYoutubeDto
    {
        public int Id { get; set; }
        public string FullTitle { get; set; }
        public string FullLink { get; set; }
        public string KeyVideo { get; set; }
        public int? IsSub { get; set; }
        public int? IsComment { get; set; }
        public int? IsLike { get; set; }
        public string FullKeySearch { get; set; }
        public string FullLinkSearch { get; set; }
        public string Description { get; set; }
        public int? Status { get; set; }
        public DateTime? createddate { get; set; }
        public int? createdby { get; set; }
        public int? LimitSub { get; set; }

    }

    public class QuanLyKenhYoutubeResult
    {
        public int Id { get; set; }
        public string FullTitle { get; set; }
        public string FullLink { get; set; }
        public string KeyVideo { get; set; }
        public int? IsSub { get; set; }
        public int? IsComment { get; set; }
        public int? IsLike { get; set; }
        public string FullKeySearch { get; set; }
        public string FullLinkSearch { get; set; }
        public string Description { get; set; }
        public int? Status { get; set; }
        public DateTime? createddate { get; set; }
        public int? createdby { get; set; }

    }


}
