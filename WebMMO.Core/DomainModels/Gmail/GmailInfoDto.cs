﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMMO.Core.DomainModels.Gmail
{
    public class GmailInfoDto
    {
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string emailrecover { get; set; }
        public string cookies { get; set; }
        public string emailType { get; set; }
        public string token { get; set; }
        public DateTime createddate { get; set; }
        public int status { get; set; }

    }

    public class GmailPriceDto
    {
        public int id { get; set; }
        public string gmailtype { get; set; }
        public int gmailprice { get; set; }
    }

    public class GmailGetLogDto
    {
        public int id { get; set; }
        public string token { get; set; }
        public int amount { get; set; }
        public string gmailtype { get; set; }
        public string result { get; set; }
        public DateTime getdate { get; set; }
        public int totalmoney { get; set; }
    }

    public class GmailStatictisResult
    {
        public string token { get; set; }
        public int amount10phut { get; set; }
        public int amount2h { get; set; }
        public int amount24h { get; set; }
        public int amounttotal { get; set; }
        public int gettotal { get; set; }
        public int realtotal { get; set; }
    }
}
