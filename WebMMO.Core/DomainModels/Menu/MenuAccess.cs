﻿namespace WebMMO.Core.DomainModels.Menu
{
    public class MenuAccess
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public bool HasSubMenu { get; set; }

        public bool Enable { get; set; }

    }
}