﻿namespace WebMMO.Core.DomainModels.Menu
{
    public class RoleMenuDto
    {
        public int ID { get; set; }
        public int? RoleID { get; set; }
        public int? MenuID { get; set; }
    }
}
