﻿namespace WebMMO.Core.DomainModels.Menu
{
    public class MenuDto
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string JpName { get; set; }

        public string Link { get; set; }

        public int? MenuParentID { get; set; }

        public int Seq { get; set; }

        public string IconClass { get; set; }

    }
}