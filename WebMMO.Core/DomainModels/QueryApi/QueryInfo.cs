﻿namespace WebMMO.Core.DomainModels.QueryApi
{
    public abstract class QueryInfo : AuthenLoginToken
    {
        public int UserID { get; set; } = 0;
        public int PageSize { get; set; } = 10;
        public int PageNumber { get; set; } = 1;
        public string OrderBy { get; set; } = "DESC"; /*ASC,DESC*/
        public int OrderByColumns { get; set; } = 0; /**/
        public string Language { get; set; } = "en";
    }

    public abstract class AuthenLoginToken
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
