﻿namespace WebMMO.Core.DomainModels.Appsetting
{
    public class AppSettingDto
    {
        public int ID { get; set; }
        public string PropertyName { get; set; }
        public string PropertyDescription { get; set; }
        public string Value { get; set; }

    }
}
