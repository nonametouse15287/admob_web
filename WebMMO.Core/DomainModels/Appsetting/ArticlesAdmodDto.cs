﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMMO.Core.DomainModels.Appsetting
{
    public class ArticlesAdmodDto
    {
        public int? Id { get; set; }
        public string Title { get; set; }
        public string BodyContent { get; set; }
        public string CountryCode { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
    }

}
