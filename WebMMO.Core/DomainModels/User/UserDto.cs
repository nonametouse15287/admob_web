﻿using System;

namespace WebMMO.Core.DomainModels.User
{
    public class UserDto
    {
        public int ID { get; set; }
        public int? OrganizationID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string ContactNumber { get; set; }
        public bool? IsActive { get; set; }
        public byte[] Password { get; set; }
        public byte[] Photo { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public int? CreatedByUserID { get; set; }
        public int? ModifiedByUserID { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public int? GenderID { get; set; }
        public int? AgeGroupID { get; set; }
        public int? NationalityID { get; set; }
        public int? CountryID { get; set; }
        public string JobTitle { get; set; }
        public bool? DisableGetStarted { get; set; }
        public string LastLoginIP { get; set; }
        public int? DashboardID { get; set; }

    }


    public class UserResult
    {
        public int ID { get; set; }
        public int? OrganizationID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string ContactNumber { get; set; }
        public bool? IsActive { get; set; }
        public byte[] Password { get; set; }
        public byte[] Photo { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateModified { get; set; }
        public int? CreatedByUserID { get; set; }
        public int? ModifiedByUserID { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public int? GenderID { get; set; }
        public int? AgeGroupID { get; set; }
        public int? NationalityID { get; set; }
        public int? CountryID { get; set; }
        public string JobTitle { get; set; }
        public bool? DisableGetStarted { get; set; }
        public string LastLoginIP { get; set; }
        public int? DashboardID { get; set; }
        public string RoleName { get; set; }

        public void FromUserDto(UserDto objDto)
        {
            ID = objDto.ID;
            OrganizationID = objDto.OrganizationID;
            FirstName = objDto.FirstName;
            LastName = objDto.LastName;
            EmailAddress = objDto.EmailAddress;
            ContactNumber = objDto.ContactNumber;
            IsActive = objDto.IsActive;
            Password = objDto.Password;
            Photo = objDto.Photo;
            DateCreated = objDto.DateCreated;
            DateModified = objDto.DateModified;
            CreatedByUserID = objDto.CreatedByUserID;
            ModifiedByUserID = objDto.ModifiedByUserID;
            LastLoginDate = objDto.LastLoginDate;
            GenderID = objDto.GenderID;
            AgeGroupID = objDto.AgeGroupID;
            NationalityID = objDto.NationalityID;
            CountryID = objDto.CountryID;
            JobTitle = objDto.JobTitle;
            DisableGetStarted = objDto.DisableGetStarted;
            LastLoginIP = objDto.LastLoginIP;
            DashboardID = objDto.DashboardID;


        }

    }
}
