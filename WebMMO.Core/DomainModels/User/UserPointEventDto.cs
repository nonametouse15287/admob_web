﻿using System;

namespace WebMMO.Core.DomainModels.User
{
    public class UserPointEventDto
    {
        public int ID { get; set; }
        public int? UserId { get; set; }
        public int? EventId { get; set; }
        public int? RecievePointEvent { get; set; }
        public DateTime? DateCreate { get; set; }

    }
}
