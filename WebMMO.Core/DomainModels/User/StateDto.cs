﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMMO.Core.DomainModels.User
{
    public class StateDto
    {
        public int ID { get; set; }
        public int? CountryID { get; set; }
        public string Name { get; set; }

    }
}
