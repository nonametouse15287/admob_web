﻿using System;

namespace WebMMO.Core.DomainModels.User
{
    public class ActivateUserRequestDto
    {
        public int ID { get; set; }

        public int? UserID { get; set; }
        
        public string Key { get; set; }
        
        public DateTime? DateCreated { get; set; }
    }
}