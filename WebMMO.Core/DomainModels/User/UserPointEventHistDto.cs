﻿using System;

namespace WebMMO.Core.DomainModels.User
{
    public class UserPointEventHistDto
    {
        public int ID { get; set; }
        public int? MapEventPointId { get; set; }
        public int? UserId { get; set; }
        public int? RecievePoint { get; set; }
        public DateTime? DateCreate { get; set; }

    }
}
