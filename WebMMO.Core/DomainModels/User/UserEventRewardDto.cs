﻿using System;

namespace WebMMO.Core.DomainModels.User
{
    public class UserEventRewardDto
    {
        public int ID { get; set; }
        public int? UserId { get; set; }
        public int? EventRewardId { get; set; }
        public DateTime? RecieveDate { get; set; }
    }
}
