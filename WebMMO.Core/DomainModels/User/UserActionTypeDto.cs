﻿using System;

namespace WebMMO.Core.DomainModels.User
{
    public class UserActionTypeDto
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? OrderNum { get; set; }
        public int? Status { get; set; }
        public DateTime? DateCreate { get; set; }
    }
}
