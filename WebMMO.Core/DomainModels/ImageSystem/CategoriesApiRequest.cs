﻿using WebMMO.Core.DomainModels.QueryApi;

namespace WebMMO.Core.DomainModels.WebMMO
{
    public class CategoriesApiRequest : QueryInfo
    {
        public string SearchKey { get; set; }
        public int CategoriesId { get; set; }
        public int SubCategoriesId { get; set; }
    }

    public class CreateApiRequest : QueryInfo
    {
        public string Key { get; set; }
        public string service_id { get; set; }
        public string country_id { get; set; }
    }

    public class CheckApiRequest : QueryInfo
    {
        public string Key { get; set; }
        public string tzid { get; set; }
    }
}
