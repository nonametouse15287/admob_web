﻿using System;

namespace WebMMO.Core.DomainModels.ImageSystem
{
    public class ImageSubCategoriesDto
    {
        public int ID { get; set; }
        public Guid Guid { get; set; }
        public string SubCategoriesName { get; set; }
        public string SubCategoriesSearch { get; set; }
        public string SubCategoriesElastic { get; set; }
        public int? CategoriesId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifedBy { get; set; }
        public int? Status { get; set; }
        public int? SyncElasticStatus { get; set; }
    }
}
