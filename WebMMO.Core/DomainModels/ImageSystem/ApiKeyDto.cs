﻿using System;

namespace WebMMO.Core.DomainModels.WebMMO
{
    public class ApiKeyDto
    {
        public int ID { get; set; }        
        public string ApiKey { get; set; }
        public int CountNumber { get; set; }        
        public DateTime? NextDate { get; set; }
    }
}
