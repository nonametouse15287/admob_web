﻿using System;

namespace WebMMO.Core.DomainModels.WebMMO
{
    public class SearchLogDto
    {
        public int ID { get; set; }
        public string KeySearch { get; set; }
        public int CategoriesId { get; set; }
        public int SubCategoriesId { get; set; }
        public int UserId { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
