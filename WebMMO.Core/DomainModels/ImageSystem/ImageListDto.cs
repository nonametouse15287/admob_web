﻿using System;

namespace WebMMO.Core.DomainModels.WebMMO
{
    public class ImageListDto
    {
        public int ID { get; set; }
        public Guid Guid { get; set; }
        public Guid GuidSearch { get; set; }
        public int? CategoriesId { get; set; }
        public int? SubCategoriesId { get; set; }        
        public string KeyElasticSearch { get; set; }
        public string ImageThumb { get; set; }
        public string ImageLink { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public int? Status { get; set; }
        public int? SyncElasticStatus { get; set; }
    }

}
