﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMMO.Core.DomainModels.WebMMO
{
    public class ImageCategoriesResult
    {
        public int ID { get; set; }
        public Guid Guid { get; set; }
        public string CategoriesName { get; set; }
        public string CategoriesSearch { get; set; }
        public string CategoriesElastic { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifedBy { get; set; }
        public int? Status { get; set; }
        public int? SyncElasticStatus { get; set; }
    }
}
