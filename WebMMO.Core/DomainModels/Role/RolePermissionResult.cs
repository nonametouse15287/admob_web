﻿namespace WebMMO.Core.DomainModels.Role
{
    public class RolePermissionResult
    {
        public int PermissionID { get; set; }

        public string PermissionName { get; set; }

        public bool HasPermission { get; set; }

    }
}