﻿namespace WebMMO.Core.DomainModels.Role
{
    public class RoleMenu
    {
        public int ID { get; set; }

        public int RoleID { get; set; }

        public int MenuID { get; set; }

        public virtual RoleDto Role { get; set; }
    }
}