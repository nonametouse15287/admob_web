﻿namespace WebMMO.Core.DomainModels.Role
{
    public class UserRoleDto
    {
        public int ID { get; set; }
        public int? UserID { get; set; }
        public int? RoleID { get; set; }
    }
}
