﻿namespace WebMMO.Core.DomainModels.Role
{
    public class RolePermission
    {

        public int ID { get; set; }

        public int RoleID { get; set; }

        public int PermissionID { get; set; }

    }
}