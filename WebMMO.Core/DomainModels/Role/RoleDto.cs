﻿namespace WebMMO.Core.DomainModels.Role
{
    /// <summary>
    /// Represents the Role database entity.
    /// </summary>

    public class RoleDto
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string JpName { get; set; }

        public string Description { get; set; }

        public bool IsSysAdmin { get; set; }

        public bool IsExternal { get; set; }

        public int? RankLevel { get; set; }

        public bool IsAuditor { get; set; }
    }
}