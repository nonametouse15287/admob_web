﻿public class DeviceTemp
{
    public string AndroidID { get; set; }
    public string AndroidSerial { get; set; }
    public string Base_OS { get; set; }
    public string Board { get; set; }
    public string Bootloader { get; set; }
    public string Brand { get; set; }
    public string BuildID { get; set; }
    public string BuildTime { get; set; }
    public string CodeName { get; set; }
    public string CountryISO { get; set; }
    public string Display { get; set; }
    public string FingerPrint { get; set; }
    public string GSFID { get; set; }
    public string Hardware { get; set; }
    public string Host { get; set; }
    public string Imei { get; set; }
    public string Incremental { get; set; }
    public string KernelVersion { get; set; }
    public string LanguageCode { get; set; }
    public string Manufacture { get; set; }
    public string Model { get; set; }
    public string PreviewSDK { get; set; }
    public string PreviewSDKInt { get; set; }
    public string Product { get; set; }
    public string Radio { get; set; }
    public string Release { get; set; }
    public string Resolution { get; set; }
    public string SDKInt { get; set; }
    public string SecurityPath { get; set; }
    public string Serial { get; set; }
    public string Tags { get; set; }
    public string Time { get; set; }
    public string Type { get; set; }
    public string User { get; set; }
    public string UserAgent { get; set; }
    public string Description { get; set; }
    public string Device { get; set; }
    public string ID { get; set; }
    public string InstallTime { get; set; }
}

