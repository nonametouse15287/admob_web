﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMMO.Core.DomainModels.devices
{
    public class DevicesFakeInfoDto
    {

        public string androidID { get; set; }
        public string androidSerial { get; set; }
        public string base_OS { get; set; }
        public string board { get; set; }
        public string bootloader { get; set; }
        public string brand { get; set; }
        public string buildID { get; set; }
        public string buildTime { get; set; }
        public string codeName { get; set; }
        public string countryISO { get; set; }
        public string display { get; set; }
        public string fingerPrint { get; set; }
        public string gsFID { get; set; }
        public string hardWare { get; set; }
        public string host { get; set; }
        public string incremental { get; set; }
        public string kernelVersion { get; set; }
        public string languageCode { get; set; }
        public string manufacture { get; set; }
        public string model { get; set; }
        public string other1 { get; set; }
        public string other2 { get; set; }
        public string other3 { get; set; }
        public string previewSDK { get; set; }
        public string previewSDKInt { get; set; }
        public string product { get; set; }
        public string radio { get; set; }
        public string release { get; set; }
        public string resolution { get; set; }
        public string sdkInt { get; set; }
        public DateTime securityPatch { get; set; }
        public string serial { get; set; }
        public string tags { get; set; }
        public string time { get; set; }
        public string type { get; set; }
        public string user { get; set; }
        public string userAgent { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? Status { get; set; }
        public int id { get; set; }
        public string imei { get; set; }
    }
}
