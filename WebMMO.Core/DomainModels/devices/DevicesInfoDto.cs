﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMMO.Core.DomainModels.devices
{
    public class DevicesInfoDto
    {
        public int id { get; set; }
        public string DevicesInfo { get; set; }
        public int DevicesType { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Status { get; set; }


    }
}
