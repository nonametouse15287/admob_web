﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMMO.Core.DomainModels.devices
{
    public class CitiesCodeDto
    {
        public int id { get; set; }
        public string country { get; set; }
        public string name { get; set; }
        public decimal lat { get; set; }
        public decimal lng { get; set; }
    }
}
