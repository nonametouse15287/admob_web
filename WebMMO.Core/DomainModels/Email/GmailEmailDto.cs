﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMMO.Core.DomainModels.Email
{
    public class GmailEmailDto
    {
        public int Id { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public int? statusprofile { get; set; }
        public int? statusdelete { get; set; }
        public string description { get; set; }
        public string fulltextinput { get; set; }
        public DateTime? createddate { get; set; }
        public int? createdby { get; set; }
        public string recoveremail { get; set; }
    }

    public class GmailEmailResult
    {
        public int Id { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public int? statusprofile { get; set; }
        public int? statusdelete { get; set; }
        public string description { get; set; }
        public string fulltextinput { get; set; }
    }


}
