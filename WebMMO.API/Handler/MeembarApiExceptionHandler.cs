﻿using System.Net;
using System.Web.Http.ExceptionHandling;
using ImageSystem.Common.Utils;
using ImageSystem.Core.Exceptions;

namespace ImageSystem.Handler
{
    public class ImageSystemExceptionHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            var exception = context.Exception as BusinessRuleException;

            if (exception != null)
            {
                var httpEx = exception as BaseHttpException;
                var httpCode = httpEx?.HttpCode ?? GetHttpCode(exception.ResultDetail.Code);

                context.Result = new TextPlainErrorResult(context.Request, exception.ResultDetail, httpCode);
            }
            else
            {
                context.Result = new TextPlainErrorResult(context.Request, context.Exception);
            }
        }

        public override bool ShouldHandle(ExceptionHandlerContext context)
        {
            return true;
        }

        private static HttpStatusCode GetHttpCode(int code)
        {
            return code == ErrorConstants.SystemInsufficientPermission ? HttpStatusCode.Forbidden : HttpStatusCode.BadRequest;
        }
    }
}