﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using ImageSystem.Common.Utils;
using ImageSystem.Core.MessageResponse;

namespace ImageSystem.Handler
{
    public class TextPlainErrorResult : IHttpActionResult
    {
        public TextPlainErrorResult()
        {
        }

        public TextPlainErrorResult(HttpRequestMessage request, Exception ex) : this(
            request,
            new ResultDetail
                (ErrorConstants.Unknown, ex.Message, ex.ToString()))
        {
        }

        public TextPlainErrorResult(HttpRequestMessage request, ResultDetail details, HttpStatusCode httpCode = HttpStatusCode.BadRequest)
        {
            Request = request;
            ResultDetail = details;
            HttpCode = httpCode;
        }

        public HttpRequestMessage Request { get; set; }
        public HttpStatusCode HttpCode { get; set; }
        public ResultDetail ResultDetail { get; set; }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = Request.CreateResponse(HttpCode, ResultDetail);
            response.Headers.Add("X-Error", ResultDetail.Code.ToString());
            return Task.FromResult(response);
        }
    }
}