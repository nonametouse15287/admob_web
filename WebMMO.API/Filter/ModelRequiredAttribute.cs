﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using ImageSystem.Common;

namespace ImageSystem.Filter
{
    /// <summary>
    /// By default invalid model can be validated but posting completely empty request is still valid!
    /// So, this attribute will be used to ensure marked models be available
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class ModelRequiredAttribute : ActionFilterAttribute
    {
        private readonly string[] argumentNames;

        public ModelRequiredAttribute(params string[] arguments)
        {
            argumentNames = arguments != null && arguments.Length == 0 ? null : arguments;
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (WebApiHelper.IsOurController(actionContext))
            {
                var nullArgs = actionContext.ActionArguments
                    .Where(a => a.Value == null && (argumentNames == null || argumentNames.Contains(a.Key)))
                    .Select(a => a.Key)
                    .ToArray();

                if (nullArgs.Length > 0)
                {
                    actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                        "The argument(s) cannot be null: " + string.Join(", ", nullArgs));
                }
            }
        }
    }
}
