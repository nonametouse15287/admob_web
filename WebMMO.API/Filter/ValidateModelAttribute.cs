﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using ImageSystem.Common;
using ImageSystem.Common.Utils;
using ImageSystem.Core.MessageResponse;
using ActionFilterAttribute = System.Web.Http.Filters.ActionFilterAttribute;
using ModelState = System.Web.Http.ModelBinding.ModelState;

namespace ImageSystem.Filter
{
    public class ValidateModelStateAttribute : ActionFilterAttribute
    {
        //ApiServicesManager RepositoryService => ApiServicesManager.RepositoryService;
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            //int isLogin = 0;

            //var actionParams = actionContext.ActionArguments.Values;

            //foreach (var param in actionParams)
            //{
            //    if (param is BaseUserLoginApiRequest || param is BaseUserRegisterApiRequest)
            //    {
            //        isLogin = 1;
            //    }
            //}

            //if (isLogin != 1)
            //{
            //    string AccessToken = string.Empty;
            //    string RefreshToken = string.Empty;

            //    if (HttpContext.Current.Request.Params["AccessToken"] != null)
            //    {
            //        AccessToken = HttpContext.Current.Request.Params["AccessToken"];
            //    }

            //    if (HttpContext.Current.Request.Params["RefreshToken"] != null)
            //    {
            //        RefreshToken = HttpContext.Current.Request.Params["RefreshToken"];
            //    }

            //    bool xxx = xxx = RepositoryService.BaseUserApiService.GetExpriedLogin(AccessToken).ResponseObject;

            //    if (!xxx)
            //    {
            //        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
            //            new ResultDetail(ErrorConstants.LoginExpried, "Login Expried.", "v1", ""));
            //    }

            //}

            //if ((HttpContext.Current.Request.Params["UserID"] != null || HttpContext.Current.Request.Params["BaseUserID"] != null ) && 
            //    (!WebApiHelper.IsOurController(actionContext) || actionContext.ModelState.IsValid))
            //{
            //    int iUserID = 0;

            //    if (HttpContext.Current.Request.Params["UserID"] == null)
            //        int.TryParse(HttpContext.Current.Request.Params["BaseUserID"], out iUserID);
            //    else
            //        int.TryParse(HttpContext.Current.Request.Params["UserID"], out iUserID);

            //    if (iUserID > 0)
            //    {
            //        string requiredPermission =
            //        $"{actionContext.ActionDescriptor.ControllerDescriptor.ControllerName}-{actionContext.ActionDescriptor.ActionName}";
            //        /*Add point for user*/
            //        /*Get point*/
            //        UserActionTypeDto objDto = RepositoryService.BaseUserApiService.GetUserActionType(requiredPermission.ToLower()).ResponseObject;
            //        if (objDto != null)
            //        {
            //            /*Add point*/
            //            BaseUserActionLogDto objUserActionLogDto = new BaseUserActionLogDto();
            //            objUserActionLogDto.UserID = iUserID;
            //            objUserActionLogDto.DateCreate = DateTime.UtcNow;
            //            objUserActionLogDto.ScoredPoint = objDto.Point;
            //            objUserActionLogDto.UserActionTypeId = objDto.ID;
            //            RepositoryService.BaseUserApiService.InsertUserActionLog(objUserActionLogDto);
            //        }
            //    }
            //}


            if (!WebApiHelper.IsOurController(actionContext) || actionContext.ModelState.IsValid) return;


            var modelStateError = new HttpError();
            foreach (KeyValuePair<string, ModelState> keyModelStatePair in actionContext.ModelState)
            {
                var key = keyModelStatePair.Key;
                var errors = keyModelStatePair.Value.Errors;
                if (errors != null && errors.Count > 0)
                {
                    IEnumerable<string> errorMessages = errors.Select(error =>
                    {
                        var attemptedValue = keyModelStatePair.Value?.Value?.AttemptedValue;
                        if (!string.IsNullOrEmpty(attemptedValue))
                            return $"The value '{attemptedValue}' is not valid.";
                        return error.Exception?.Message ?? error.ErrorMessage;
                    }).ToArray();

                    modelStateError.Add(key, errorMessages);
                }
            }

            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest,
                new ResultDetail(ErrorConstants.InvalidPropertyValue, "The request is invalid.", "v1", modelStateError));
        }
    }
}