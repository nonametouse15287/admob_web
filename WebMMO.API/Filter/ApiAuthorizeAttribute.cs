﻿using System.Web.Http;
using System.Web.Http.Controllers;
using ImageSystem.Common;

namespace ImageSystem.Filter
{
    public class ApiAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            //base.OnAuthorization(actionContext);
            if (!WebApiHelper.IsOurController(actionContext)) return;
        }
    }
}
