﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace ImageSystem.Controllers
{
    public interface IApiController
    {


    }

    public class ApiControllerBase : ApiController, IApiController
    {
        protected const string Version = "v1";
    }
}