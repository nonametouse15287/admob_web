﻿using System.Web.Http.Controllers;
using ImageSystem.Controllers;

namespace ImageSystem.Common
{
    public static class WebApiHelper
    {
        public static bool IsOurController(HttpActionContext context)
        {
            var ok = false;
            if (context != null)
            {
                var cdesc = context.ControllerContext.ControllerDescriptor;
                ok = typeof(IApiController).IsAssignableFrom(cdesc.ControllerType);
            }
            return ok;
        }
    }
}