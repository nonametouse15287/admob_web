﻿using System;
using ImageSystem.Common.Utils;
using Newtonsoft.Json.Serialization;

namespace ImageSystem.Common
{
    public class SnakeCasePropertyNamesContractResolver : CamelCasePropertyNamesContractResolver
    {
        protected override JsonDictionaryContract CreateDictionaryContract(Type objectType)
        {
            var contract = base.CreateDictionaryContract(objectType);
            contract.PropertyNameResolver = name => name;
            return contract;
        }
        protected override string ResolvePropertyName(string propertyName)
        {
            return base.ResolvePropertyName(propertyName).ToSnakeCase();
        }
    }
}
