﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using ImageSystem.Common;
using ImageSystem.Filter;
using ImageSystem.Handler;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ImageSystem
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.Filters.Add(new ValidateModelStateAttribute());
            config.Services.Replace(typeof(IExceptionHandler), new ImageSystemExceptionHandler());
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            json.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            json.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));
            json.SupportedMediaTypes.Add(new MediaTypeHeaderValue("multipart/form-data"));

            json.SerializerSettings.Formatting = Formatting.Indented;
            json.UseDataContractJsonSerializer = false;
            json.SerializerSettings.ContractResolver = new SnakeCasePropertyNamesContractResolver();
            json.SerializerSettings.MissingMemberHandling = MissingMemberHandling.Error;
            json.SerializerSettings.Converters.Add(new StringEnumConverter());
            // Web API routes
            config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);
        }
    }
}
